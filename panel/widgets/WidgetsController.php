<?php
	
	namespace frontend\widgets;
	
	use yii\base\Widget;
	
	class WidgetsController extends Widget
	{
		public $action;

		public function run()
		{			
			return $this->render('widgetsController');
		}
	}