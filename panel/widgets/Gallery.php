<?php
	
	namespace frontend\widgets;
	
	use yii\base\Widget;
	
	class Gallery extends Widget
	{		
		public function run()
		{
			return $this->render('gallery');
		}
	}