<?php
	
	namespace frontend\widgets;
	
	use yii\base\Widget;
	
	class SpecialOffer extends Widget
	{
		public $type;
		public $option;
		public $bg;
					
		public function run()
		{			
			$data["bg"] = $this->bg;
			
			return $this->render('specialOffer', $data);
		}
	}