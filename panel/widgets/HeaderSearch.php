<?php
	
	namespace frontend\widgets;
	
	use yii\base\Widget;
	
	class HeaderSearch extends Widget
	{		
		public function run()
		{

			return $this->render('headerSearch');
		}
	}