<form method="post" id="search-property" action="">
						<input type="hidden" class="input-date-from" name="date_from" id="from" value="2017-01-31" kl_vkbd_parsed="true">
						<input type="hidden" class="input-date-to" name="date_to" id="to" value="2017-02-01" kl_vkbd_parsed="true">
						<div class="form-search-area">
							<div class="box-target box-border-right">
								<p>Cel pobytu</p>
								<div class="box-select-dropdown box-select-target-dropdown">
									Wybierz cel pobytu								</div>
								<b>Wybrano podkategorie: <u>0</u></b>
								
								<div class="box-select-checked">
									<div class="col-checked-only "> <input type="checkbox" name="objects" value="1" kl_vkbd_parsed="true">Tylko <strong>NOCLEG</strong>, bez zabiegów medycznych <span class="show-help" rel="8">?</span></div>
									<div class="col-checked-area">
										<h4>Turnusy, pakiety wraz z noclegiem</h4>
										<div class="col-checked">
											<p>Pakiety weekendowe</p>
											<ul>
												<li class="all"> <input type="checkbox" name="turnusy[27]" value="27" kl_vkbd_parsed="true"> Wszystkie pakiety</li>
												<li class="one"><input type="checkbox" name="turnusy[26]" value="26" kl_vkbd_parsed="true"> Pakiety SPA &amp; Wellness</li>
												<li class="one"><input type="checkbox" name="turnusy[28]" value="28" kl_vkbd_parsed="true"> Pakiety okazjonalne</li>
												<li class="one"><input type="checkbox" name="turnusy[32]" value="32" kl_vkbd_parsed="true"> Profilaktyczno lecznicze</li>
											</ul>
										</div>
										<div class="col-checked">
											<p>TURNUSY OKOŁO 7 DNIOWE</p>
											<ul>
												<li class="all"><input type="checkbox" name="turnusy[17]" value="17" kl_vkbd_parsed="true"> Wszystkie 7 dniowe</li>
												<li class="one"><input type="checkbox" name="turnusy[6]" value="6" kl_vkbd_parsed="true"> Sanatoryjne</li>
												<li class="one"><input type="checkbox" name="turnusy[7]" value="7" kl_vkbd_parsed="true"> Profilaktyczne</li>
												<li class="one"><input type="checkbox" name="turnusy[22]" value="22" kl_vkbd_parsed="true"> Specjalistyczne</li>
											</ul>
										</div>
										<div class="clearfix"></div>
										<div class="col-checked">
											<p>TURNUSY OKOŁO 14 DNIOWE</p>
											<ul>
												<li class="all"><input type="checkbox" name="turnusy[18]" value="18" kl_vkbd_parsed="true"> Wszystkie około 14 dniowe</li>
												<li class="one"><input type="checkbox" name="turnusy[10]" value="10" kl_vkbd_parsed="true"> Sanatoryjne</li>
												<li class="one"><input type="checkbox" name="turnusy[11]" value="11" kl_vkbd_parsed="true"> Profilaktyczne</li>
												<li class="one"><input type="checkbox" name="turnusy[12]" value="12" kl_vkbd_parsed="true"> Specjalistyczne</li>
												</ul>
										</div>
										<div class="col-checked">
											<p>TURNUSY OKOŁO 21 DNIOWE</p>
											<ul>
												<li class="all"><input type="checkbox" name="turnusy[19]" value="19" kl_vkbd_parsed="true"> Wszystkie około 21 dniowe</li>
												<li class="one"><input type="checkbox" name="turnusy[13]" value="13" kl_vkbd_parsed="true"> Sanatoryjne</li>
												<li class="one"><input type="checkbox" name="turnusy[20]" value="20" kl_vkbd_parsed="true"> Profilaktyczne</li>
												<li class="one"><input type="checkbox" name="turnusy[35]" value="35" kl_vkbd_parsed="true"> Specjalistyczne</li>
											</ul>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="col-solo"><a href="/pl/zabiegi-medyczne">Lista pojedynczych zabiegów medycznych</a></div>
									<div class="box-promo">
										<h4>Super oferta</h4>
										<img src="/themes/lawendowyzdroj/files/img/_tmp/promocja.png">
									</div>
								</div>
							</div>
							<div class="box-calendar box-calendar-left box-border-left date-from" date-option="from">
								<p>Przyjazd</p>
								<div class="box-calendar-dropdown">
									<b>31</b>
									<span class="dayname first">wtorek</span>
									<span class="monthname">styczeń 2017</span>
								</div>
								
								<div class="elastic-box">
									<div class="elastic-title">Elastyczne daty:</div>
									<div class="elastic-spinner">
										<span class="spinner" data-max="10" data-prefix="dni"><em class="minus">-</em> <u>0</u> dni <em class="plus">+</em> <input type="hidden" name="elastic" value="0" kl_vkbd_parsed="true"></span>
									</div>
								</div>
								
							</div>
							<div class="box-calendar box-calendar-right box-border-right date-to" date-option="to">
								<p class="text-right">Wyjazd</p>
								<div class="box-calendar-dropdown">
									<b>01</b>
									<span class="dayname first">środa</span>
									<span class="monthname">luty 2017</span>
								</div>
								
								
							</div>
							<div class="box-persons">
								<div class="line-persons line-persons-first">
									<div>Kobiety <span class="spinner" data-max="4"><em class="minus">-</em> <u>0</u> <em class="plus">+</em> <input type="hidden" name="women" value="0" kl_vkbd_parsed="true"></span></div>
									<div class="center">Mężczyźni <span class="spinner" data-max="4"><em class="minus">-</em> <u>0</u> <em class="plus">+</em> <input type="hidden" name="men" value="0" kl_vkbd_parsed="true"></span></div>
									<div class="advanced-dropdown">Dodaj lub edytuj opcje</div>
									<div class="advanced-count">Wybranych filtrów: <b>0</b></div>
									
									<div class="advanced-dropdown-roll">
										
										<div class="spinner add-adults" "="">
											<div>
												Osoby towarzyszące
												<span class="spinner" data-max="3"><em class="minus">-</em> <u>0</u> <em class="plus">+</em> <input type="hidden" name="adv_adults" value="0" kl_vkbd_parsed="true"></span>
												<span class="show-help" rel="11">?</span>
											</div>
										</div>

										<ul>
											<li class="">Dostawka <input type="checkbox" name="extra" value="1" kl_vkbd_parsed="true"><span class="show-help" rel="12">?</span></li>
											<li class="">Super oferty <input type="checkbox" name="bestdeal" value="1" kl_vkbd_parsed="true"><span class="show-help" rel="13">?</span></li>
											<li class="">Oferty last minute <input type="checkbox" name="lastminute" value="1" kl_vkbd_parsed="true"><span class="show-help" rel="14">?</span></li>
											<li class="">Oferty first minute <input type="checkbox" name="firstminute" value="1" kl_vkbd_parsed="true"><span class="show-help" rel="15">?</span></li>
											<li class="">Oferty "Nie czekaj na weekend" <input type="checkbox" name="shortgaps" value="1" kl_vkbd_parsed="true"><span class="show-help" rel="16">?</span></li>
										</ul>
									</div>
								</div>
								<div class="line-persons line-persons-two">
									<div>0-2 lat <span class="spinner" data-max="3"><em class="minus">-</em> <u>0</u> <em class="plus">+</em> <input type="hidden" name="age1" value="0" kl_vkbd_parsed="true"></span></div>
									<div class="center">2-12 lat <span class="spinner" data-max="3"><em class="minus">-</em> <u>0</u> <em class="plus">+</em> <input type="hidden" name="age2" value="0" kl_vkbd_parsed="true"></span></div>
								</div>
							</div>
						</div>
						<div class="box box-button button-search-general">
							<button>Szukaj</button>
						</div>
						
						<div class="alert-search-top"></div>
					</form>