<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-panel',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'panel\controllers',
    'bootstrap' => ['panel\components\DynamicRoutes'],
    'modules' => [
        'settings'			=> ['class' => 'panel\modules\settings\Module'],
        'reservations' 		=> ['class' => 'panel\modules\reservations\Module'],
        'rooms' 			=> ['class' => 'panel\modules\rooms\Module'],
        'activities' 		=> ['class' => 'panel\modules\activities\Module'],
        'pages' 			=> ['class' => 'panel\modules\pages\Module'],
        'reviews' 			=> ['class' => 'panel\modules\reviews\Module'],
        'communication' 	=> ['class' => 'panel\modules\communication\Module'],
    ],
    'language' => 'pl-PL',
    'sourceLanguage' => 'pl-PL',
    'components' => [
	    'db' => require(__DIR__ . '/db.php'),
	    'price' => ['class' => 'common\components\Price'],
	    'gallery' => ['class' => 'common\components\Gallery'],
	    'availability' => ['class' => 'common\components\Availability'],
	    'general' => ['class' => 'common\components\General'],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl'=>'',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'assignmentTable'=> '{{%users_auth_assignment}}',
            'itemTable'=> '{{%users_auth_item}}',
            'itemChildTable'=> '{{%users_auth_item_child}}',
        ],
        'i18n' => [
	        'translations' => [
	            'panel*' => [
	                'class' => 'yii\i18n\PhpMessageSource',
	                'fileMap' => [
	                    'app' => 'app.php',
	                    'app/error' => 'error.php',
	                ],
	            ],
	        ],
	    ],
        'urlManager'=>[
            'scriptUrl'=>'/panel/index.php',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-panel', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,

        ],
    ],
    'params' => $params,
];
