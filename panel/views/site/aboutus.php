<?php

	use yii\helpers\Html;

	$this->title = 'About';
	//$this->params['breadcrumbs'][] = $this->title;
	
?><div class="container">
	<div class="lead-header lead-header-slim">
		<h2>Lawendowy Zdrój <small>Medycyna & SPA - Busko Zdrój</small></h2>
		<img src="/img/page/header_about_us.png">
	</div>
	
	<div class="content-page content-text-cms">
		<div class="box-special">
			<p>Lawendowy Zdrój to nowo powstały pensjonat medyczny, który zapadnie w pamięć wielu kuracjuszom ze względu na unikalnie zaprojektowane tematyczne wnętrza. Specjalizujemy się w holistycznym podejściu do leczenia. Oferujemy zabiegi z wykorzystaniem kąpieli siarkowych oraz metody leczenie z zakresu Holistycznej Medycyny Manualnej. Dysponujemy rozbudowaną strefą SPA & Wellness z kompleksem saun, jacuzzi, grotą solną oraz restauracją, która serwuje zdrowe i smaczne dania mięsne i wegetariańskie. Zapraszamy do Lawendowego Zdroju na turnusy sanatoryjne, specjalistyczne turnusy lecznicze, warsztaty jogi, psychoterapię indywidualna i grupową, a także relaksacyjne weekendy. Jednym słowem wszystko dla ciała, umysłu i duszy.</p>
			
			<h4><span>Nasza Filozofia</span></h4>
			
			<p>W Lawendowym Zdroju przykładamy ogromną wagę do kompleksowego podejścia do każdego Kuracjusza goszczącego w naszym pensjonacie. Termin „podejście holistyczne” w dzisiejszych czasach często jest nadużywany, jednak my autentycznie wierzymy, że postrzeganie człowieka na wszystkich poziomach zarówno duchowym, jak i fizycznym wspomaga proces zdrowienia. Manualna Medycyna Holistyczna, która jest naszą specjalnością traktuje psychikę jako kluczowy czynnik w chorobie, co medycyna konwencjonalna często marginalizuje.</p>
			
			<h4><span>Nasz Ośrodek</span></h4>
			
			<p>Lawendowy Zdrój usytuowany jest w Busku-Zdroju Zbludowicach. Znajduje się zaledwie kilka minut od głównej alei spacerowej do parku zdrojowego, a jednocześnie relatywnie blisko kąpieliska Radzanów. Do dyspozycji naszych gości oddajemy 31 komfortowych pokojów tematycznych urządzonych w odmiennych aranżacjach. Do każdego pokoju przynależy oddzielna łazienka. Ponadto sypialnie wyposażone są w darmowy dostęp do sieci WiFi, mini barek, sejf oraz telewizor LCD.</p>
			
			<h4><span>Nasze Zabiegi Lecznicze</span></h4>
			
			<p>Lawendowy Zdrój łączy w sobie atmosferę rodzinnego pensjonatu i sanatorium nastawionego na leczenie. Korzystając z naturalnych źródeł siarki występujących w Busku Zdroju, specjalizujemy się w leczeniu tym dobroczynnym pierwiastkiem piękna. Kąpiele siarkowe są bezkonkurencyjne w leczeniu stawów, schorzeń reumatycznych, chorób skóry i wzmacnianiu odporności. Zabiegi z zakresu balneologii, na którą kładziemy duży nacisk, sprawiają, że skóra, włosy i paznokcie są zdrowe, odżywione i zregenerowane. Kuracje z wykorzystaniem siarki łagodzą zapalenie stawów kręgosłupa, rwy kulszowej oraz barkowej.</p>
			
			<h4><span>Nasze Turnusy Lecznicze i Pobyty Sanatoryjne</span></h4>
			
			<p>Specjalistyczne turnusy lecznicze, bazujące na Holistycznej Medycynie Manualnej, znajdujące się w ofercie Lawendowego Zdroju powstały m. in. z myślą o osobach cierpiących na migreny, dolegliwości bólowe bioder, kręgosłupa, barków, żuchwy czy kolan. Oprócz turnusów specjalistycznych oferujemy naszym klientom standardowe pobyty sanatoryjne, dysponując w tym celu szerokim wachlarzem zabiegów z zakresu fizykoterapii. Elektroterapia, Balneoterapia, Hydroterapia czy Światłolecznictwo to tylko niektóre z metod leczniczych, dzięki którym ułatwiamy naszym gościom powrót do zdrowia.</p>
			
			<h4><span>Nasze SPA & Wellness</span></h4>
			
			<p>Z troski o urodę i ciało, w Lawendowym Zdroju powstało kompleksowe centrum SPA & Wellness. Oferujemy lecznicze i relaksacyjne masaże, zabiegi pielęgnacyjne na twarz i ciało. W tym miejscu nasi goście  w atmosferze spokoju i relaksu mogą również skorzystać z jacuzzi, kompleksu saun oraz groty solnej. </p>
			
			<h4><span>Nasza kuchnia</span></h4>
			
			<p>W naszej kuchni wszystkie dania przygotowywane są z jedną, główną najważniejszą myślą, żywienie ma służyć zdrowiu. Dania przygotowywane są z wielką ważnością i miłością do gotowania. Wszyscy, którzy pracują w kuchni traktują swoją prace z pasją i chęcią poznawania nowych smaków. Odnajdujemy te smaki w produktach najwyższej jakości, ze starannie wyselekcjonowanych produktów ekologicznych, naturalnych, lokalnych i regionalnych oraz daniach z nich przygotowywanych. Zapewniamy żywienie w oparciu o dobre węglowodany, białka, tłuszcze wszystko w odpowiednich proporcjach. Przywiązujemy ogromną wagę do jakości, wyglądu i smaku serwowanego jedzenia. Tworzymy menu w zgodzie ze zmieniającymi się porami roku, stosując w diecie sezonowość. Ważne są też dla nas techniki gotowania. Od gotowania na parze, przez długie gotowanie i pieczenie w niskiej temperaturze do tradycyjnego pieczenia i krótkiego smażenia. Gotujemy na nowoczesnym sprzęcie, przy użyciu gazu. Komponujemy codzienne menu tak, aby każdy znalazł coś interesującego dla siebie, aby wegetarianin i weganin mógł się u nas żywić, a mięsożerca nie był głodny.</p>
			
			<p class="text-welcome">Serdecznie zapraszamy wszystkich, którzy chcą aktywnie odpocząć, zregenerować siły, podreperować zdrowie oraz odetchnąć od zgiełku miasta w uzdrowiskowym regionie!</p>
		</div>
	</div>
	
</div>
