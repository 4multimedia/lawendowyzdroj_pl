<?php
	
	use yii\widgets\Pjax;
	use frontend\widgets\WidgetsController;
	
?><div class="container">
	<div class="row">
		<div class="col-md-9">
			<?php Pjax::begin([]); ?>
			<?php echo Yii::$app->controller->renderPartial('_list', ['items' => $items]); ?>
			<?php Pjax::end(); ?>
		</div>
		<div class="col-md-3">
			<?php echo WidgetsController::widget(['action' => 'activities']) ?>
		</div>
	</div>
</div>