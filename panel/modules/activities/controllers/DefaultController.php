<?php
	
	namespace app\modules\activities\controllers;
	
	use Yii;
	use yii\web\Controller;
	use common\models\Activities;
	use common\models\ActivitiesSearch;
	
	class DefaultController extends Controller
	{
	    public function actionSearch()
	    {
		    return $this->render('search');
	    }
	    
	    public function actionList()
	    {
		    $data["model"] = new ActivitiesSearch();
		    $data["params"] = Yii::$app->request->post();
			$data["items"] = $data["model"]->search($data["params"]);
		    
		    return $this->render('list', $data);
	    }
	}

?>