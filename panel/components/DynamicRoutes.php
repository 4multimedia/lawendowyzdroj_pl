<?php
	
namespace panel\components;

use Yii;
use yii\base\BootstrapInterface;

class DynamicRoutes implements BootstrapInterface
{
	public function bootstrap($app)
	{
		$routeArray["/"]			= "site/index";
		
		$app->urlManager->addRules($routeArray);
	}
}