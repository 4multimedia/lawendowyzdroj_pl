<?php
	
	namespace api\modules\v1\models;
	
	use Yii;
	use \yii\db\ActiveRecord;
	use yii\web\Link;
	use yii\web\Linkable;
	
	class Activities extends ActiveRecord
	{		
		public static function tableName()
	    {
	        return 'stays';
	    }
	    
	    public function fields()
	    {
	        return [
		        'id',
	        	'name'
	        ];
	    }
	    
	    public function getLinks()
	    {
	        return [
	            Link::REL_SELF => Url::to(['user/view', 'id' => $this->id], true),
	            'edit' => Url::to(['user/view', 'id' => $this->id], true),
	            'profile' => Url::to(['user/profile/view', 'id' => $this->id], true),
	            'index' => Url::to(['users'], true),
	        ];
	    }
	    
	    public function rooms($id)
	    {
		    return [];
	    }
	}