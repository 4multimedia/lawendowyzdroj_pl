<?php
	
	namespace api\modules\v1\models;
	
	use Yii;
	use \yii\db\ActiveRecord;
	use yii\web\Link;
	use yii\web\Linkable;
	
	class Reservations extends ActiveRecord
	{		
		public static function tableName()
	    {
	        return 'reservations';
	    }
	    
	    public function fields()
	    {
	        return [
		        'key' => 'sesid',
	        	'reservation_no' => 'no',
	        	'type' => function($model) { if ($this->object_type == "stay") { return "activity"; } else { return "room"; }},
	        	'activity_id' => function($model) { if ($this->object_type == "stay") { return $this->object_id; } else { return ""; }},
	        	'name',
	        	'lastname',
	        	'address',
	        	'code',
	        	'town',
	        	'country',
	        	'email',
	        	'phone',
	        	'adults' => function($model) { return ['men' => $this->people_men, 'women' => $this->people_women, 'partner' => $this->people_compan]; },
	        	'children' => function($model) { return ['range1' => $this->people_children1, 'range2' => $this->people_children2, 'range3' => $this->people_children3]; },
	        	'package_name' => function($model) { return ($this->package_name == "Wybrany własny pokój" ? "WŁASNY PAKIET" : $this->package_name); },
	        	'rooms' => function($model) { self::rooms($this->id); },
	        	'price' => function($model) { return ['price' => $this->price, 'no_canceled' => $this->no_canceled, 'booking_percent' => $this->booking_percent]; },
	        	'date' => function($model) { return ['begin' => $this->date_from, 'end' => $this->date_to]; }
	        ];
	    }
	    
	    public function extraFields()
		{
		    return ['profile'];
		}
	    
	    public function getLinks()
	    {
	        return [
	            Link::REL_SELF => Url::to(['user/view', 'id' => $this->id], true),
	            'edit' => Url::to(['user/view', 'id' => $this->id], true),
	            'profile' => Url::to(['user/profile/view', 'id' => $this->id], true),
	            'index' => Url::to(['users'], true),
	        ];
	    }
	    
	    public function rooms($id)
	    {
		    return [];
	    }
	}