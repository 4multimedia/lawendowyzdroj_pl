<?php

	namespace api\modules\v1\controllers;
	
	use yii\rest\ActiveController;
	use yii\web\Response;
	use yii\data\ActiveDataProvider;
	use yii\filters\auth\CompositeAuth;
	use yii\filters\auth\HttpBasicAuth;
	use yii\filters\auth\HttpBearerAuth;
	use yii\filters\auth\QueryParamAuth;
	use api\modules\v1\models\Activities;
	
	/**
	 * Activities Controller API
	**/
	 
	class ActivitiesController extends ActiveController
	{
	    public $modelClass = 'api\modules\v1\models\Activities';
	    
	    public $serializer = [
	        'class' => 'yii\rest\Serializer',
	        'collectionEnvelope' => 'activities',
	    ];
	    
	    public function init()
		{
		    parent::init();
		    \Yii::$app->user->enableSession = false;
		}
	    
	    public function behaviors()
		{
		    $behaviors = parent::behaviors();
		    $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
		    $behaviors['authenticator'] = [
		        'class' => CompositeAuth::className(),
		        'authMethods' => [
		            HttpBasicAuth::className(),
		            HttpBearerAuth::className(),
		            QueryParamAuth::className(),
		        ],
		    ];
		    return $behaviors;
		}
		
		public function actions()
		{
		    $actions = parent::actions();

		    unset($actions['delete'], $actions['create']);
		
		    $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
		
		    return $actions;
		}
		
		public function prepareDataProvider()
		{
		    return new ActiveDataProvider([
            	'query' => Activities::find()->where(['active' => 1]),
				'pagination' => false,
			]);
		}
	}