<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * Register form
 */
class RegisterForm extends \yii\db\ActiveRecord
{
	public $name;
	public $lastname;
	public $password;
	public $repeat_password;
	
	public static function tableName()
	{
	    return 'users';
	}
	
	public function rules()
    {
        return [
            [['email', 'password', 'name', 'lastname', 'repeat_password'], 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Adres e-mail został zarejestrowany'],
            
            ['password', 'string', 'min' => 8],
            ['repeat_password', 'compare', 'compareAttribute'=>'password'],
        ];
    }
    
    public function attributeLabels()
	{
		return [
			'name'				=> 'Imię',
			'lastname'			=> 'Nazwisko',
			'email'				=> 'Adres e-mail',
			'password' 			=> 'Hasło',
			'repeat_password'	=> 'Powtórz hasło'
		];
	}
}