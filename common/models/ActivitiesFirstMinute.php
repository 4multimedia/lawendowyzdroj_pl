<?php
	
	namespace common\models;
	
	use Yii;
	
	class ActivitiesFirstMinute extends \yii\db\ActiveRecord
	{	

		public static function tableName()
	    {
	        return 'stays_first_minute';
	    }
	    
	    public function getActivities()
	    {
	        return $this->hasOne(Activities::className(), ['stays.id' => 'stays_id']);
	    }
	}