<?php
	
	namespace common\models;

	use Yii;
	use yii\base\Model;
	use yii\data\ActiveDataProvider;
	
	class ActivitiesSearch extends Activities
	{
		public function search($params)
		{
	
			
			if ((isset($params["begin"]) AND !empty($params["begin"])) AND (isset($params["end"]) AND !empty($params["end"])))
			{
				$query = ActivitiesDates::find();
				$query->leftJoin('stays', 'stays.id = stays_dates.stays_id');
				$query->groupBy('stays_dates.id');
			}
			else
			{
				$query = Activities::find();
				$query->leftJoin('stays_dates', 'stays_dates.stays_id = stays.id');
				$query->groupBy('stays.id');
			}
			
			$query->select([
				"stays_dates.id as stays_date_id",
				"stays.id as stay_id",
				"name", "date_from", "date_to", "days", "main_features", "special_event", "special_event_text", "summary", "alias", "slogan", "price_text", "treatment_summary", "description",
				"(select `path` FROM `objects_gallery` WHERE `type` = 'stays' AND `object_id` = `stays`.`id` ORDER BY `priority` ASC LIMIT 1) AS `image`"
			]);

			$query->leftJoin('stays_categories_relative','stays_categories_relative.stay_id = stays.id');
			$query->orderBy("date_from ASC");
			
			if ($params["type"] == "turnusy-lecznicze") { $params["category"] = [6,7,11,12,13,17,18,19,20,22,35]; }
			if ($params["type"] == "turnusy-lecznicze-sanatoryjne") { $params["category"] = [6,10,13,20,35]; }
			if ($params["type"] == "turnusy-lecznicze-profilaktyczno-lecznicze") { $params["category"] = [7,11,20]; }
			if ($params["type"] == "turnusy-lecznicze-specjalistyczne") { $params["category"] = [12,22,35]; }
			if ($params["type"] == "pakiety-weekendowe") { $params["category"] = [26,27,28,32]; }
			if ($params["type"] == "pakiety-weekendowe-spa-i-wellness") { $params["category"] = [26]; }
			if ($params["type"] == "pakiety-weekendowe-pakiety-okazjonalne") { $params["category"] = [28]; }
			if ($params["type"] == "pakiety-weekendowe-profilaktyczno-lecznicze") { $params["category"] = [32]; }
			
			if ($params["type"] == "turnusy-lecznicze-7dniowe") { $params["category"] = [4,17,6,7,22]; }
			if ($params["type"] == "turnusy-lecznicze-7dniowe-sanatoryjne") { $params["category"] = [6]; }
			if ($params["type"] == "turnusy-lecznicze-7dniowe-profilaktyczne") { $params["category"] = [7]; }
			if ($params["type"] == "turnusy-lecznicze-7dniowe-specjalistyczne") { $params["category"] = [22]; }
			if ($params["type"] == "turnusy-lecznicze-14dniowe") { $params["category"] = [8,18,10,11,12]; }
			if ($params["type"] == "turnusy-lecznicze-14dniowe-sanatoryjne") { $params["category"] = [10]; }
			if ($params["type"] == "turnusy-lecznicze-14dniowe-profilaktyczne") { $params["category"] = [11]; }
			if ($params["type"] == "turnusy-lecznicze-14dniowe-specjalistyczne") { $params["category"] = [12]; }
			if ($params["type"] == "turnusy-lecznicze-21dniowe") { $params["category"] = [9,19,20,13,35]; }
			if ($params["type"] == "turnusy-lecznicze-21dniowe-sanatoryjne") { $params["category"] = [13]; }
			if ($params["type"] == "turnusy-lecznicze-21dniowe-profilaktyczne") { $params["category"] = [20]; }
			if ($params["type"] == "turnusy-lecznicze-21dniowe-specjalistyczne") { $params["category"] = [35]; }
			
			if ($params["period"])
			{
				list($period_year, $period_month) = explode("-", $params["period"]);
				
				$query->andFilterWhere(["AND", "(MONTH(`stays_dates`.`date_to`) = $period_month AND YEAR(`stays_dates`.`date_to`) = $period_year) OR (MONTH(`stays_dates`.`date_from`) = $period_month AND YEAR(`stays_dates`.`date_from`) = $period_year)"]);
			}
			
			if (isset($params["category"]) AND !empty($params["category"]))
			{
				$query->andFilterWhere(['AND', ['stays_categories_relative.category_id' => $params["category"]]]);
			}
			
			if (isset($params["id"]))
			{
				$query->andFilterWhere(['AND', ['stays.id' => $params["id"]]]);
			}
			
			if ((isset($params["begin"]) AND !empty($params["begin"])) AND (isset($params["end"]) AND !empty($params["end"])))
			{
				if (isset($params["elastic"]))
				{
					$params["end"] = date("Y-m-d", strtotime("+".$params["elastic"]." days", strtotime($params["end"])));
					$params["begin"] = date("Y-m-d", strtotime("-".$params["elastic"]." days", strtotime($params["begin"])));
					
					if (strtotime($params["begin"]) < strtotime(date("Y-m-d")))
					{
						$params["begin"] = date("Y-m-d");
					}
				}
				
				$query->andFilterWhere(["AND", "(`stays_dates`.`date_from` >= '".$params["begin"]."' AND `stays_dates`.`date_from` <= '".$params["end"]."')"]);
			}
			else
			{
				if ($params["disabled"] == true)
				{
					//echo 'sadsda'; die;
				}
				else
				{
					$query->andFilterWhere(["AND", "`stays_dates`.`date_to` > NOW()"]);
				}
			}
			
			$dataProvider = new ActiveDataProvider([
            	'query' => $query,
            	'pagination' => false
			]);
			
			if (!($params))
			{
				return $dataProvider;
			}
			
			return $dataProvider;
		}
	}