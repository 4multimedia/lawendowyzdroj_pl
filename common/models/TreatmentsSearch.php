<?php
	
	namespace common\models;

	use Yii;
	use yii\base\Model;
	use yii\data\ActiveDataProvider;
	
	class TreatmentsSearch extends Treatments
	{
		public function search($params)
		{
			$query = Treatments::find();
			
			$dataProvider = new ActiveDataProvider([
            	'query' => $query,
            	'pagination' => false
			]);
			
			if ($params["category_id"]) { $query->andFilterWhere(['=', 'category_id', $params["category_id"]]); }
			
			if (!($params))
			{
				return $dataProvider;
			}
			
			return $dataProvider;
		}
	}