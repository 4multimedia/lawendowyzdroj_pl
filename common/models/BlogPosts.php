<?php
	
	namespace common\models;
	
	use Yii;
	use yii\imagine\Image;
	use yii\db\ActiveRecord;
	
	class BlogPosts extends ActiveRecord
	{
		public static function tableName()
	    {
	        return 'blog_posts';
	    }
	    
	    public function getCategory()
	    {
	        return $this->hasOne(BlogCategories::className(), ['blog_categories_copy.id' => 'id_category']);
	    }
	    
	    public function getParentCategory()
	    {
	        return BlogCategories::find()->where(['blog_categories_copy.id' => $this->category->id_parent])->one();
	    }
	    
	    public function getUrl()
	    {
		    return '/pl/'.$this->parentCategory->alias.'/'.$this->category->alias.'/'.$this->alias;
	    }
	    
	    public function getThumb()
	    {	        
	        $ext = explode(".", $this->image);
		    $ext = $ext[count($ext)-1];
		    $width = 600;
		    $height = 340;

			$cache = '/img/gallery/'.str_replace(" ", "_", strtolower($this->alias.'_'.$width.'_'.$height)).".".$ext;
		    
		    $cache_path = Yii::getAlias('@webroot').$cache;
		    if (!file_exists($cache_path))
		    {
		    	Image::thumbnail('@webroot'.urldecode($this->image), $width, $height)->save($cache_path, ['quality' => 100]);
		    }
		    
	        $cache = '/img/gallery/'.str_replace(" ", "_", strtolower($this->alias.'_'.$width.'_'.$height)).".".$ext;
	        
	        return $cache;
	    }
	}