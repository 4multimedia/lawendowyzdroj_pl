<?php
	
	namespace common\models;
	
	use Yii;
	use yii\base\Model;
	
	class LastMinute extends Model
	{
	    public static function maxDiscount($type)
	    {
		    if ($type == "activities")
		    {
			    $Item = ActivitiesLastMinute::find()
			    ->where("date_from > NOW()")
			    ->leftJoin("stays_dates", "stays_last_minute.stays_id = stays_dates.id")
			    ->orderBy('discount DESC')
			    ->one();
		    
			    if ($Item)
				{
					return $Item->discount + $Item->activities->price_discount_random_room;
				}
			}
			else if ($type == "rooms")
			{
				$Item = RoomsLastMinute::find()
				->orderBy('discount DESC')
				->one();
				
				if ($Item)
				{
					return $Item->discount;
				}
			}
	    }
	}