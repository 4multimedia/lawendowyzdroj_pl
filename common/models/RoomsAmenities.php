<?php
	
	namespace common\models;

	use Yii;
	
	class RoomsAmenities extends \yii\db\ActiveRecord
	{
		public static function tableName()
	    {
	        return 'objects_amenities';
	    }
	    
	    public function label($id)
	    {
		    if ($id == 1) { $label = "ogrzewanie"; }
			if ($id == 2) { $label = "pralka"; }
			if ($id == 3) { $label = "zakaza palenia"; }
			if ($id == 4) { $label = "klimatyzacja"; }
			if ($id == 5) { $label = "wi-fi"; }
			if ($id == 6) { $label = "przyjazne niepełnosprawnym"; }
			if ($id == 7) { $label = "winda"; }
			if ($id == 8) { $label = "parking"; }
			if ($id == 9) { $label = "TV&nbsp;wypożyczany&nbsp;nieodpłatnie"; }
			if ($id == 10) { $label = "łóżeczko dziecięce dostępne"; }
			if ($id == 11) { $label = "zakaz czworonogów"; }
			if ($id == 12) { $label = "łóżeczko dziecięce dostępne"; }
			
			return $label;
	    }
	}