<?php
	
	namespace common\models;
	
	use Yii;
	
	class Treatments extends \yii\db\ActiveRecord
	{			
		public static function tableName()
	    {
	        return 'treatments';
	    }
	    
	    public function getTime()
	    {
		    if ($this->time_end == 0)
		    {
			    return false;
		    }
		    else if ($this->time_end >= 60)
		    {
			    $minutes = $this->time_end - 60;
			    return "1 godz. ".($minutes > 0 ? $minutes." min." : "");
		    }
		    else
		    {
			    return $this->time_end." min";
		    }
	    }
	}