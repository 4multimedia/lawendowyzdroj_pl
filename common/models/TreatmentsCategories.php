<?php
	
	namespace common\models;
	
	use Yii;
	
	class TreatmentsCategories extends \yii\db\ActiveRecord
	{			
		public static function tableName()
	    {
	        return 'treatments_categories';
	    }
	    
	    public function getItems()
	    {
	        return $this->hasMany(Treatments::className(), ['category_id' => 'id']);
	    }
	}