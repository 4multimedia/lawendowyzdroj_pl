<?php
	
	namespace common\models;
	
	use Yii;
	
	class RoomsPrice extends \yii\db\ActiveRecord
	{
		public static function tableName()
	    {
	        return 'objects_pricelist';
	    }
	    
	    public static function pricelist($classes, $persons, $seasson, $discount = 0)
	    {
		    $Room = Rooms::find()->where(['class' => $classes, 'max_person' => $persons])->one();
		    $room_id = $Room->id;
		    
		    $Pricelists = RoomsPrice::find()->where(['object_id' => $room_id])->all();
		    
		    $array = [];
		    
		    if ($Pricelists)
		    {
			    foreach ($Pricelists as $Pricelist)
			    {
				    $Price = $Pricelist->price * ((100 + -20) / 100);
				    $Price = $Price * ((100 + $discount) / 100);
				    $array[$Pricelist->person][$Pricelist->night] = $Price;
			    }
		    }
		    
		    return $array;
	    }
	}