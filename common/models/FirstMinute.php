<?php
	
	namespace common\models;
	
	use Yii;
	use yii\base\Model;
	
	class FirstMinute extends Model
	{
	    public static function maxDiscount($type)
	    {
		    if ($type == "activities")
		    {
			    $Item = ActivitiesFirstMinute::find()
			    ->orderBy('discount DESC')
			    ->one();
		    }
		    
		    if ($Item)
			{
				return $Item->discount + $Item->activities->price_discount_random_room;
			}
	    }
	}