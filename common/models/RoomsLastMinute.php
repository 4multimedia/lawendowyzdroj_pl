<?php
	
	namespace common\models;
	
	use Yii;
	
	class RoomsLastMinute extends \yii\db\ActiveRecord
	{			
		public static function tableName()
	    {
	        return 'objects_last_minute';
	    }
	    
	    public function getRoom()
	    {
	        return $this->hasOne(Rooms::className(), ['objects.id' => 'object_id']);
	    }
	}