<?php
	
	namespace common\models;
	
	use Yii;
	
	class FaqQuestions extends \yii\db\ActiveRecord
	{			
		public $question;
		public $answer;
		
		public static function tableName()
	    {
	        return 'faq_questions';
	    }
	    
	    public function items($id_category)
	    {
		    $questions = FaqQuestions::find()
		    ->select(["q.value as question", "a.value as answer"])
		    ->where(['category_id' => $id_category])
		    ->leftJoin("faq_categories_relative", "`question_id` = `faq_questions`.`id`")
		    ->leftJoin("languages_data as q", "`q`.`table` = 'faq_questions' AND `q`.`record_id` = `faq_questions`.`id` AND `q`.`column` = 'question'")
		    ->leftJoin("languages_data as a", "`a`.`table` = 'faq_questions' AND `a`.`record_id` = `faq_questions`.`id` AND `a`.`column` = 'answer'")
		    ->all();
		    
		    return $questions;
	    }
	}