<?php
	
	namespace common\models;
	
	use Yii;
	
	class ActivitiesLastMinute extends \yii\db\ActiveRecord
	{	

		public static function tableName()
	    {
	        return 'stays_last_minute';
	    }
	    
	    public function getActivities()
	    {
	        return $this->hasOne(Activities::className(), ['stays.id' => 'stays_id']);
	    }
	}