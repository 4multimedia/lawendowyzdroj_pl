<?php
	
	namespace common\models;
	
	use Yii;
	
	class RoomsBestDeal extends \yii\db\ActiveRecord
	{			
		public static function tableName()
	    {
	        return 'objects_best_deal';
	    }
	    
	    public function getRoom()
	    {
	        return $this->hasOne(Rooms::className(), ['objects.id' => 'object_id']);
	    }
	}