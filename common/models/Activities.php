<?php
	
	namespace common\models;
	
	use Yii;
	use yii\imagine\Image;
	
	class Activities extends \yii\db\ActiveRecord
	{
		public $image;
		public $date_from;
		public $date_to;
		public $stay_id;
		public $stays_date_id;
		
		public static function tableName()
	    {
	        return 'stays';
	    }
	    
	    public function getImage($width, $height, $cache_name = '')
	    {
		    $Image = Gallery::find()->where(['type' => 'stays', 'object_id' => $this->id])->orderBy('priority ASC')->limit(1)->one();
		    
		    $ext = explode(".", $Image->path);
		    $ext = $ext[count($ext)-1];
		    if ($cache_name)
		    {
			    $cache = '/img/gallery/'.str_replace(" ", "_", strtolower($cache_name.'_'.$width.'_'.$height)).".".$ext;
		    }
		    $cache_path = Yii::getAlias('@webroot').$cache;
		    if (!file_exists($cache_path))
		    {
		    	Image::thumbnail('@webroot'.urldecode($Image->path), $width, $height)->save($cache_path, ['quality' => 100]);
		    }
		    
	        $cache = '/img/gallery/'.str_replace(" ", "_", strtolower($cache_name.'_'.$width.'_'.$height)).".".$ext;
	        
	        return $cache;
	    }
	    
	    public static function image($path, $width, $height, $cache_name = '')
	    {
		    $ext = explode(".", $path);
		    $ext = $ext[count($ext)-1];
		    if ($cache_name)
		    {
			    $cache = '/img/gallery/'.str_replace(" ", "_", strtolower($cache_name.'_'.$width.'_'.$height)).".".$ext;
		    }
		    $cache_path = Yii::getAlias('@webroot').$cache;
		    if (!file_exists($cache_path))
		    {
		    	Image::thumbnail('@webroot'.urldecode($path), $width, $height)->save($cache_path, ['quality' => 100]);
		    }
		    
		    return $cache;
	    }
	    
	    public static function period($begin, $end)
	    {
		    list($year_from, $month_from, $day_from) = explode("-", $begin);
			list($year_to, $month_to, $day_to) = explode("-", $end);
			
			if ($month_from != $month_to)
			{
				return $day_from.' '.\Yii::$app->general->month($month_from).' - '.$day_to.' '.\Yii::$app->general->month($month_to).' '.$year_to;
			}
			else
			{
				return $day_from.'-'.$day_to.' '.\Yii::$app->general->month($month_from).' '.$year_to;
			}
	    }
	    
	    public function getDates()
	    {
	        return $this->hasMany(ActivitiesDates::className(), ['stays_id' => 'id'])->where("date_from >= '".date("Y-m-d")."'")->orderBy(['date_from' => 'ASC']);
	    }
	    
	    public function getWeek()
	    {
		    $weeks = (strtotime($this->date_to) - strtotime($this->date_from)) / (7 * 24 * 3600);
			if ($weeks == 1)
			{
				return "tydzień";
			}
			else if ($weeks == 2)
			{
				return "2 tygodnie";
			}
			else if ($weeks == 3)
			{
				return "3 tygodnie";
			}
	    }
	}