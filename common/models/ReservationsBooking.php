<?php
	
	namespace common\models;
	
	use Yii;
	use yii\imagine\Image;
	
	class ReservationsBooking extends \yii\db\ActiveRecord
	{	
		public $date_end;
		
		public static function tableName()
	    {
	        return 'reservations_booking';
	    }
	
		public static function reserved()
		{
			$array = array();
			
			$items = self::find()
		    ->select("*, (select date FROM reservations_booking rb WHERE reservations_booking.key = rb.key ORDER BY rb.date DESC LIMIT 1) as date_end")
		    ->from("(select * FROM reservations_booking ORDER BY date ASC) as reservations_booking")
		    ->where("(date >= '2017-02-01' AND date <= '2017-02-28') and object_type = 'object'")
		    ->groupBy("`key`")
		    ->orderBy("object_id asc, date asc")
		    ->all();

			if ($items)
			{
				foreach ($items as $item)
				{
					$array[$item->object_id][$item->date] = $item->key;
					
					$array[$item->object_id][$item->key]["date"] = $item->date;
					$array[$item->object_id][$item->key]["date_end"] = $item->date_end;
					$array[$item->object_id][$item->key]["half"] = $item->half;
				}	
			}
			
			return $array;
		}
		
		public static function days($start, $end)
		{
			$start = strtotime($start);
			$end = strtotime($end);
			
			return $days_between = ceil(abs($end - $start) / 86400);
		}
	}