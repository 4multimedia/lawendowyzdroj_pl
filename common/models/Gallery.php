<?php
	
	namespace common\models;
	
	use Yii;
	
	class Gallery extends \yii\db\ActiveRecord
	{			
		public static function tableName()
	    {
	        return 'objects_gallery';
	    }
	}