<?php
	
	namespace common\models;
	
	use Yii;
	use yii\base\Model;
	
	class BestDeal extends Model
	{
	    public static function maxDiscount($type)
	    {
		    if ($type == "activities")
		    {
			    $Item = ActivitiesBestDeal::find()
			    ->where("date_from > NOW()")
			    ->leftJoin("stays_dates", "stays_date_id = stays_dates.id")
			    ->orderBy('discount DESC')
			    ->one();
		    }
		    
		    if ($Item)
			{
				return $Item->discount + $Item->activities->price_discount_random_room;
			}
	    }
	}