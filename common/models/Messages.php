<?php
	
	namespace common\models;
	
	use Yii;
	use yii\imagine\Image;
	use yii\db\ActiveRecord;
	
	class Messages extends ActiveRecord
	{
		public static function tableName()
	    {
	        return 'messages';
	    }
	}