<?php
	
	namespace common\models;
	
	use Yii;
	use yii\imagine\Image;
	
	class Rooms extends \yii\db\ActiveRecord
	{
		public $image;
		public $amenities;
		
		public static function tableName()
	    {
	        return 'objects';
	    }
	    
	    public function getImage($width, $height, $cache_name = '')
	    {
		    $Image = Gallery::find()->where(['type' => 'objects', 'object_id' => $this->id])->orderBy('priority ASC')->limit(1)->one();
		    
		    $ext = explode(".", $Image->path);
		    $ext = $ext[count($ext)-1];
		    if ($cache_name)
		    {
			    $cache = '/img/gallery/'.str_replace(" ", "_", strtolower($cache_name.'_'.$width.'_'.$height)).".".$ext;
		    }
		    $cache_path = Yii::getAlias('@webroot').$cache;
		    if (!file_exists($cache_path))
		    {
		    	Image::thumbnail('@webroot'.urldecode($Image->path), $width, $height)->save($cache_path, ['quality' => 100]);
		    }
		    
	        $cache = '/img/gallery/'.str_replace(" ", "_", strtolower($cache_name.'_'.$width.'_'.$height)).".".$ext;
	        
	        return $cache;
	    }
	    
	    public static function image($path, $width, $height, $cache_name = '')
	    {
		    $ext = explode(".", $path);
		    $ext = $ext[count($ext)-1];
		    if ($cache_name)
		    {
			    $cache = '/img/gallery/'.str_replace(" ", "_", strtolower($cache_name.'_'.$width.'_'.$height)).".".$ext;
		    }
		    $cache_path = Yii::getAlias('@webroot').$cache;
		    if (!file_exists($cache_path))
		    {
		    	Image::thumbnail('@webroot'.urldecode($path), $width, $height)->save($cache_path, ['quality' => 100]);		}
		    
		    return $cache;
	    }
	    
	    public static function getAmenities()
	    {
		    return '123'; //ArrayHelper::map(RoomsAmenities::find()->where(['object_id' => $this->id])->all(), 'amenities_id', 'amenities_id');
	    }
	    
	    public static function classesName($class)
	    {
	       if ($class == 1) { return "KOMFORT"; }
	       if ($class == 2) { return "PREMIUM"; }
	       if ($class == 3) { return "PRESTIGE"; }
	    }
	    
	    public function getLastminute()
	    {
	        return $this->hasMany(RoomsLastMinute::className(), ['object_id' => 'id'])->orderBy(['discount' => 'ASC']);
	    }
	}