<?php
	
	namespace common\models;
	
	use Yii;
	use yii\imagine\Image;
	use yii\db\ActiveRecord;
	
	class Pages extends ActiveRecord
	{
		public static function tableName()
	    {
	        return 'pages';
	    }
	}