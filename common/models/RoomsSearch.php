<?php
	
	namespace common\models;

	use Yii;
	use yii\base\Model;
	use yii\data\ActiveDataProvider;
	
	class RoomsSearch extends Rooms
	{
		public function search($params)
		{
			$query = Rooms::find()
			->select([
		    	"*",
				"(select `path` FROM `objects_gallery` WHERE `type` = 'objects' AND `object_id` = `objects`.`id` ORDER BY `priority` ASC LIMIT 1) AS `image`",
				"(select group_concat(amenities_id) FROM `objects_amenities` WHERE object_id = objects.id) AS `amenities`"
			])
		    ->where(['active' => 1]);
		    
		    if ($params["person_from"] AND $params["person_to"])
		    {
			    $query->andFilterWhere(['>=', 'max_person', $params["person_from"]]);
			    $query->andFilterWhere(['<=', 'max_person', $params["person_to"]]);
			}
			if ($params["person_from"] AND !$params["person_to"]) { $query->andFilterWhere(['>=', 'max_person', $params["person_from"]]); }
			if (!$params["person_from"] AND $params["person_to"]) { $query->andFilterWhere(['<=', 'max_person', $params["person_to"]]); }
			if ($params["alias"])
		    {
			    $query->andFilterWhere(['=', 'alias', $params["alias"]]);
			}
			
			if ($params["class"])
		    {
			    $query->andFilterWhere(['=', 'class', $params["class"]]);
			}
			
			$dataProvider = new ActiveDataProvider([
            	'query' => $query,
            	'pagination' => false
			]);
			
			$dataProvider->sort->defaultOrder = ['attractiveness' => SORT_ASC];
			
			if (!($params))
			{
				return $dataProvider;
			}
			
			return $dataProvider;
		}
	}