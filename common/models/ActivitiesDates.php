<?php
	
	namespace common\models;
	
	use Yii;
	
	class ActivitiesDates extends \yii\db\ActiveRecord
	{	
		public $days;
		public $name;
		public $capacity;
		public $image;
		public $main_features;
		public $summary;
		public $stay_id;
		public $alias;
		public $stays_date_id;
		
		public static function tableName()
	    {
	        return 'stays_dates';
	    }
	    
	    public static function last_period()
	    {
		    $Date = ActivitiesDates::find()->orderBy('date_to DESC')->one();
		    return date("Y-m", strtotime($Date->date_to));
	    }
	}