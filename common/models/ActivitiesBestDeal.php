<?php
	
	namespace common\models;
	
	use Yii;
	
	class ActivitiesBestDeal extends \yii\db\ActiveRecord
	{	
		public $date_from;
		public $date_to;
		public $date_id;
		public $days;
		
		public static function tableName()
	    {
	        return 'stays_best_deals';
	    }
	    
	    public function getActivities()
	    {
	        return $this->hasOne(Activities::className(), ['stays.id' => 'stays_id']);
	    }
	}