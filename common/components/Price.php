<?php
	
	namespace common\components;
	
	use Yii;
	use yii\base\Component;
	use yii\base\Event;
	use yii\web\Session;
	use common\models\RoomsPrice;
	
	class Price extends Component
	{	
		const CURRENCY = 'zł';
		
		public function getPrice($object_id, $person, $night)
		{
			$Price = RoomsPrice::find()
			->where(['object_id' => $object_id, 'person' => $person, 'night' => $night])
			->one();
			
			return number_format($Price->price, 0);
		}
	}	