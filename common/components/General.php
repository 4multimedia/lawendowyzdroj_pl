<?php
	
	namespace common\components;
	
	use Yii;
	use yii\base\Component;
	use yii\base\Event;
	use yii\web\Session;
	
	class General extends Component
	{	
		/**
			* GENEROWANIE PRZYJAZNEGO URL-a
		*/
		public function create_alias($text)
		{
			$link = mb_strtolower($text, 'utf-8');
			
			$array = array(
				' ' => '-',
				' - ' => '-',
				'\'' => '',
				', ' => '-',
				',' => '-',
				'(' => '',
				')' => '',
				'à' => 'a',
				'î' => 'i',
				'ü' => 'u',
				'é' => 'e',
				'è' => 'e',
				'.' => '-',
				'ą' => 'a',
				'ę' => 'e',
				'ó' => 'o',
				'ś' => 's',
				'ł' => 'l',
				'ż' => 'z',
				'ź' => 'z',
				'ć' => 'c',
				'ń' => 'n',
				'"' => '',
			);
			
			$link = strtr($link, $array);
			if (substr($link, strlen($link) - 1, 1) == "-") { $link = substr($link, 0, strlen($link) - 1); }
			return $link;
		}
		
		/**
			* SŁOWNIK - Dni tygodnia
     	*/
		public function day($month, $short = false)
		{
			
			
			if ($month == 1) { $full = "Poniedziałek"; $short = "Pn"; }
			elseif ($month == 2) { $full = "Wtorek"; $short = "Wt"; }
			elseif ($month == 3) { $full = "Środa"; $short = "Śr"; }
			elseif ($month == 4) { $full = "Czwartek"; $short = "Cz"; }
			elseif ($month == 5) { $full = "Piątek"; $short = "Pt"; }
			elseif ($month == 6) { $full = "Sobota"; $short = "So"; }
			elseif ($month == 7) { $full = "Niedziela"; $short = "N"; }
			
			if ($short == true)
			{
				return $short;
			}
			else
			{
				return $full;
			}
		}
		
		public function dayFull($day)
		{
			if ($day == 1) { return "Poniedziałek"; }
			elseif ($day == 2) { return "Wtorek"; }
			elseif ($day == 3) { return "Środa"; }
			elseif ($day == 4) { return "Czwartek"; }
			elseif ($day == 5) { return "Piątek"; }
			elseif ($day == 6) { return "Sobota"; }
			elseif ($day == 7) { return "Niedziela"; }
		}
		
		/**
			* SŁOWNIK - Dni miesiąca
     	*/
		public function month($month)
		{
			if ($month == 1) { return "Styczeń"; }
			elseif ($month == 2) { return "Luty"; }
			elseif ($month == 3) { return "Marzec"; }
			elseif ($month == 4) { return "Kwiecień"; }
			elseif ($month == 5) { return "Maj"; }
			elseif ($month == 6) { return "Czerwiec"; }
			elseif ($month == 7) { return "Lipiec"; }
			elseif ($month == 8) { return "Sierpień"; }
			elseif ($month == 9) { return "Wrzesień"; }
			elseif ($month == 10) { return "Październik"; }
			elseif ($month == 11) { return "Listopad"; }
			elseif ($month == 12) { return "Grudzień"; }
		}
		
		public function month_id($month)
		{
			if ($month == "styczen") { return 1; }
			elseif ($month == "luty") { return 2; }
			elseif ($month == "marzec") { return 3; }
			elseif ($month == "kwiecien") { return 4; }
			elseif ($month == "maj") { return 5; }
			elseif ($month == "czerwiec") { return 6; }
			elseif ($month == "lipiec") { return 7; }
			elseif ($month == "sierpien") { return 8; }
			elseif ($month == "wrzesien") { return 9; }
			elseif ($month == "pazdziernik") { return 10; }
			elseif ($month == "listopad") { return 11; }
			elseif ($month == "grudzien") { return 12; }
		}
		
		
		/**
			* POBIERANIA NAZWY DNIA NA PODSTAWIE DATY
		*/
		public function day_from_date($date, $short = false)
		{
			return $day = self::day(date("N", strtotime($date)), $short);
		}
		
		public function price($price, $dec = 0, $currency = 'PLN')
		{
			if (!empty($price))
			{
				return number_format($price, $dec, ',', ' ').' '.$currency;
			}
		}
	}	