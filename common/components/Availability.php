<?php
	
	namespace common\components;
	
	use Yii;
	use yii\base\Component;
	use yii\base\Event;
	use yii\web\Session;
	use common\models\RoomsSearch;
	
	class Availability extends Component
	{	
		
		/**
			* Sprawdzenie dostepności pokoju.
			* Metoda sprawdza stan dostepnośći danego pokoju, pokojów w danym okresie
			*
			* @param date $from - sprawdzenie dostępności od dnia
			* @param date $to - sprawdzenie dostępności do dnia
			* @param array $params - dodatkowe parametry: object_id - id pokoju, class - klasa pokoju, min_person - minimalna liczba osób w pokoju, max_person - maksymalna liczba osób w pokoju, seasson_id - sezon
     	*/
		public function checkRoom($from, $to, $params = array())
		{
			$model = new RoomsSearch();
			$items = $model->search($params);
			
			return $items;
		}
	}	