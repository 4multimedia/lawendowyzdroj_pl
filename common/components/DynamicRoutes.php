<?php
	
namespace common\components;

use Yii;
use yii\base\BootstrapInterface;
use frontend\models\SystemRoutes;

class DynamicRoutes implements BootstrapInterface
{
	public function bootstrap($app)
	{
		/* STRONY TEKSTOWE */
		$routeArray["pl/o-nas"] 																	= "site/aboutus";
		$routeArray["pl/hotel/o-nas"]																= "site/aboutus";
		$routeArray["pl/hotel/pokoje"]																= "rooms/default/list";
		$routeArray["pl/hotel/galeria-zdjec"]														= "site/gallery";
		$routeArray["pl/hotel/opinie-gosci"]														= "site/reviews";
		$routeArray["pl/pomoc"]																		= "site/faq";
		$routeArray["pl/pomoc/<type>"]																= "site/faq";
		$routeArray["pl/regulamin-korzystania-ze-strony-internetowej"]								= "site/page";
		$routeArray["pl/regulamin-rezerwacji-uslugi"]												= "site/page";
		$routeArray["pl/kontakt"]																	= "site/contact";
			
		/* BLOG */
		$routeArray["pl/baza-wiedzy"]																= "pages/blog/categories";
		$routeArray["pl/aktualnosci-i-wydarzenia"]													= "pages/blog/categories";
		$routeArray["pl/busko-zdroj"]																= "pages/advisor";
		
		/* OFERTA */
		$routeArray["pl/nasza-oferta"]																= "offer/calendar";
		$routeArray["pl/nasza-oferta/kalendarz/<month>/<year>"]										= "offer/calendar";
		$routeArray["pl/nasza-oferta/turnusy-lecznicze"]											= "offer/activities";
		$routeArray["pl/nasza-oferta/turnusy-lecznicze/<type>"]										= "offer/activities";
		$routeArray["pl/nasza-oferta/pakiety-weekendowe"]											= "offer/packages";
		$routeArray["pl/nasza-oferta/pakiety-weekendowe/<type>"]									= "offer/packages";
		$routeArray["pl/nasza-oferta/zabiegi"]														= "offer/treatments";
		$routeArray["pl/nasza-oferta/zabiegi/<alias>"]												= "offer/treatments";
		$routeArray["pl/nasza-oferta/pokoje"]														= "offer/rooms";
		$routeArray["pl/nasza-oferta/pokoje/<type>"]												= "offer/rooms";
		$routeArray["pl/nasza-oferta/pokoje/<type>/<class>"]										= "offer/rooms";
		$routeArray["pl/nasza-oferta/pokoje/<type>/<class>/<seasson>"]								= "offer/rooms";
		$routeArray["pl/nasza-oferta/cennik"]														= "offer/pricelist/rooms";
		$routeArray["pl/nasza-oferta/cennik/nasze-pokoje"]											= "offer/pricelist/rooms";
		$routeArray["pl/nasza-oferta/cennik/nasze-pokoje/<persons>/<class>/<seassons>"]				= "offer/pricelist/rooms";
		$routeArray["pl/nasza-oferta/cennik/turnusy-lecznicze"]										= "offer/pricelist/activities";
		$routeArray["pl/nasza-oferta/cennik/turnusy-lecznicze/<persons>/<class>/<category>"]		= "offer/pricelist/activities";
		$routeArray["pl/nasza-oferta/cennik/pakiety-weekendowe"]									= "offer/pricelist/package";
		$routeArray["pl/nasza-oferta/cennik/pakiety-weekendowe/<persons>/<class>/<category>"]		= "offer/pricelist/package";
		$routeArray["pl/nasza-oferta/cennik/zabiegi"]												= "offer/pricelist/treatments";
		$routeArray["pl/nasza-oferta/oferty-promocyjne"]											= "offer/promotions";
		
		/* TURNUSY LECZNICZE */
		$routeArray["pl/turnusy-lecznicze/szukaj"]													= "activities/default/search";
		$routeArray["pl/turnus-leczniczy/<alias>"]													= "activities/default/view";
		
		/* POKOJE */
		$routeArray["pl/nasze-pokoje/szukaj"]														= "rooms/default/list";
		$routeArray["pl/noclegi"]																	= "rooms/default/list";
		$routeArray["pl/pokoj/<alias>"]																= "rooms/default/view";
		
		$routeArray["pl/nasze-pokoje/oferty-specjalne/last-minute"]									= "specialoffer/rooms/lastminute";
		$routeArray["pl/turnusy-lecznicze/oferty-specjalne/super-oferty"]							= "specialoffer/activities/bestdeal";
		
		/* ZABIEGI */
		$routeArray["pl/zabiegi-medyczne"]															= "treatments/default/index";
		
		/* SYSTEM */
		$routeArray["pl/szukaj"]																	= "system/search";
		
		/* PROFIL */
		$routeArray["pl/rejestracja/<token>"]														= "site/confirmregistration";
		$routeArray["pl/aktywuj-konto/<token>"]														= "site/activation";
		$routeArray["pl/profil"]																	= "profil";
		$routeArray["pl/profil/rezerwacje"]															= "profil/reservations";
		$routeArray["pl/profil/wyloguj"]															= "profil/default/logout";
		
		$app->urlManager->addRules($routeArray);
	}
}