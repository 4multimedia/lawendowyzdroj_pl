<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
	<style>
		body {
			margin:0;
			font-family:Arial;
			font-size:12px;
			color:#000000;
			line-height:19px;
			background:url("http://lawendowyzdroj.pl/themes/lawendowyzdroj/files/img/bg.gif");
		}
		a:link, a:visited, a:hover {
			color:#5b376f;
			text-decoration:underline;
		}
		.top {
			height:30px;
			line-height:30px;
			background:#5b376f;
			font-size:11px;
		}
		.header {
			height:145px;
			background:url("http://lawendowyzdroj.pl/themes/lawendowyzdroj/files/img/mail_top.png") top center;
			text-align:center;
		}
		.header img {
			border:none;
			margin:20px 0 0 0;
		}
		.wrapper {
			width:580px;
			margin:0 auto;
			padding:0 10px;
		}
		.top ul {
			margin:0;
			padding:0;
			list-style:none;
		}
		.top ul li {
			float:left;
			padding:0 6px;
		}
		.top ul li a:link,
		.top ul li a:visited,
		.top ul li a:hover {
			color:#e6e6e6;
			text-decoration:none;
		}
		.top ul.right {
			float:right;
		}
		h1 {
			font-size:18px;
			text-transform:uppercase;
			color:#8322b4;
			margin:0 0 15px 0;
			padding:0 0 8px 0;
			border-bottom:1px solid #8322b4;
		}
		.content {
			padding:35px 0 0 0;
		}
		.author {
			border-top:1px solid #eee;
			font-size:15px;
			font-family:Georgia, Arial;
			font-style:italic;
			color:#e646a6;
			padding:15px 0 0 0;
			margin:15px 0 0 0;
		}
		.footer {
			background:url("http://lawendowyzdroj.pl/themes/lawendowyzdroj/files/img/footer.png") top center no-repeat;
			padding:70px 0 0 0;
			font-size:11px;
			color:#fff;
		}
		.footer h2 {
			margin:0 0 12px 0;
			font-family:Georgia,Arial;
			font-size:16px;
			color:#e646a6;
			font-weight:normal;
			line-height:16px;
		}
		.copyright {
			height:43px;
			background:url("http://lawendowyzdroj.pl/themes/lawendowyzdroj/files/img/copyright.gif") top left no-repeat #18141b;
			text-align:right;
			font-size:11px;
			line-height:43px;
			color:#ccc;
		}
	</style>
</head>
<body>
    <?php $this->beginBody() ?>
	<div class="top">
		<div class="wrapper">
			<ul>
				<li><a href="/o-nas">O NAS</a></li>
				<li><a href="/kontakt">KONTAKT</a></li>
				<li><a href="/pomoc">POMOC</a></li>
			</ul>
			
			<ul class="right">
				<li><a href="/logowanie">LOGOWANIE</a></li>
				<li><a href="/rejestracja">REJESTRACJA</a></li>
			</ul>
		</div>
	</div>
	<div class="header">
		<img src="http://lawendowyzdroj.pl/themes/lawendowyzdroj/files/img/img_logo.png">
	</div>
	<div class="wrapper content">
    <?= $content ?>
		<div class="author">
			Pozdrawiamy<br>Zespół <b>Lawendowy Zdrój</b>
		</div>
	</div>
	<div class="footer">
		<div class="wrapper">
			<div>
				<h2>Lawendowy Zdrój</h2>
				Lawendowy Zdrój to nowo powstały pensjonat medyczny, który zapadnie w pamięć wielu kuracjoszom ze wzgledu unikalne, zaprojektowane tematycznie wnętrza jak również ze względu na holistyczne podejście do leczenia. Oferujemy zabiegi z wykorzystaniem kąpieli siarkowych oraz metody leczenie z zakresu Holistycznej Medycyny Manualnej.<br><br>
			</div>
		</div>
	</div>
	<div class="copyright">
		<div class="wrapper">
			Lawendowy Zdrój | Copyright &copy; 2015 | All Right Reserved
		</div>
	</div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
