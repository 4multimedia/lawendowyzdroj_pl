<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Messages;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $subject;
    public $body;
    public $verifyCode;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
	        'name'				=> 'Imię i nazwisko',
	        'email'				=> 'Adres e-mail',
	        'phone'				=> 'Telefon',
	        'subject'			=> 'Temat wiadomości',
	        'body'				=> 'Treść wiadomości',
            'verifyCode' 		=> 'Kod weryfikacyjny',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
	    
	    
	    $ip = $_SERVER['REMOTE_ADDR'];
		$host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		
		$Message = new Messages;
		$Message -> date_insert = date("Y-m-d H:i:s");
		$Message -> name = $this->name;
		$Message -> phone = $_POST["ContactForm"]["phone"];
		$Message -> email = $this->email;
		$Message -> subject = $this->subject;
		$Message -> note = $this->body;
		$Message -> ip = $ip;
		$Message -> host = $host;
		$Message -> save();
			
		$Message -> no = $Message -> id.'-'.date("dHi");
		$Message -> save();
		
		$this->phone = $_POST["ContactForm"]["phone"];
		
		Yii::$app->mailer->compose('contact/copy_html', ['model' => $this])
		->setTo($this->email)
		->setFrom('powiadomienia@lawendowyzdroj.pl')
		->setSubject('Kopia Twojej wiadomości z formularza kontaktowego - lawendowyzdroj.pl')
		->send();
	    
        return Yii::$app->mailer->compose('contact/html', ['model' => $this])
		->setTo('info@lawendowyzdroj.pl')
		->setFrom('powiadomienia@lawendowyzdroj.pl')
		->setSubject('Wiadomość z formularza kontaktowego - lawendowyzdroj.pl')
		->send();
    }
}
