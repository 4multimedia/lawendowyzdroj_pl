(function($)
{
	$.fn.lzchat = function(options)
	{ 
		var settings = $.extend(
		{
			refresh: 5000,
			page: '/chat/',
			container: '.box-livechat',
			key: 'empty',
			owner: ''
		}, options);
		
		$(settings.container+" textarea").keydown(function(e)
		{
			if (e.keyCode == 13 && e.shiftKey)
			{
				return true
			}
			else if(e.which == 13)
			{
				_result=this.value;
				if(_result) { sendChat(_result); }
				this.value='';
				return false;
			}
		});
		
		Construct();
		
		function Construct()
		{
			$.getJSON(settings.page, {key : settings.key}, function(data)
			{
				parseChat(data.chat);
				
				setInterval(function()
				{
					refreshChat()
				}, settings.refresh);
			});
		}
		
		function parseChat(data)
		{
			var _talk = '<ul>';
			$.each(data, function(time, item)
			{
				$.each(item, function(person, item)
				{
					_talk += '<li class="'+person+'"><b>'+person+'</b><span>'+item.time+'</span><br>'+item.text+'</li>';
				});
			});
			_talk += '</ul>';
			$(settings.container+" .talk").html(_talk);
			$(settings.container+" .talk").scrollTop(99999999999999999);
		}
		
		function refreshChat()
		{
			$.getJSON(settings.page, {key : settings.key}, function(data)
			{
				parseChat(data.chat);
			});
		}
		
		function sendChat(_result)
		{
			$.post(settings.page+'default/save', {key : settings.key, text : _result, owner : settings.owner}, function(data)
			{
				refreshChat();
			});
		}
	}
		
})(jQuery);