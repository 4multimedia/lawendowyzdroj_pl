$(document).ready(function()
{
	$(".question-faq p.question").click(function()
	{
		if ($(this).closest("li").hasClass("active") == false)
		{
			$(".question-faq li.active").removeClass("active").find(".answer").slideUp(300);
			$(this).closest("li").addClass("active").find(".answer").slideDown(300);
		}
		else
		{
			$(".question-faq li.active").removeClass("active").find(".answer").slideUp();
		}
	});
});