						$("document").ready(function()
						{
							var optionCalendar = "";
							var prepareDate = function(date, classes)
							{
								classes = "date-"+classes;
								var date = date.toString();
								var params = date.split("-");
									
								var month = params[1];
								var month_text = "";
								if (month == 1) { month_text = "stycznia"; }
								else if (month == 2) { month_text = "lutego"; }
								else if (month == 3) { month_text = "marca"; }
								else if (month == 4) { month_text = "kwietnia"; }
								else if (month == 5) { month_text = "maja"; }
								else if (month == 6) { month_text = "czerwca"; }
								else if (month == 7) { month_text = "lipca"; }
								else if (month == 8) { month_text = "sierpnia"; }
								else if (month == 9) { month_text = "września"; }
								else if (month == 10) { month_text = "października"; }
								else if (month == 11) { month_text = "listopada"; }
								else if (month == 12) { month_text = "grudnia"; }
									
								$(".input-"+classes).val(date);
									
								var date = new Date(params[0], params[1], params[2]);
								var day = date.getDay();
									
								var day_text = "";
								if (day == 0) { day_text = "piątek"; }
								else if (day == 1) { day_text = "sobota"; }
								else if (day == 2) { day_text = "niedziela"; }
								else if (day == 3) { day_text = "poniedziałek"; }
								else if (day == 4) { day_text = "wtorek"; }
								else if (day == 5) { day_text = "środa"; }
								else if (day == 6) { day_text = "czwartek"; }
									
								$("."+classes).find("span.monthname").html(month_text+" "+params[0]);
								$("."+classes).find("span.dayname").text(day_text);
								$("."+classes).find("b").html(params[2]);
									
							}
							
							function differenceInDays()
							{
								var date1 = $("input#from").val();
								var date2 = $("input#to").val();
											
								date1 = date1.split('-');
								date2 = date2.split('-');
											
								date1 = new Date(date1[0], date1[1], date1[2]);
								date2 = new Date(date2[0], date2[1], date2[2]);
											
								date1_unixtime = parseInt(date1.getTime() / 1000);
								date2_unixtime = parseInt(date2.getTime() / 1000);
								var timeDifference = date2_unixtime - date1_unixtime;
								var timeDifferenceInHours = timeDifference / 60 / 60;
								var timeDifferenceInDays = timeDifferenceInHours  / 24;
								
								return timeDifferenceInDays;
							}
							
							function checkingDateRange(option)
							{
								var date1 = $("input#from").val();
								var date2 = $("input#to").val();
												
								date1 = date1.split('-');
								date2 = date2.split('-');
												
								date1 = new Date(date1[0], date1[1], date1[2]);
								date2 = new Date(date2[0], date2[1], date2[2]);
												
								date1_unixtime = parseInt(date1.getTime() / 1000);
								date2_unixtime = parseInt(date2.getTime() / 1000);
								var timeDifference = date2_unixtime - date1_unixtime;
								var timeDifferenceInHours = timeDifference / 60 / 60;
								var timeDifferenceInDays = timeDifferenceInHours  / 24;
									
								if (timeDifferenceInDays < 1)
								{
									if (option == "from")
									{
											var new_date = moment($("input#from").val(), "YYYY-MM-DD").add(1, 'days');
											
											var day = new_date.format('DD');
											var month = new_date.format('MM');
											var year = new_date.format('YYYY');
											
											new_date = year+"-"+month+"-"+day;
											prepareDate(new_date, "to");
										}	
										else if (option == "to")
										{
											var new_date = moment($("input#to").val(), "YYYY-MM-DD").subtract(1, 'days');
											
											var day = new_date.format('DD');
											var month = new_date.format('MM');
											var year = new_date.format('YYYY');
											
											new_date = year+"-"+month+"-"+day;
											prepareDate(new_date, "from");
										}
									}
								}
							
							function colorRange(date)
							{
								var dates = new Array();			
								var days = differenceInDays();
								for(var d = 0; d <= days; d++)
								{
									dates.push(moment($("input#from").val()).add(d, 'day').format('YYYY-MM-DD'));
								}
								
								for (var i = 0; i < dates.length; i++)
								{
					            	if (dates[i] == moment(date).format('YYYY-MM-DD'))
					            	{
						            	if (i == 0)
						            	{
											return [true, 'ui-state-range-color'];
										}
										else
										{
											return [true, 'ui-state-range-color'];
										}
									}
					        	}
								return [true];
							}
							
							$(".box-calendar-dropdown, .box-calendar p").click(function()
							{
								var $cal = $(this).parent();
								optionCalendar = $cal.attr("date-option");
								var direction = 'left';
								if ($cal.hasClass("box-calendar-right") == true)
								{
									direction = 'right';
								}

								if ($(".calendar-opacity").length == 0)
								{
									$cal.addClass("box-calendar-active");
									$("body").append('<div class="calendar-opacity" style="width:100%; height:186px; left:0; top:0;"></div>');
									
									var box = $cal.offset();
									var boxwidth = $cal.width();
									
									var box3left = box.left + boxwidth + 30;
									var box3width = $(window).width() - box3left;
									
									var box4height = $(document).height() - 276;
									
									$("body").append('<div class="calendar-opacity" style="width:'+box.left+'px; height:90px; left:0; top:186px;"></div>');
									$("body").append('<div class="calendar-opacity" style="width:'+(box3width-2)+'px; height:90px; left:'+(box3left+2)+'px; top:186px;"></div>');
									$("body").append('<div class="calendar-opacity" style="width:100%; height:'+box4height+'px; left:0; top:276px;"></div>');
									
									$(".elastic-box").fadeOut(200);
									var min_day = 0;
									$("body").append("<div class=\"calendar-ui-box\"><div id=\"datepickertop\"></div></div>");
									if (direction == "left")
									{
										$(".calendar-ui-box").show().css({'left' : box.left});
										min_day = 0;
									}
									else if (direction == "right")
									{
										$(".calendar-ui-box").show().css({'left' : (box3left-518)+'px'});
										min_day = 1;
									}
									
									$("#datepickertop").show().datepicker({
										numberOfMonths: 2,
										dateFormat: "yy-mm-dd",
										minDate: min_day,
										firstDay : 1,
										dayNames: ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota" ],
										dayNamesMin: [ "Nd", "Pn", "Wt", "Śr", "Cz", "Pt", "So" ],
										monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
										nextText: "Kolejny miesiąc",
										prevText: "Poprzedni miesiąc",
										beforeShowDay: colorRange,
										onSelect: function( selectedDate )
										{
											prepareDate(selectedDate, optionCalendar);
											$("#datepickertop").hide();
											$(".calendar-opacity").fadeOut(200, function() { $(".calendar-opacity").remove(); });
											$(".elastic-box").fadeIn(200);
											$(".box-calendar-active").removeClass("box-calendar-active");
											$(".calendar-ui-box").remove();
											
											checkingDateRange(optionCalendar);
											
											if (optionCalendar == "to")
											{
												$("#from").datepicker( "option", "maxDate", selectedDate);
											}
											//$(".nights_all").text(differenceInDays());
										}
									});
								}
							});
							
							$(document).on("click", ".calendar-opacity", function()
							{
								$(".calendar-opacity").fadeOut(200, function() { $(".calendar-opacity").remove(); });
								$(".elastic-box").fadeIn(200);
								$(".box-calendar-active").removeClass("box-calendar-active");
								$(".calendar-ui-box").remove();
							});
							
							var $open_checked = false;
							var $open_advanced = false;
							
							$(document).click(function(event)
							{ 
								if(!$(event.target).closest('.box-select-checked').length && !$(event.target).is('.box-select-checked') && $open_checked == true)
								{
									if($('.box-select-checked').is(":visible"))
									{
										$('.box-select-checked').slideUp(300, function() { $open_checked = false; });
									}
		   						}        
							});
							
							$(document).click(function(event)
							{ 
								if(!$(event.target).closest('.advanced-dropdown-roll').length && !$(event.target).is('.advanced-dropdown-roll') && $open_advanced == true)
								{
									if($('.advanced-dropdown-roll').is(":visible"))
									{
										$('.advanced-dropdown-roll').slideUp(300, function() { $open_advanced = false; });
									}
		   						}        
							});
							
							$(".box-select-target-dropdown").click(function()
							{
								$(".alert-search-top").hide();
								var $dropdown = $(this).parent().find(".box-select-checked");
								var $dropdown_advanced = $(".advanced-dropdown-roll").css('display');
								if ($dropdown_advanced != "none") { $(".advanced-dropdown-roll").slideUp(300); $open_advanced = false; }
								if ($dropdown.css('display') == 'none')
								{
									$dropdown.slideDown(300, function() { $open_checked = true; });
								}
								else
								{
									$dropdown.slideUp(300, function() { $open_checked = false; });
								}
							});
							
							
							$(".advanced-dropdown").click(function()
							{
								$(".alert-search-top").hide();
								var $dropdown = $(".advanced-dropdown-roll");
								var $dropdown_checked = $(".box-select-checked").css('display');
								if ($dropdown_checked != "none") { $(".box-select-checked").slideUp(300); $open_checked = false; }
								if ($dropdown.css('display') == 'none')
								{
									$dropdown.slideDown(300, function() { $open_advanced = true; });
								}
								else
								{
									$dropdown.slideUp(300, function() { $open_advanced = false; });
								}
							});
							
							
							
							
							$(".col-checked li").click(function()
							{
								var $active = $(this).hasClass("active");
								if ($active == false)
								{
									$(this).addClass("active");
									$(".col-checked-only").removeClass("active");
									$(".box-select-target-dropdown").text('Pobyt z zabiegami medycznymi');
									$(this).find("input").prop("checked", true);
									$(".col-checked-only").find("input").prop("checked", false);
									
									//$("form#search-property").attr({'action' : '/pl/turnusy-lecznicze/szukaj'});
									$(".add-adults").show();
									$countAdvOption();
								}
								else
								{
									$(this).removeClass("active");
									$(this).find("input").prop("checked", false);
									$(".add-adults").show()
								}
								
								var $targets = $(".box-select-checked input:checked").length;
								if ($targets == 0)
								{
									$(".box-select-target-dropdown").text('Wybierz cel pobytu');
									$(".add-adults").show();
									$(".box-target b").css({'display' : 'none'});
								}
							});
							
							var $countAdvOption = function()
							{
								var $add = 0;
								// zliczanie opcji dodatkowych
								var $compan = parseInt($(".advanced-dropdown-roll input:hidden").val());
								if ($compan > 0)
								{
									if ($(".spinner.add-adults").css("display") == "block")
									{
										$add = $add+1;
									}
								}
								$add = $add + $(".advanced-dropdown-roll input:checked").length;
								if ($add == 0)
								{
									$(".advanced-count").hide();
								}
								else
								{
									$(".advanced-count").show();
									$(".advanced-count b").text($add);
								}
							}
							
							$(".advanced-dropdown-roll ul li").click(function()
							{
								var $active = $(this).hasClass("active");
								if ($active == false)
								{
									$(this).addClass("active");
									$(this).find("input").prop("checked", true);
								}
								else
								{
									$(this).removeClass("active");
									$(this).find("input").prop("checked", false);
								}
								$countAdvOption();
							});
							
							$(".col-checked-only").click(function()
							{
								var $active = $(this).hasClass("active");
								if ($active == false)
								{
									$(this).addClass("active");
									$(".col-checked li.active").removeClass("active");
									$(".box-select-target-dropdown").text('Pobyt bez zabiegów medycznych');
									$(this).find("input").prop("checked", true);
									$(".col-checked").find("input").prop("checked", false);
									
									//$("form#search-property").attr({'action' : '/pl/noclegi'});
									$(".add-adults").hide();
									$countAdvOption();
									
									$(".box-target b").css({'display' : 'none'});
								}
								else
								{
									$(this).find("input").prop("checked", false);
									$(this).removeClass("active");
									$(".add-adults").show()
								}
								
								var $targets = $(".box-select-checked input:checked").length;
								if ($targets == 0)
								{
									$(".box-select-target-dropdown").text('Wybierz cel pobytu');
									$(".add-adults").show();
									$(".box-target b").css({'display' : 'none'});
								}
							});
							
							$(".col-checked li.all").click(function()
							{
								var $status = $(this).hasClass("active");
								if ($status == true)
								{
									$(this).parent().find("input").prop("checked", true);
									$(this).parent().find("li").addClass("active");
								}
								else
								{
									$(this).parent().find("input").prop("checked", false);
									$(this).parent().find("li").removeClass("active");
								}
								
								$selected_one = $(".col-checked li.one.active").length;
								if ($selected_one > 0)
								{
									$(".box-target b").css({'display' : 'block'}).find("u").text($selected_one);
								}
								else
								{
									$(".box-target b").css({'display' : 'none'});
								}
							});
							
							$(".col-checked li.one").click(function()
							{
								$all = $(this).parent().find(".one").length;
								$selected = $(this).parent().find(".one.active").length;
								
								if ($all == $selected)
								{
									$(this).parent().find("li.all input").prop("checked", true);
									$(this).parent().find("li.all").addClass("active");
								}
								else
								{
									$(this).parent().find("li.all input").prop("checked", false);
									$(this).parent().find("li.all").removeClass("active");
								}
								
								$selected_one = $(".col-checked li.one.active").length;
								if ($selected_one > 0)
								{
									$(".box-target b").css({'display' : 'block'}).find("u").text($selected_one);
								}
								else
								{
									$(".box-target b").css({'display' : 'none'});
								}
							});
							
							$("form#search-property").submit(function()
							{
								$(".alert-search-top").hide();
								var errors = 0;
								var alerts = '';
								var $all = parseInt($('input[name="women"]').val()) + parseInt($('input[name="men"]').val());
								var $targets = $(".box-select-checked input:checked").length;
								if ($all == 0)
								{
									errors = 1;
									alerts += 'Zaznacz kobietę i/lub mężczyznę<br>';
								}
								if ($targets == 0)
								{
									errors = 1;
									alerts += 'Wybierz cel pobytu';
								}
								
								if (errors > 0) {  $(".alert-search-top").show().html(alerts); return false; }
							});
							
							$("span.spinner em").click(function()
							{
								var $val = $(this).parent().find("u").text();
								$val = parseInt($val);
								
								var $action = "plus";
								if ($(this).hasClass("minus")) { $action = "minus"; }
								
								var $max = $(this).parent().attr("data-max");
								if ($action == "plus")
								{
									$val = $val+1;
								}
								
								if ($action == "minus")
								{
									$val = $val-1;
								}
								
								if ($val > $max) { $val = $max; }
								if ($val < 0) { $val = 0; }
								
								$(this).parent().find("u").text($val);
								$(this).parent().find("input").val($val);
								
								if ($(this).parent().parent().parent().hasClass("add-adults") == true)
								{
									$countAdvOption();
								}
							});
						});