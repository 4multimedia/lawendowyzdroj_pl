$(document).ready(function()
{
    $('.contact-maps')
    .gmap3({
        address: "ul. Korczyńska 25, Busko Zdrój - Zbludowice",
        zoom: 13,
        mapTypeId: "shadeOfGrey",
        // to select it directly
        mapTypeControlOptions: {
            mapTypeIds: ["shadeOfGrey"]
        }
    })
    .styledmaptype(
    "shadeOfGrey",
    [{
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#e3d0ec"
        }]
    }, {
        "featureType": "transit",
        "stylers": [{
            "color": "#8c7398"
        }, {
            "visibility": "off"
        }]
    }, {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#af93bd"
        }]
    }, {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#ffffff"
        }]
    }, {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#ffffff"
        }, {
            "weight": 1.8
        }]
    }, {
        "featureType": "road.local",
        "elementType": "geometry.stroke",
        "stylers": [{
            "color": "#d0b6dd"
        }]
    }, {
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#e1cbec"
        }]
    }, {
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [{
            "color": "#a48db0"
        }]
    }, {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#ffffff"
        }]
    }, {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#ffffff"
        }]
    }, {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#e6d8ed"
        }]
    }, {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#5f4c69"
        }]
    }, {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#6d3489"
        }]
    }, {
        "featureType": "poi",
        "elementType": "labels.icon",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "road.arterial",
        "elementType": "geometry.stroke",
        "stylers": [{
            "color": "#c9a2dc"
        }]
    }, {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [{
            "visibility": "off"
        }]
    }, {}, {
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#c88be6"
        }]
    }],
    {
        name: "Mapa"
    }
    )
    .marker([
    {
        address: "ul. Korczyńska 25, Busko Zdrój - Zbludowice",
        icon: "/img/icon/marker.png"
    }
    ]);
});

