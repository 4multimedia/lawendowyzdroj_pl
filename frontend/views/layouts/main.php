<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\widgets\HeaderSearch;

use frontend\widgets\modal\Login;
use frontend\widgets\modal\Register;

use venkatesanchinna\yii2livechat\assets\LivechatAsset;

AppAsset::register($this);

// \Yii::$app->language = 'en-US';

$this->registerJs("$().lzchat({'key' : 'sdasdasdasd', 'owner' : 'me'});"); 

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="loader">
	<img src="/img/gfx/home-loader.gif">
</div>
<header>
	<div class="top">
		<div class="container">
			<ul class="navbar-header navbar-left">
				<li><a href="/"><?=Yii::t('app', 'Start');?></a></li>
				<li class="dropdown">
					<a href=""><?=Yii::t('app', 'Hotel');?></a>
					<ul>
						<li><a href="/pl/hotel/o-nas"><?=Yii::t('app', 'O nas');?></a></li>
						<li><a href="/pl/hotel/galeria-zdjec"><?=Yii::t('app', 'Galeria zdjęć i Video');?></a></li>
						<li><a href="/pl/hotel/pokoje"><?=Yii::t('app', 'Nasze pokoje');?></a></li>
						<li><a href="/pl/hotel/opinie-gosci"><?=Yii::t('app', 'Opinie gości');?></a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href=""><?=Yii::t('app', 'Informacje i wydarzenia');?></a>
					<ul>
						<li><a href="/pl/baza-wiedzy"><?=Yii::t('app', 'Baza wiedzy');?></a></li>
						<li><a href="/pl/aktualnosci-i-wydarzenia"><?=Yii::t('app', 'Aktualności i ogłoszenia');?></a></li>
						<li><a href="/pl/busko-zdroj"><?=Yii::t('app', 'Informacje o Busko-Zdroju');?></a></li>
					</ul>
				</li>
			</ul>
			
			<a href="/pl/nasza-oferta" class="offer">
				<?=Yii::t('app', 'Nasza oferta');?> <small><?=Yii::t('app', 'Poznaj nasze pakiety i ceny');?></small>
			</a>
			
			<ul class="navbar-header navbar-right">
				<li class="dropdown">
					<a href=""><?=Yii::t('app', 'Panel Klienta');?></a>
					<ul>
						<?php if (Yii::$app->user->isGuest) : ?>
							<li><a href="javascript:;" data-toggle="modal" data-target="#login-modal"><?=Yii::t('app', 'Zaloguj się');?></a></li>
							<li><a href="javascript:;" data-toggle="modal" data-target="#login-register"><?=Yii::t('app', 'Zarejestruj się');?></a></li>
							<li><a href="javascript:;" data-toggle="modal" data-target="#recover-modal"><?=Yii::t('app', 'Przypomnij hasło');?></a></li>
						<?php else : ?>
							<li><a href="/pl/profil"><?=Yii::t('app', 'Ustawienia konta');?></a></li>
								<li><a href="/pl/profil/rezerwacje"><?=Yii::t('app', 'Rezerwacje');?></a></li>
							<li><?php echo Html::a(Yii::t('app', 'Wyloguj się'), '/pl/profil/wyloguj', ['data-method' => 'post']);?></li>
						<?php endif; ?>
					</ul>
				</li>
				<li><a href="/pl/pomoc"><?=Yii::t('app', 'Pomoc');?></a></li>
				<li><a href="/pl/kontakt"><?=Yii::t('app', 'Kontakt');?></a></li>
				<li class="dropdown">
					<a href=""><img src="/img/gfx/lang_pl.gif"></a>
					<? /*<ul class="dropdown-lang">
						<li><a href="/en"><img src="/img/gfx/lang_en.gif"></a></li>
						<li><a href="/de"><img src="/img/gfx/lang_de.gif"></a></li>
					</ul> */ ?>
				</li>
			</ul>
			<span class="top-contact top-contact-left">info@lawendowyzdroj.pl</span>
			<span class="top-contact top-contact-right">tel. +48 530 138 148</span>
		</div>
	</div>
	<div class="header header-close">
		<div class="header-top">
			<div class="container">
				<a href="/" title="Hotel Lawendowy Zdrój">
					<img src="/img/gfx/logo_lawendowy_zdroj.png" alt="Sanatorium Lawendowy Zdrój" class="logo">
				</a>
				
				<div class="header-contact-links">
					<a href="javascript:;" class="open-contact-form wow fadeInRight animated" data-wow-duration="0.75s" data-wow-delay="1.5s">
						<i class="fa fa-microphone"></i>
						<b>CHAT ONLINE <small><?=Yii::t('app', 'codziennie od 8.00 - 20.00');?></small></b>
					</a>
					<a href="javascript:;" class="open-order-call wow fadeInRight animated" data-wow-duration="0.75s" data-wow-delay="2s">
						<i class="fa fa-phone"></i>
						<b><?=Yii::t('app', 'ZAMÓW ROZMOWĘ');?> <small><?=Yii::t('app', 'oddzwonimy w ciągu godziny');?></small></b>
					</a>
				</div>
			</div>
		</div>
		<div class="header-bottom">
			<div class="search-shadow">
				<a href="javascript:;" id="show-search">
					Zarezerwuj on-line
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</div>
			<div class="container">
				<?= HeaderSearch::widget([]); ?>
			</div>
		</div>
	</div>
</header>

	<?php if (isset($this->params['breadcrumbs']) AND !empty($this->params['breadcrumbs'])) : ?>
        <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?>
    <?php endif; ?>
    
	<?//Alert::widget()
		 ?>
	<?= $content ?>


<div class="footer">
	<div class="container">
		<div class="payments">
			<img src="/img/gfx/payments.gif">
		</div>
		<div class="row">
			<div class="col-md-3 links">
				<p>Lawendowy Zdrój</p>
				<ul>
					<li><a href="/pl/hotel/o-nas">O Sanatorium Lawendowy Zdrój</a></li>
					<li><a href="/pl/pomoc">Pytania i odpowiedzi</a></li>
					<li><a href="/pl/regulamin-korzystania-ze-strony-internetowej">Regulamin strony internetowej</a></li>	
					<li><a href="/pl/regulamin-rezerwacji-uslugi">Regulamin rezerwacji usługi</a></li>
					<li><a href="/pl/mapa-strony">Mapa strony</a></li>
					<li><a href="/pl/hotel/kontakt">Kontakt</a></li>
				</ul>
			</div>
			<div class="col-md-3 links">
				<p>Turnusy lecznicze</p>
				<ul>
					<li><a href="/pl/turnusy-lecznicze">Turnusy lecznicze</a></li>
					<li><a href="/pl/turnusy-lecznicze/kalendarz">Oferta zabiegów medycznych</a></li>
					<li><a href="/pl/turnusy-lecznicze/oferta-specjalna/last-minute">Oferty Last Minute - Turnusy lecznicze</a></li>
					<li><a href="/pl/strefa-spa-i-wellness/oferta-specjalna/last-minute">Oferty Last Minute - SPA &amp; Wellness</a></li>
					<li><a href="/pl/turnusy-lecznicze/oferta-specjalna/best-deal">Super oferty - Turnusy lecznicze</a></li>
					<li><a href="/pl/turnusy-lecznicze/oferta-specjalna/first-minute">Taniej bo wcześniej - Turnusy lecznicze</a></li>
				</ul>
			</div>
			<div class="col-md-3 links">
				<p>Noclegi - Busko Zdrój</p>
				<ul>
					<li><a href="/pl/noclegi">Wyszukaj nocleg</a></li>
					<li><a href="/pl/noclegi/oferta-specjalna/last-minute">Oferta Last minute</a></li>
					<li><a href="/pl/noclegi/oferta-specjalna/best-deal">Super oferty</a></li>
					<li><a href="/pl/noclegi/oferta-specjalna/first-minute">Taniej bo wcześniej</a></li>
					<li><a href="/pl/noclegi/oferta-specjalna/short-gaps">Nie czekaj na weekend</a></li>
					<li><a href="/pl/regulamin-rezerwacji-obiektu">Regulamin rezerwacji obiektu</a></li>
				</ul>
			</div>
			<div class="col-md-3 links links-last">
				<a href="https://www.facebook.com/lawendowyzdroj/" class="footer-fb" target="_blank"><i class="fa fa-facebook"></i></a>
				<a href="https://www.youtube.com/channel/UC2WL1TyBNjrV1j057PIHMeg" class="footer-yt" target="_blank"><i class="fa fa-youtube"></i></a>
			</div>
		</div>
	</div>
</div>

<div class="copyright">
	Lawendowy Zdrój - Medycyna &amp; SPA | Copyright &copy; 2015-<?=date("Y");?>. All Rights Reserved
</div>


<div class="box-livechat">
	<div class="box-livechat-form">
		<div class="avatar">
			<div class="image"></div>
			<div>
				<p>Anna Nowak</p>
				<span>Recepcjonistka Lawendowy Zdrój</span>
			</div>
		</div>
		<div class="talk">
			
		</div>
		<textarea placeholder="Wpisz wiadomość..."></textarea>
	</div>
	<a href="" class="btn-livechat"><i class="fa fa-comments-o" aria-hidden="true"></i></a>
</div>

<?php echo Login::widget([]) ?>
<?php echo Register::widget([]) ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>