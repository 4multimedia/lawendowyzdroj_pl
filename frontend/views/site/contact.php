<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Kontakt z nami - Hotel Lawendowy Zdrój | Busko Zdrój';

	$this->registerJsFile('http://maps.google.com/maps/api/js?sensor=false&amp;amp;language=pl', ['depends' => [\yii\web\JqueryAsset::className()]]);
	$this->registerJsFile('/js/gmap.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
	$this->registerJsFile('/js/contact.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
	
?>
<div class="container">
	<div class="lead-header lead-header-slim">
		<h2>Kontakt z nami <small>Hotel Lawendowy Zdrój</small></h2>
		<img src="/img/page/header_about_us.png">
	</div>
	
	<div class="content-page content-text-cms">
		<div class="box-special">
			
			<?php
				
			if (Yii::$app->session->getFlash('success')) :
		        echo '<div class="alert alert-success text-center">'.Yii::$app->session->getFlash('success').'</div>';
		    endif;
		    if (Yii::$app->session->getFlash('danger')) :
		        echo '<div class="alert alert-danger text-center">'.Yii::$app->session->getFlash('danger').'</div>';
		    endif;
				
			?>
			
			<div class="row">
		        <div class="col-lg-7">
		            <?php
			            
			            $form = ActiveForm::begin([
							'id' => 'site-contact-form',
							'layout' => 'horizontal',
							'fieldConfig' => [
					        	'horizontalCssClasses' => [
					            	'label' => 'col-sm-4',
									'offset' => 'col-sm-offset-4',
									'wrapper' => 'col-sm-8',
									'error' => '',
									'hint' => '',
								],
							],
						]);	
					?>
		
		                <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
		
		                <?= $form->field($model, 'email') ?>
		                <?= $form->field($model, 'phone') ?>
		
		                <?= $form->field($model, 'subject') ?>
		
		                <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
		
		                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
		                    'template' => '<div class="row"><div class="col-lg-6">{input}</div><div class="col-lg-4">{image}</div></div>',
		                ]) ?>
		
		                <div class="form-group">
			                <div class="row">
				                <div class="col-sm-offset-4 col-md-8"> 
			                    	<?= Html::submitButton('Wyślij wiadomość', ['class' => 'btn btn-primary red', 'style' => 'border:1px solid #a22a60', 'name' => 'contact-button']) ?>
				                </div>
			                </div>
		                </div>
		
		            <?php ActiveForm::end(); ?>
		        </div>
		        <div class="col-md-5">
			        <div class="contact-address">
				        <p>Lawendowy Zdrój</p>
				        <span>
				        	<i class="fa fa-map-marker" aria-hidden="true"></i>
				        	Medycyna & SPA<br>ul. Korczyńska 25<br>Busko Zdrój - Zbludowice
				        </span>
				        <span>
				        	<i class="fa fa-envelope" aria-hidden="true"></i>
				        	info@lawendowyzdroj.pl
				        </span>
				        <span>
				        	<i class="fa fa-phone" aria-hidden="true"></i>
				        	+48 530 138 148<br>+48 41 241 22 10
				        </span>
				        <ul>
					        <li>
					        	<a href="https://www.facebook.com/lawendowyzdroj/" class="footer-fb" target="_blank"><i class="fa fa-facebook"></i></a>
					        </li>
							<li>
								<a href="https://www.youtube.com/channel/UC2WL1TyBNjrV1j057PIHMeg" class="footer-yt" target="_blank"><i class="fa fa-youtube"></i></a>
							</li>
				        </ul>
				        </ul>
			        </div>
		        </div>
		        <div class="col-md-12">
			        <div class="contact-maps">
				        
			        </div>
		        </div>
		    </div>
		</div>
	</div>
</div>
