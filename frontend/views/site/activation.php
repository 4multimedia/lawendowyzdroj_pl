<?php

	use yii\helpers\Html;

	$this->title = 'Konto zostało aktywowane';
	
?><div class="container">

	
	<div class="content-page">
		<div class="box-special text-center">
			<div class="m-t-b-50">
				<h3><span class="text-red">Twoje konto zostało aktywowane.</span><br>Możesz zalogować się do swojego profilu!</h3>
			</div>
		</div>
	</div>
	
</div>
