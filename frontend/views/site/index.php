<?php

use yii\web\View;
use frontend\widgets\SpecialOffer;

$this->title = 'Sanatorium i Hotel Lawendowy Zdrój -  Medycyna & Spa | Busko Zdrój';

$this->registerJsFile('/js/uikit.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/slideshow.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/slideshow-fx.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile('/css/slide.css');

?>

<!-- START Fullscreen block -->
<div class="tm-fullscreen uk-position-relative">
	<div class="akslider-module ">
		<div class="uk-slidenav-position" data-uk-slideshow="{height: 'auto', animation: 'scale', duration: '500', autoplay: false, autoplayInterval: '7000', videoautoplay: true, videomute: true, kenburns: false}">
                        <ul class="uk-slideshow">
                            <li class="uk-cover uk-height-viewport uk-active">
                                <!-- START Slide 1 -->
                                <div class="uk-cover-background uk-position-cover" style="background-image: url(/img/slider/fd9f88dcbaf5df7d72b73e15efdbaa81.jpg);"></div>
                                <img src="images/demo/slider/slide_1.jpg" width="800" height="400" alt="Go to Gym">
                                <div class="uk-caption uk-caption-panel uk-animation-fade uk-flex uk-flex-center uk-flex-middle">
                                    <div class="tm-slide-style-1">
                                        <div class="slide-text-primary tm-heading-font">
                                            <a href="#">Nowoczesny sprzęt</a>
                                        </div>
                                        <h4 class="slide-subhead text-center tm-heading-font">
                                            Najnowocześniejszy sprzęt w pięknych wnętrzach gwarantuje wysoką skuteczność i zmysłową przyjemność</sup>
                                        </h4>
                                    </div>
                                </div>
                                <!-- END Slide 1 -->
                            </li>
                            <li class="uk-cover uk-height-viewport">
                                <!-- START Slide 2 -->
                                <div class="uk-cover-background uk-position-cover" style="background-image: url(/img/slider/3b1a3f3ea88103321a9f070fc510a652.jpg);"></div>
                                <img src="images/demo/slider/slide_2.jpg" width="800" height="400" alt="Kickboxing">
                                <div class="uk-caption uk-caption-panel uk-animation-fade uk-flex uk-flex-center uk-flex-middle">
                                    <div class="tm-slide-style-1">
                                        <div class="slide-text-primary tm-heading-font">
                                            <a href="#">Tematyczne pokoje</a>
                                        </div>
                                        <h4 class="slide-subhead text-center tm-heading-font">
                                            Tematycznie wnętrza Lawendowego Zdroju zaprojektowano z dbałością<br> o najmniejsze detale - każdy pokój jest wyjątkowy
                                        </h4>
                                    </div>
                                </div>
                                <!-- END Slide 2 -->
                            </li>
                            <li class="uk-cover uk-height-viewport">
                                <!-- START Slide 2 -->
                                <div class="uk-cover-background uk-position-cover" style="background-image: url(/img/slider/21935e2bf755e34a1b74b5c26f4131c6.jpg);"></div>
                                <img src="images/demo/slider/slide_2.jpg" width="800" height="400" alt="Kickboxing">
                                <div class="uk-caption uk-caption-panel uk-animation-fade uk-flex uk-flex-center uk-flex-middle">
                                    <div class="tm-slide-style-1">
                                        <div class="slide-text-primary tm-heading-font">
                                            <a href="#">Kameralna strefa SPA</a>
                                        </div>
                                        <h4 class="slide-subhead text-center tm-heading-font">
                                            Wycisz się i zrelaksuj w nowoczesnej i kameralnej strefie Wellness Lawendowego Zdroju.
                                        </h4>
                                    </div>
                                </div>
                                <!-- END Slide 2 -->
                            </li>
                            <li class="uk-cover uk-height-viewport">
                                <!-- START Slide 2 -->
                                <div class="uk-cover-background uk-position-cover" style="background-image: url(/img/slider/aa30a92383f952c1a1fbe1af83838663.jpg);"></div>
                                <img src="images/demo/slider/slide_2.jpg" width="800" height="400" alt="Kickboxing">
                                <div class="uk-caption uk-caption-panel uk-animation-fade uk-flex uk-flex-center uk-flex-middle">
                                    <div class="tm-slide-style-1">
                                        <div class="slide-text-primary tm-heading-font">
                                            <a href="#">Nasza restauracja</a>
                                        </div>
                                        <h4 class="slide-subhead text-center tm-heading-font">
                                           Po pierwsze etycznie, po drugie smacznie i zdrowo a po trzecie - klimatycznie!
                                        </h4>
                                    </div>
                                </div>
                                <!-- END Slide 2 -->
                            </li>
                            <li class="uk-cover uk-height-viewport">
                                <!-- START Slide 2 -->
                                <div class="uk-cover-background uk-position-cover" style="background-image: url(/img/slider/7d13f058a65308547a127000515f4975.jpg);"></div>
                                <img src="images/demo/slider/slide_2.jpg" width="800" height="400" alt="Kickboxing">
                                <div class="uk-caption uk-caption-panel uk-animation-fade uk-flex uk-flex-center uk-flex-middle">
                                    <div class="tm-slide-style-1">
                                        <div class="slide-text-primary tm-heading-font">
                                            <a href="#">"Zdrowy kręgosłup"</a>
                                        </div>
                                        <h4 class="slide-subhead text-center tm-heading-font">
                                            Już teraz możesz zarezerwować nasz specjalistyczny turnus leczniczy,<br>który w procesie leczenia bazuje na holistycznej medycynie manualnej
                                        </h4>
                                    </div>
                                </div>
                                <!-- END Slide 2 -->
                            </li>
                        </ul>
                        
                        <!-- START Slider next/prev buttons -->
                        <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                        <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
                        <!-- END Slider next/prev buttons -->
                        
                        <!-- START Slider navigator -->
                        <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-text-center">
                            <li data-uk-slideshow-item="0" class="uk-active"><a href="#" style="background-image: url('/img/slider/slide_thumb_1.jpg')">0</a></li>
                            <li data-uk-slideshow-item="1"><a href="#" style="background-image: url('/img/slider/slide_thumb_2.jpg')">1</a></li>
                            <li data-uk-slideshow-item="2"><a href="#" style="background-image: url('/img/slider/slide_thumb_3.jpg')">2</a></li>
                            <li data-uk-slideshow-item="3"><a href="#" style="background-image: url('/img/slider/slide_thumb_4.jpg')">3</a></li>
                            <li data-uk-slideshow-item="4"><a href="#" style="background-image: url('/img/slider/slide_thumb_5.jpg')">4</a></li>
                        </ul>
                        <!-- END Slider navigator -->

                    </div>
                </div>
    <a href="" class="scroll-slider"><i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
</div>
<!-- END Fullscreen block -->

<h3 class="title-lead violet"><span data-title="Oferty promocyjne hotelu & spa">Oferty promocyjne hotelu & spa</span></h3>

<div class="container">
	<div class="home-box">
		<div class="row">
			<div class="col-md-3">
				<?php echo SpecialOffer::widget(['type' => 'lastminute', 'option' => 'rooms', 'bg' => 5]) ?>
			</div>
			<div class="col-md-3">
				<?php echo SpecialOffer::widget(['type' => 'lastminute', 'option' => 'activities', 'bg' => 1]) ?>
			</div>
			<div class="col-md-3">
				<?php echo SpecialOffer::widget(['type' => 'firstminute', 'option' => 'activities', 'bg' => 10]) ?>
			</div>
			<div class="col-md-3">
				<?php echo SpecialOffer::widget(['type' => 'bestdeal', 'option' => 'activities', 'bg' => 15]) ?>
			</div>
		</div>
	</div>
</div>

<h3 class="title-lead claret"><span data-title="Wydarzenia i informacje">Wydarzenia i informacje</span></h3>

<div class="container">
	<div class="home-news">
		<div class="row">
			<div class="col-sm-6 col-md-3 col-xs-12">
				<div class="box-special">
					<strong>Aktualności</strong>
					<img src="/img/gfx/img_news.png">
					<span>10 październik 2016</span>
					<p>Weekend z czerwonym winem i muzyką hiszpańską</p>
					<div class="more"><a href="">Czytaj więcej</a></div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3 col-xs-12">
				<div class="box-special">
					<strong>Aktualności</strong>
					<img src="/img/gfx/img_news.png">
					<span>10 październik 2016</span>
					<p>Weekend z czerwonym winem i muzyką hiszpańską</p>
					<div class="more"><a href="">Czytaj więcej</a></div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3 col-xs-12">
				<div class="box-special">
					<strong>Aktualności</strong>
					<img src="/img/gfx/img_news.png">
					<span>10 październik 2016</span>
					<p>Weekend z czerwonym winem i muzyką hiszpańską</p>
					<div class="more"><a href="">Czytaj więcej</a></div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3 col-xs-12">
				<div class="box-special">
					<strong>Aktualności</strong>
					<img src="/img/gfx/img_news.png">
					<span>10 październik 2016</span>
					<p>Weekend z czerwonym winem i muzyką hiszpańską</p>
					<div class="more"><a href="">Czytaj więcej</a></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="box-opinion">
	<div class="container">
		<div class="row">
			<div class="col-md-4 text-center">
				<img src="/img/gfx/img_opinion_1.jpg">
			</div>
			<div class="col-md-8">
				<h3>Opinie gości hotelu</h3>
				<p>Jestem pod wielkim wrażeniem tego miejsca, byłam tam zdecydowanie za krótko!<br>
Cudowne miejsce dla ciała i duszy, wspaniała atmosfera, piękne, tematycznie urządzone, wygodne pokoje. Jednak największy plus tego miejsca to wspaniała, pyszna, niebanalna a przede wszystkim zdrowa kuchnia, oparta na ekologicznych, lokalnych produktach. <br><br>
				Dziękuję :-) Nie zmieniajcie się!<br><br>Na pewno będę wracać! </p>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="home-box-border home-box-contact">
				<h4>Kontakt</h4>
				<img src="/img/gfx/logo_small_contact.png" class="contact-logo">
				<div class="line">
					<img src="/img/icon/small_phone.png">
					48 41 241 22 10<br>48 530 138 148
				</div>
				<div class="line">
					<img src="/img/icon/small_envelope.png">
					info@lawendowyzdroj.pl
				</div>
				
				<div class="line">
					<img src="/img/icon/small_location.png">
					ul. Korczyńska 25<br>Busko Zdrój - Zbludowice<br>Polska
				</div>
			</div>
		</div>
		
		<div class="col-md-4">
			<div class="home-box-border home-box-call">
				<h4>Zamów rozmowę</h4>
				
				<div class="line">
					<img src="/img/icon/microphone.png">
					<b>Zamów bezpłatną rozmowę</b><br> Oddzwonimy do Ciebie w <br> ciągu najbliższej godziny!
				</div>
				<div class="line">
					<img src="/img/icon/communication.png">
					<b>Porozmawiaj z recepcją on-line</b><br>Jesteśmy dostępni dla Ciebie <br> codziennie przez 24 godziny!
				</div>
			</div>
		</div>
		
		<div class="col-md-4">
			<div class="home-box-border home-box-newsletter">
				<h4>Newsletter</h4>
				
				<p>Bądź na bieżąco z naszymi nowościami!</p>
				<span>
					<img src="/img/icon/envelope.png">
					Zapisz się do naszego bezpłatnego biuletynu informacyjnego, dzięki któremu żadna nasza nowość w ofercie oraz promocja Cię nie ominie.
				</span>
				
				<form>
					<input type="text" placeholder="Wpisz adres e-mail...">
					<button>+</button>
				</form>
				
			</div>
		</div>
		
		<div class="clearfix"></div>
	</div>
</div>

<div class="container">
	<div class="seo-text">Lawendowy Zdrój to nowo powstały pensjonat medyczny, który zapadnie w pamięć wielu kuracjuszom ze względu na unikalnie zaprojektowane tematyczne wnętrza. Specjalizujemy się w holistycznym podejściu do leczenia. Oferujemy zabiegi z wykorzystaniem kąpieli siarkowych oraz metody leczenie z zakresu Holistycznej Medycyny Manualnej. Dysponujemy rozbudowaną strefą SPA & Wellness z kompleksem saun, jacuzzi, grotą solną oraz restauracją, która serwuje zdrowe i smaczne dania mięsne i wegetariańskie. Zapraszamy do Lawendowego Zdroju na turnusy sanatoryjne, specjalistyczne turnusy lecznicze, warsztaty jogi, psychoterapię indywidualna i grupową, a także relaksacyjne weekendy. Jednym słowem wszystko dla ciała, umysłu i duszy. Z troski o urodę i ciało, w Lawendowym Zdroju powstało kompleksowe centrum SPA & Wellness. Oferujemy lecznicze i relaksacyjne masaże, zabiegi pielęgnacyjne na twarz i ciało. W tym miejscu nasi goście  w atmosferze spokoju i relaksu mogą również skorzystać z jacuzzi, kompleksu saun oraz groty solnej. </div>
</div>

<div class="container">
	<div class="box-special">
		
	</div>
</div>