<?php

	use common\models\FaqQuestions;
	
	$this->registerJsFile('/js/faq.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
	
?><div class="container">
	<div class="lead-header">
		<h2>Pomoc / FAQ <small>Najczęściej zadawane pytania</small></h2>
		<img src="/img/page/header_faq.jpg">
		<img src="/img/gfx/lead_header_arrow.png" class="arrow_bottom">
		
		<div class="lead-tabs lead-tabs-4">
			<ul>
				<li<?php if ($tab == "cat-1") : echo ' class="active"'; endif; ?>><a href="/pl/pomoc"><i class="fa fa-file-text-o" aria-hidden="true"></i> Podstawowe informacje</a></li>
				<li<?php if ($tab == "cat-2") : echo ' class="active"'; endif; ?>><a href="/pl/pomoc/rezerwacje-anulacje-platnosci"><i class="fa fa-credit-card" aria-hidden="true"></i> Rezerwacje, anulacje, płatności</a></li>
				<li<?php if ($tab == "cat-3") : echo ' class="active"'; endif; ?>><a href="/pl/pomoc/przyjazd-pobyt-wyjazd"><i class="fa fa-bed" aria-hidden="true"></i> Przyjazd, pobyt, wyjazd</a></li>
				<li<?php if ($tab == "cat-4") : echo ' class="active"'; endif; ?>><a href="/pl/pomoc/restauracja-spa-konferencje"><i class="fa fa-briefcase" aria-hidden="true"></i> Restauracja, SPA, Konferencje</a></li>
			</ul>
		</div>
	</div>

	<div class="box-special box-special-faq">
		<div class="content-faq">
		<?php foreach ($Categories as $Category) : ?>
			<h3><?=$Category->value;?></h3>
			
			<? $questions = FaqQuestions::items($Category->id); ?>
			
			<?php if ($questions) : ?>
				<ul class="question-faq">
				<?php foreach ($questions as $question) : ?>
					<li>
						<p class="question"><?=$question->question;?></p>
						<div class="answer"><?=$question->answer;?></div>
					</li>
				<?php endforeach; ?>
				</ul>
			<?php endif; ?>
			
		<?php endforeach; ?>
		</div>
	</div>
</div>