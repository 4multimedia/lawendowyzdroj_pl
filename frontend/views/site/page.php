<?php

	use yii\helpers\Html;

	$this->title = 'About';
	//$this->params['breadcrumbs'][] = $this->title;
	
?><div class="container">
	<div class="lead-header lead-header-slim">
		<h2><?=$Page->name;?></h2>
		<img src="/img/page/header_about_us.png">
	</div>
	
	<div class="content-page content-text-cms">
		<div class="box-special">
			<?=$Page->description;?>
		</div>
	</div>
	
</div>
