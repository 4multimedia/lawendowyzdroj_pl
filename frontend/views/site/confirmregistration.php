<?php

	use yii\helpers\Html;

	$this->title = 'Dziękujemy za rejestrację';
	//$this->params['breadcrumbs'][] = $this->title;
	
?><div class="container">

	
	<div class="content-page">
		<div class="box-special text-center">
			<div class="m-t-b-50">
				<h4>Na Twój adres e-mail <b class="text-red"><?=$user->email;?></b> wysłaliśmy wiadomość z linkiem aktywacyjnym konta.</h4>
				<br>
				<h4>Odbierz wiadomość i postępuj według instrukcji zawartych w e-mailu</h4>
			</div>
		</div>
	</div>
	
</div>
