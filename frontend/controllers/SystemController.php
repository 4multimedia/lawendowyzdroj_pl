<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * System controller
 */
class SystemController extends Controller
{
	public function actionSearch()
	{
		$session = Yii::$app->session;
		
		if (isset($_POST) AND $_POST["action"] == "user-search")
		{
			$session["search_request"] = new \ArrayObject;
			$session["search_request"]["begin"] = $_POST["date_from"];
			$session["search_request"]["end"] = $_POST["date_to"];
			$session["search_request"]["elastic"] = $_POST["elastic"];
			$session["search_request"]["activities"] = '';
			if (isset($_POST["turnusy"])) { $session["search_request"]["activities"] = $_POST["turnusy"]; }
			$session["search_request"]["objects"] = '';
			if (isset($_POST["objects"])) { $session["search_request"]["objects"] = 1; }
			$session["search_request"]["women"] = $_POST["women"];
			$session["search_request"]["men"] = $_POST["men"];
			$session["search_request"]["compan"] = $_POST["adv_adults"];
			$session["search_request"]["children"] = $_POST["age1"] + $_POST["age2"];
			$session["search_request"]["children1"] = $_POST["age1"];
			$session["search_request"]["children2"] = $_POST["age2"];
			
			if (!empty($_POST["turnusy"]))
			{
				Yii::$app->getResponse()->redirect(array('/pl/turnusy-lecznicze/szukaj'));
			}
			else
			{
				Yii::$app->getResponse()->redirect(array('/pl/nasze-pokoje/szukaj'));
			}
		}
	}
}
