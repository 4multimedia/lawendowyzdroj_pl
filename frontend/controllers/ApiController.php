<?php
	
	namespace frontend\controllers;
	
	use Yii;
	use yii\rest\ActiveController;

	/**
	 * API controller
	 */
	class ApiController extends ActiveController
	{
		public $modelClass = 'common\models\Reservations';
	}