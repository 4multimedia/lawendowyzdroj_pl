<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\LoginForm;
use common\models\RegisterForm;
use common\models\FaqCategories;
use common\models\BlogPosts;
use common\models\Pages;
use common\models\User;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
	
	public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
	    $data["blog"] = BlogPosts::find()->where([])->orderBy('date DESC')->limit(4)->all();
	    
        return $this->render('index', $data);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $data["model"] = new LoginForm();
        
        if($data["model"]->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
		{	
			$data["model"]->refresh();
			Yii::$app->response->format = 'json';
			echo json_encode(ActiveForm::validate($data["model"]));
			\Yii::$app->end();
		}
		elseif ($data["model"]->load(Yii::$app->request->post()) && $data["model"]->login())
		{
			Yii::$app->getResponse()->redirect(array('profil'));
		}
		else
		{
			echo Yii::$app->controller->renderPartial('login', $data);
		}
    }
    
    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $data["model"] = new RegisterForm();
        
        if($data["model"]->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
		{	
			$data["model"]->refresh();
			Yii::$app->response->format = 'json';
			echo json_encode(ActiveForm::validate($data["model"]));
			\Yii::$app->end();
		}
		elseif ($data["model"]->load(Yii::$app->request->post()))
		{
			$User = new User;
			$User->group_id = 2;
			$User->active = 0;
			$User->sesid = md5(time());
			$User->name = $_POST["RegisterForm"]["name"];
			$User->lastname = $_POST["RegisterForm"]["lastname"];
			$User->email = $_POST["RegisterForm"]["email"];
			$User->password = sha1(md5($_POST["RegisterForm"]["password"]));
			$User->save();
			
			Yii::$app->getResponse()->redirect(array('/pl/rejestracja/'.$User->sesid));
		}
		else
		{
			echo Yii::$app->controller->renderPartial('register', $data);
		}
    }
    
    public function actionConfirmregistration($token)
    {
	    $data["user"] = User::find()->where(['sesid' => $token])->one();
	    return $this->render('confirmregistration', $data);
    }
    
    public function actionActivation($token)
    {
	    $user = User::find()->where(['sesid' => $token, 'active' => 0])->one();
	    if ($user)
	    {
		    $user->active = 1;
		    $user->active_date = date("Y-m-d H:i:s");
		    $user->save();
		    
	    	return $this->render('activation');
	    }
	    else
	    {
		    Yii::$app->getResponse()->redirect(array('/'));
	    }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail('biuro@4multimedia.pl')) {
                Yii::$app->session->setFlash('success', '<b>Dziękujemy Twoja wiadomość została do nas wysłana!</b><br><br>Na Twój adres e-mail wysłaliśmy kopię Twojej wiadomości.<br>Jeśli jej nie otrzymałeś sprawdź SPAM lub podany adres e-mail został nieprawidłowo wpisany.');
            } else {
                Yii::$app->session->setFlash('error', 'Przepraszamy wystąpił błąd w wysyłce wiadomości, spróbuj ponownie.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAboutus()
    {
        return $this->render('aboutus');
    }
    
    public function actionGallery()
    {
	    return $this->render('gallery');
    }
    
    public function actionReviews()
    {
	    return $this->render('reviews');
    }
    
    public function actionPage()
    {
	    $url = \Yii::$app->getRequest()->url;
	    $url = str_replace("/pl/", "", $url);
	    
	    $data["Page"] = Pages::find()->where(['alias' => $url])->one();
	    
	    return $this->render('page', $data);
    }
    
    public function actionFaq($type = "")
    {	    
	    $data["tab"] = "cat-1"; $idcat = 2;
	    if ($type == "rezerwacje-anulacje-platnosci") { $data["tab"] = "cat-2"; $idcat = 3; }
	    if ($type == "przyjazd-pobyt-wyjazd") { $data["tab"] = "cat-3"; $idcat = 10; }
	    if ($type == "restauracja-spa-konferencje") { $data["tab"] = "cat-4"; $idcat = 23; }
	    
	    $data["Categories"] = FaqCategories::find()
	    ->select("faq_categories.id, value")
	    ->where(['parent_id' => $idcat])
	    ->leftJoin("languages_data", "`table` = 'faq_categories' AND language_id = 1 AND `column` = 'name' AND record_id = faq_categories.id")
	    ->all();
	    
	    return $this->render('faq', $data);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
