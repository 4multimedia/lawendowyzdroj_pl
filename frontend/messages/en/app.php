<?php
	
	return [
    	'Start' => 'Start',
    	'Hotel' => 'Hotel',
    	'O nas' => 'About Us',
    	'Galeria zdjęć i Video' => 'Photo Gallery and Video',
    	'Nasze pokoje' => 'Our rooms',
    	'Opinie gości' => 'Guest reviews',
    	'Informacje i wydarzenia' => 'News & Events',
    	'Baza wiedzy' => 'Knowledge base',
    	'Aktualności i ogłoszenia' => 'News',
    	'Informacje o Busko-Zdroju' => 'Informations about Busko-Zdroj',
    	'Nasza oferta' => 'Our offer',
    	'Poznaj nasze pakiety i ceny' => 'Our packages and prices',
    	'Panel Klienta' => 'Customer panel',
    	'Zaloguj się' => 'Login',
    	'Zarejestruj się' => 'Regsitration',
    	'Przypomnij hasło' => 'Remind password',
    	'Pomoc' => 'Help / FAQ',
    	'Kontakt' => 'Contact',
    	'codziennie od 8.00 - 20.00' => 'everyday from 8 a.m. to 8 p.m.',
    	'ZAMÓW ROZMOWĘ' => 'ORDER PHONE CALL',
    	'oddzwonimy w ciągu godziny' => 'We call back in an hour',
	];

?>