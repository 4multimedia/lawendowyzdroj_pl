<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['common\components\DynamicRoutes'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'offer' => ['class' => 'app\modules\offer\Module'],
        'rooms' => ['class' => 'app\modules\rooms\Module'],
        'activities' => ['class' => 'app\modules\activities\Module'],
        'pages' => ['class' => 'app\modules\pages\Module'],
        'specialoffer' => ['class' => 'app\modules\specialoffer\Module'],
        'treatments' => ['class' => 'app\modules\treatments\Module'],
        'profil' => ['class' => 'app\modules\profil\Module'],
        'chat' => ['class' => 'app\modules\chat\Module']
    ],
    'language' => 'pl-PL',
    'sourceLanguage' => 'pl-PL',
    'components' => [
	    'db' => require(__DIR__ . '/db.php'),
	    'price' => ['class' => 'common\components\Price'],
	    'gallery' => ['class' => 'common\components\Gallery'],
	    'availability' => ['class' => 'common\components\Availability'],
	    'general' => ['class' => 'common\components\General'],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl'=>'',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'assignmentTable'=> '{{%users_auth_assignment}}',
            'itemTable'=> '{{%users_auth_item}}',
            'itemChildTable'=> '{{%users_auth_item_child}}',
        ],
        'i18n' => [
	        'translations' => [
	            'app*' => [
	                'class' => 'yii\i18n\PhpMessageSource',
	                'fileMap' => [
	                    'app' => 'app.php',
	                    'app/error' => 'error.php',
	                ],
	            ],
	        ],
	    ],
        'urlManager'=>[
            'scriptUrl'=>'/index.php',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            'class' => 'yii\web\DbSession',
            'name' => 'lz-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
		'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
	            'class' => 'Swift_SmtpTransport',
	            'host' => 'mail2.mydevil.net',
	            'username' => 'powiadomienia@lawendowyzdroj.pl',
	            'password' => 'WhMdj9eUavkiVRmf5Mmj',
	            'port' => '465',
	            'encryption' => 'ssl',
	        ],
	        'useFileTransport' => False,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'params' => $params,
];
