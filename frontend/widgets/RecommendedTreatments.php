<?php
	
	namespace frontend\widgets;
	
	use yii\base\Widget;
	
	class RecommendedTreatments extends Widget
	{
		public $action;

		public function run()
		{
			//$data["Items"] = "";
			
			return $this->render('recommendedTreatments');
		}
	}