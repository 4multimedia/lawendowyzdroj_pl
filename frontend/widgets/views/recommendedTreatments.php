<?php
	
	use yii\web\View;
	
	$this->registerJsFile('@web/js/plugins/owlcarousel/dist/owl.carousel.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
	$this->registerCssFile('@web/js/plugins/owlcarousel/dist/assets/owl.carousel.min.css');
	$this->registerCssFile('@web/js/plugins/owlcarousel/dist/assets/owl.theme.default.min.css');
	
	$js = "

	$(\".box-recommended .owl-carousel\").owlCarousel({
		'items' : 1,
		'loop' : true,
		'autoplay' : true,
		'autoplayTimeout' : 4000,
		'autoplaySpeed' : 800,
		'dots' : true
	});
	
	";
	
	$this->registerJs($js, View::POS_END, 'my-script');
	
?><div class="box-down box-module box-recommended">
	<div class="box-border">
		<h3>Rekomendowane zabiegi</h3>
		<div class="box-slider">
			<div class="owl-carousel">
				<div>
					<img src="/img/module/zabieg_masaz.jpg">
					<p>Masaż</p>
				</div>
				<div>
					<img src="/img/module/zabieg_laser.jpg">
					<p>Laseroterapia</p>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>