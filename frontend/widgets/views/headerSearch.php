<?php

	use yii\widgets\ActiveForm;

	$form = ActiveForm::begin(['id' => 'search-property', 'action' => '/pl/szukaj']);
	
?>
	<input type="hidden" name="action" value="user-search">
	<input type="hidden" class="input-date-from" name="date_from" id="from" value="<?=$search_from;?>">
	<input type="hidden" class="input-date-to" name="date_to" id="to" value="<?=$search_to;?>">
		<div class="form-search-area">
			<div class="box-target box-border-right">
				<p>Cel pobytu</p>
				<div class="box-select-dropdown box-select-target-dropdown">
									<?php
										if ($paramsObjects == 1)
										{
											echo "Pobyt bez zabiegów medycznych";
										}
										else if(isset($searchTurnusy))
										{
											echo "Pobyt z zabiegami medycznymi";
										}
										else
										{
											echo "Wybierz cel pobytu";
										}
									?>
								</div>
								<b>Wybrano podkategorie: <u>0</u></b>
								
								<div class="box-select-checked">
									<div class="col-checked-only <?php if ($paramsObjects == 1) { echo "active"; } ?>"> <input type="checkbox" <?php if ($paramsObjects == 1) { echo "checked=\"checked\""; } ?> name="objects" value="1">Tylko <strong>NOCLEG</strong>, bez zabiegów medycznych <span class="show-help" rel="8">?</span></div>
									<div class="col-checked-area">
										<h4>Turnusy, pakiety wraz z noclegiem</h4>
										<div class="col-checked">
											<p>Pakiety weekendowe</p>
											<ul>
												<li class="<?php if (isset($searchTurnusy) AND in_array(27, $searchTurnusy)) { echo "active "; } ?>all"> <input type="checkbox" name="turnusy[27]" value="27"<?php if (isset($searchTurnusy) AND in_array(27, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Wszystkie pakiety</li>
												<li class="<?php if (isset($searchTurnusy) AND in_array(26, $searchTurnusy)) { echo "active "; } ?>one"><input type="checkbox" name="turnusy[26]" value="26"<?php if (isset($searchTurnusy) AND in_array(26, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Pakiety SPA & Wellness</li>
												<li class="<?php if (isset($searchTurnusy) AND in_array(28, $searchTurnusy)) { echo "active "; } ?>one"><input type="checkbox" name="turnusy[28]" value="28"<?php if (isset($searchTurnusy) AND in_array(28, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Pakiety okazjonalne</li>
												<li class="<?php if (isset($searchTurnusy) AND in_array(32, $searchTurnusy)) { echo "active "; } ?>one"><input type="checkbox" name="turnusy[32]" value="32"<?php if (isset($searchTurnusy) AND in_array(32, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Profilaktyczno lecznicze</li>
											</ul>
										</div>
										<div class="col-checked">
											<p>TURNUSY OKOŁO 7 DNIOWE</p>
											<ul>
												<li class="all<?php if (isset($searchTurnusy) AND in_array(17, $searchTurnusy)) { echo " active "; } ?>"><input type="checkbox" name="turnusy[17]" value="17"<?php if (isset($searchTurnusy) AND in_array(17, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Wszystkie 7 dniowe</li>
												<li class="one<?php if (isset($searchTurnusy) AND in_array(6, $searchTurnusy)) { echo " active "; } ?>"><input type="checkbox" name="turnusy[6]" value="6"<?php if (isset($searchTurnusy) AND in_array(6, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Sanatoryjne</li>
												<li class="one<?php if (isset($searchTurnusy) AND in_array(7, $searchTurnusy)) { echo " active "; } ?>"><input type="checkbox" name="turnusy[7]" value="7"<?php if (isset($searchTurnusy) AND in_array(7, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Profilaktyczne</li>
												<li class="one<?php if (isset($searchTurnusy) AND in_array(22, $searchTurnusy)) { echo " active "; } ?>"><input type="checkbox" name="turnusy[22]" value="22"<?php if (isset($searchTurnusy) AND in_array(22, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Specjalistyczne</li>
											</ul>
										</div>
										<div class="clearfix"></div>
										<div class="col-checked">
											<p>TURNUSY OKOŁO 14 DNIOWE</p>
											<ul>
												<li class="all<?php if (isset($searchTurnusy) AND in_array(18, $searchTurnusy)) { echo " active "; } ?>"><input type="checkbox" name="turnusy[18]" value="18"<?php if (isset($searchTurnusy) AND in_array(18, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Wszystkie około 14 dniowe</li>
												<li class="one<?php if (isset($searchTurnusy) AND in_array(10, $searchTurnusy)) { echo " active "; } ?>"><input type="checkbox" name="turnusy[10]" value="10"<?php if (isset($searchTurnusy) AND in_array(10, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Sanatoryjne</li>
												<li class="one<?php if (isset($searchTurnusy) AND in_array(11, $searchTurnusy)) { echo " active "; } ?>"><input type="checkbox" name="turnusy[11]" value="11"<?php if (isset($searchTurnusy) AND in_array(11, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Profilaktyczne</li>
												<li class="one<?php if (isset($searchTurnusy) AND in_array(12, $searchTurnusy)) { echo " active "; } ?>"><input type="checkbox" name="turnusy[12]" value="12"<?php if (isset($searchTurnusy) AND in_array(12, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Specjalistyczne</li>
												</ul>
										</div>
										<div class="col-checked">
											<p>TURNUSY OKOŁO 21 DNIOWE</p>
											<ul>
												<li class="all<?php if (isset($searchTurnusy) AND in_array(19, $searchTurnusy)) { echo " active "; } ?>"><input type="checkbox" name="turnusy[19]" value="19"<?php if (isset($searchTurnusy) AND in_array(19, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Wszystkie około 21 dniowe</li>
												<li class="one<?php if (isset($searchTurnusy) AND in_array(13, $searchTurnusy)) { echo " active "; } ?>"><input type="checkbox" name="turnusy[13]" value="13"<?php if (isset($searchTurnusy) AND in_array(13, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Sanatoryjne</li>
												<li class="one<?php if (isset($searchTurnusy) AND in_array(20, $searchTurnusy)) { echo " active "; } ?>"><input type="checkbox" name="turnusy[20]" value="20"<?php if (isset($searchTurnusy) AND in_array(20, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Profilaktyczne</li>
												<li class="one<?php if (isset($searchTurnusy) AND in_array(35, $searchTurnusy)) { echo " active "; } ?>"><input type="checkbox" name="turnusy[35]" value="35"<?php if (isset($searchTurnusy) AND in_array(35, $searchTurnusy)) { echo " checked=\"checked\""; } ?>> Specjalistyczne</li>
											</ul>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="col-solo"><a href="/pl/zabiegi-medyczne">Lista pojedynczych zabiegów medycznych</a></div>
									<div class="box-promo">
										<h4>Super oferta</h4>
										<img src="/img/gfx/promocja.png">
									</div>
								</div>
							</div>
							<div class="box-calendar box-calendar-left box-border-left date-from" date-option="from">
								<p>Przyjazd</p>
								<div class="box-calendar-dropdown">
									<b><?=$searchObjectFrom[2];?></b>
									<span class="dayname first"><?=mb_strtolower(\Yii::$app->general->dayFull($searchObjectFromDayNo)); ?></span>
									<span class="monthname"><?=mb_strtolower(\Yii::$app->general->month($searchObjectFrom[1]));?> <?=$searchObjectFrom[0]; ?></span>
								</div>
								
								<div class="elastic-box">
									<div class="elastic-title">Elastyczne daty:</div>
									<div class="elastic-spinner">
										<span class="spinner" data-max="10" data-prefix="dni"><em class="minus">-</em> <u><?=$elastic; ?></u> dni <em class="plus">+</em> <input type="hidden" name="elastic" value="<?=$elastic; ?>"></span>
									</div>
								</div>
								
							</div>
							<div class="box-calendar box-calendar-right box-border-right date-to" date-option="to">
								<p class="text-right">Wyjazd</p>
								<div class="box-calendar-dropdown">
									<b><?=$searchObjectTo[2];?></b>
									<span class="dayname first"><?=mb_strtolower(\Yii::$app->general->dayFull($searchObjectToDayNo)); ?></span>
									<span class="monthname"><?=mb_strtolower(\Yii::$app->general->month($searchObjectTo[1])); ?> <?=$searchObjectTo[0]; ?></span>
								</div>
								
								
							</div>
							<div class="box-persons">
								<div class="line-persons line-persons-first">
									<div>Kobiety <span class="spinner" data-max="4"><em class="minus">-</em> <u><?=$searchWomen; ?></u> <em class="plus">+</em> <input type="hidden" name="women" value="<?=$searchWomen; ?>"></span></div>
									<div class="center">Mężczyźni <span class="spinner" data-max="4"><em class="minus">-</em> <u><?=$searchMen; ?></u> <em class="plus">+</em> <input type="hidden" name="men" value="<?=$searchMen; ?>"></span></div>
									<div class="advanced-dropdown">Dodaj lub edytuj opcje</div>
									<div class="advanced-count">Wybranych filtrów: <b>0</b></div>
									
									<div class="advanced-dropdown-roll">
										
										<div class="spinner add-adults" style="<?php if ($paramsObjects == 1) { ?>display:none;<?php } ?>">
											<div>
												Osoby towarzyszące
												<span class="spinner" data-max="3"><em class="minus">-</em> <u><?=$searchAdvAdults; ?></u> <em class="plus">+</em> <input type="hidden" name="adv_adults" value="<?=$searchAdvAdults; ?>"></span>
												<span class="show-help" rel="11">?</span>
											</div>
										</div>

										<ul>
											<li class="<?php if ($paramsExtra == 1) { echo "active"; } ?>">Dostawka <input type="checkbox" name="extra" value="1" <?php if ($paramsExtra == 1) { echo "checked=\"checked\""; } ?>><span class="show-help" rel="12">?</span></li>
											<li class="<?php if ($paramsBestdeal == 1) { echo "active"; } ?>">Super oferty <input type="checkbox" name="bestdeal" value="1" <?php if ($paramsBestdeal == 1) { echo "checked=\"checked\""; } ?>><span class="show-help" rel="13">?</span></li>
											<li class="<?php if ($paramsLastminute == 1) { echo "active"; } ?>">Oferty last minute <input type="checkbox" name="lastminute" value="1" <?php if ($paramsLastminute == 1) { echo "checked=\"checked\""; } ?>><span class="show-help" rel="14">?</span></li>
											<li class="<?php if ($paramsFirstminute == 1) { echo "active"; } ?>">Oferty first minute <input type="checkbox" name="firstminute" value="1" <?php if ($paramsFirstminute == 1) { echo "checked=\"checked\""; } ?>><span class="show-help" rel="15">?</span></li>
											<li class="<?php if ($paramsShortgaps == 1) { echo "active"; } ?>">Oferty "Nie czekaj na weekend" <input type="checkbox" name="shortgaps" value="1" <?php if ($paramsShortgaps == 1) { echo "checked=\"checked\""; } ?>><span class="show-help" rel="16">?</span></li>
										</ul>
									</div>
								</div>
								<div class="line-persons line-persons-two">
									<div>0-2 lat <span class="spinner" data-max="3"><em class="minus">-</em> <u><?=$searchAge1; ?></u> <em class="plus">+</em> <input type="hidden" name="age1" value="<?=$searchAge1; ?>"></span></div>
									<div class="center">2-12 lat <span class="spinner" data-max="3"><em class="minus">-</em> <u><?=$searchAge2; ?></u> <em class="plus">+</em> <input type="hidden" name="age2" value="<?=$searchAge2; ?>"></span></div>
								</div>
							</div>
						</div>
						<div class="box box-button button-search-general">
							<button>Szukaj</button>
						</div>
						
						<div class="alert-search-top"></div>

<?php ActiveForm::end() ?>