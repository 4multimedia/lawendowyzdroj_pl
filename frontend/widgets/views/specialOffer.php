<?php
	
	use common\models\Activities;
	
?><div class="flip-container">
	<div class="flipper">
		<div class="front">
			<div class="special-offer special-offer-<?=$bg;?>">
				<a href="/pl/<?=$url_object;?>/oferty-specjalne/<?=$url_type;?>">
					<div class="special-offer-header">
						<h4><?=$title;?></h4>
						<h5><?=$subtitle;?></h5>
					</div>
					<div class="special-offer-image"></div>
					<div class="special-offer-end">
						<p>
							<?php if (!empty($discount)) : ?>
								Najlepsze oferty
							<?php else : ?>
								&nbsp;
							<?php endif; ?>
						</p>
						<span class="special-offer-discount">
							<?php if (!empty($discount)) : ?>
								Rabaty do <b><?=$discount;?></b>%
							<?php else : ?>
								<small>Aktualnie brak ofert</small>
							<?php endif; ?>
						</span>
						<span class="special-offer-price">Sanatorium Busko-Zdrój</span>
						<span class="special-offer-availability">Oferty <?php if (empty($discount)) : echo 'nie'; endif; ?>dostępne</span>
					</div>
					<img src="/img/offer/bg_box_<?=$bg;?>.jpg">
				</a>
			</div>
		</div>
		<div class="back">
			<div class="special-offer special-offer-<?=$bg;?>">
				<a href="/pl/<?=$url_object;?>/oferty-specjalne/<?=$url_type;?>">
					<div class="special-offer-header">
						<h4><?=$title;?></h4>
						<h5><?=$subtitle;?></h5>
					</div>
					<?php if ($items) : ?>
						<ul class="special-offer-items">
							<?php foreach ($items as $item) : ?>
								<li>
									<b><?=$item->activities->name;?></b><br>
									<span><?=mb_strtolower(Activities::period($item["date_from"], $item["date_to"]), 'utf-8');?></span>
								</li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
					<img src="/img/offer/bg_box_<?=$bg;?>_cover.jpg">
				</a>
			</div>
		</div>
	</div>
</div>