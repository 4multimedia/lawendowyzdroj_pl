<?php
	
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;

?>
  
  
<?= Html::beginForm(['/pl/hotel/pokoje'], 'post', ['data-pjax' => '', 'class' => 'form-inline']); ?>
  
<div class="top-search-objects">
			<button class="pink hide-filters">Pokaż filtry <i class="fa fa-eye"></i></button>
			<div class="row">
				<div class="col-md-4">
					<div>
						<?= yii\jui\Slider::widget([
      'clientOptions' => [
	      'range' => true,
          'min' => 1,
          'max' => 4,
          'values' => [1,4],
      ],
  ]); ?>
						<div class="box-slider">
							<p class="text-center">Min osób</p>
							<input type="text" name="person_from" placeholder="min. 1" value="<?=$params["person_from"];?>">
							<div class="slider-review slider-one"></div>
						</div>
						<div class="box-slider">
							<p class="text-center">Max osób</p>
							<input type="text" name="person_to" placeholder="max. 4" value="<?=$params["person_to"];?>">
							<div class="slider-review slider-one"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="radio-groups">
						&nbsp;
					</div>
					<div>
						<select>
							<option>Property Type</option>
							<option>Property Type 2</option>
						</select>
						<select>
							<option>Area</option>
						</select>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-4">
					<p class="text-center">Price</p>
					<input type="text" name="price" value="40 - 400 PLN">
					<div class="slider-price slider-one"></div>
					<div class="radio-groups">
						<div><input type="radio" name="price-type" value="1" checked="checked"> Day/pers</div>
						<div><input type="radio" name="price-type" value="2"> Day</div>
						<div><input type="radio" name="price-type" value="3"> Total</div>
					</div>
					<div>
						<select>
							<option>Type of promotion</option>
						</select>
						<select>
							<option>Booking Type</option>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="title">
						<p>Filter by</p>
						<button class="pink clear-filters">Delete filters <i class="fa fa-trash-o"></i></button>
					</div>
					<div>
						<div class="icon"><div class="amenities amenities-1"></div> <span>Heating</span></div>
						<div class="icon"><div class="amenities amenities-2"></div> <span>Washing<br>machine</span></div>
						<div class="icon"><div class="amenities amenities-3"></div> <span>Smokers</span></div>
						<div class="icon"><div class="amenities amenities-4"></div> <span>Air<br>conditioning</span></div>
						<div class="icon"><div class="amenities amenities-5"></div> <span>Internet</span></div>
						<div class="icon"><div class="amenities amenities-6"></div> <span>Desabled<br>friendly</span></div>
						<div class="icon"><div class="amenities amenities-7"></div> <span>Lift</span></div>
						<div class="icon"><div class="amenities amenities-8"></div> <span>Parking</span></div>
						<div class="icon"><div class="amenities amenities-9"></div> <span>TV</span></div>
						<div class="icon"><div class="amenities amenities-10"></div> <span>Cot</span></div>
						<div class="icon"><div class="amenities amenities-11"></div> <span>Pets</span></div>
						<div class="icon"><div class="amenities amenities-12"></div> <span>Balcony</span></div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		
		<?= Html::submitButton('Filtruj', ['class' => 'btn btn-lg btn-primary', 'name' => 'hash-button']) ?>
		
<?= Html::endForm() ?>