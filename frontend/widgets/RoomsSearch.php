<?php
	
	namespace frontend\widgets;
	
	use yii\base\Widget;
	
	class RoomsSearch extends Widget
	{		
		public $params;
		
		public function run()
		{
			$data["params"] = $this->params;
			
			return $this->render('roomsSearch', $data);
		}
	}