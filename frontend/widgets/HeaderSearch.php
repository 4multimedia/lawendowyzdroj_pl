<?php
	
	namespace frontend\widgets;
	
	use yii\base\Widget;
	
	class HeaderSearch extends Widget
	{		
		public function run()
		{
			$session = \Yii::$app->session;
			
			
			$data["search_from"] = date("Y-m-d");
			$data["search_to"] = date("Y-m-d", strtotime(date("Y-m-d")) + (3600*24));
			$data["elastic"] = 0;
			$data["adults"] = 1;
			$data["children"] = 0;
			
			$searchObject["objects"] = '';
			
			if ($session->has('search_request'))
		    {
			    $search_request = $session->get('search_request');
			    $data["search_from"] = $search_request["begin"];
			    $data["search_to"] = $search_request["end"];
			    $data["elastic"] = $search_request["elastic"];
			    
			    $searchObject["men"] = $search_request["men"];
			    $searchObject["women"] = $search_request["women"];
			    $searchObject["age1"] = $search_request["children1"];
			    $searchObject["age2"] = $search_request["children2"];
			    $searchObject["adv_adults"] = $search_request["compan"];
			    
			    $searchObject["objects"] = $search_request["objects"];
			    if (isset($search_request["activities"]) AND !empty($search_request["activities"]))
			    {
			    	$data["searchTurnusy"] = $search_request["activities"];
			    }
			}
			
			$data["searchObjectFrom"] = explode("-", $data["search_from"]);
			$data["searchObjectFromDayNo"] = date("N", strtotime($data["search_from"]));
						
			$data["searchObjectTo"] = explode("-", $data["search_to"]);
			$data["searchObjectToDayNo"] = date("N", strtotime($data["search_to"]));
						
			$data["paramsObjects"] = $searchObject["objects"];
			$data["paramsExtra"] = $searchObject["extra"];
			$data["paramsBestdeal"] = $searchObject["bestdeal"];
			$data["paramsLastminute"] = $searchObject["lastminute"];
			$data["paramsFirstminute"] = $searchObject["firstminute"];
			$data["paramsShortgaps"] = $searchObject["shortgaps"];
						
			$data["searchMen"] = (strtolower($searchObject["men"])=="nan" OR $searchObject["men"] == "")?0:$searchObject["men"];
			$data["searchWomen"] = (strtolower($searchObject["women"])=="nan" OR $searchObject["women"] == "")?0:$searchObject["women"];
			$data["searchAge1"] = (strtolower($searchObject["age1"])=="nan" OR $searchObject["age1"] == "")?0:$searchObject["age1"];
			$data["searchAge2"] = (strtolower($searchObject["age2"])=="nan" OR $searchObject["age2"] == "")?0:$searchObject["age2"];
			$data["searchAdvAdults"] = (strtolower($searchObject["adv_adults"])=="nan" OR $searchObject["adv_adults"] == "")?0:$searchObject["adv_adults"];

			return $this->render('headerSearch', $data);
		}
	}