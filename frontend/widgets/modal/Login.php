<?php
	
	namespace frontend\widgets\modal;

	use yii\base\Widget;
	use yii\helpers\Html;
	use frontend\models\LoginForm;

	class Login extends Widget
	{
		public function run()
		{
			$data["model"] = new LoginForm;
						
			return $this->render('login', $data);
		}
	}