<?php
	
	use yii\web\View;
	use yii\bootstrap\Modal;
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;
	
	$form = ActiveForm::begin([
		'action' => '/site/register',
		'id' => 'register-form',
		'layout' => 'horizontal',
		'fieldConfig' => [
	        'horizontalCssClasses' => [
	            'label' => 'col-sm-3',
	            'offset' => 'col-sm-offset-3',
	            'wrapper' => 'col-sm-9',
	            'error' => '',
	            'hint' => '',
	        ],
	    ],
		'enableClientValidation' => true,
		'enableAjaxValidation' => true,
		'options' => ['class' => 'form-horizontal']
	]);	

	Modal::begin([
		'options' => ['class' => 'modal-user'],
		'id' => 'login-register',
		'header' => '<b>' . Yii::t('app', 'Rejestracja') . '</b>',
		'footer' => '
			<button class="btn btn-danger" data-dismiss="modal">Anuluj</button>
			<button class="btn btn-success">Zarejestruj się</button>
		',
	]);
?>

	<?=$form->field($model, 'name');?>
	<?=$form->field($model, 'lastname');?>
	<?=$form->field($model, 'email');?>
	<?=$form->field($model, 'password')->passwordInput();?>
	<?=$form->field($model, 'repeat_password')->passwordInput();?>

	<div class="modal-end">&nbsp;</div>

<?php	
	Modal::end();
	ActiveForm::end();
?>