<?php
	
	use yii\web\View;
	use yii\bootstrap\Modal;
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;
    
    $this->registerJs($js, View::POS_END, 'breadcrumbs');
	
	$form = ActiveForm::begin([
		'action' => '/site/login',
		'id' => 'modal-login-form',
		'layout' => 'horizontal',
		'fieldConfig' => [
	        'horizontalCssClasses' => [
	            'label' => 'col-sm-3',
	            'offset' => 'col-sm-offset-3',
	            'wrapper' => 'col-sm-9',
	            'error' => '',
	            'hint' => '',
	        ],
	    ],
		'enableClientValidation' => true,
		'enableAjaxValidation' => true,
		'options' => ['class' => 'form-horizontal']
	]);	

	Modal::begin([
		'options' => ['class' => 'modal-user'],
		'id' => 'login-modal',
		'header' => '<b>' . Yii::t('app', 'Logowanie') . '</b>',
		'footer' => '
			<button class="btn btn-info pull-left">Zaloguj</button>
			<button class="btn btn-danger" data-dismiss="modal">Anuluj</button>
			<button class="btn btn-success">Zaloguj się</button>
		',
	]);
?>

	<?=$form->field($model, 'email');?>
	<?=$form->field($model, 'password')->passwordInput();?>

	<div class="modal-end">&nbsp;</div>

<?php	
	Modal::end();
	ActiveForm::end();
?>