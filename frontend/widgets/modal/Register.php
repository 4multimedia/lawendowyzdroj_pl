<?php
	
	namespace frontend\widgets\modal;

	use yii\base\Widget;
	use yii\helpers\Html;
	use frontend\models\RegisterForm;

	class Register extends Widget
	{
		public function run()
		{
			$data["model"] = new RegisterForm;
						
			return $this->render('register', $data);
		}
	}