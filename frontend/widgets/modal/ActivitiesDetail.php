<?php
	
	namespace frontend\widgets\modal;

	use yii\base\Widget;
	use yii\helpers\Html;

	class ActivitiesDetail extends Widget
	{
		public function run()
		{						
			return $this->render('activitiesdetail');
		}
	}