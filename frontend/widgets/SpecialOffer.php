<?php
	
	namespace frontend\widgets;
	
	use yii\base\Widget;
	use common\models\BestDeal;
	use common\models\FirstMinute;
	use common\models\LastMinute;
	use common\models\ActivitiesBestDeal;
	
	class SpecialOffer extends Widget
	{
		public $type;
		public $option;
		public $bg;
					
		public function run()
		{			
			$data["bg"] = $this->bg;
			
			$data["items"] = array();
			
			if ($this->type == "lastminute")
			{
				$data["title"] = "Last minute";
				$data["url_type"] = "last-minute";
				$data["discount"] = LastMinute::maxDiscount($this->option);
			}
			if ($this->type == "firstminute")
			{
				$data["title"] = "First minute";
				$data["discount"] = FirstMinute::maxDiscount($this->option);
				$data["url_type"] = "first-minute";
			}
			if ($this->type == "bestdeal")
			{
				$data["title"] = "Super oferty";
				$data["discount"] = BestDeal::maxDiscount($this->option);
				$data["url_type"] = "super-oferty";
				
				$data["items"] = ActivitiesBestDeal::find()
				->select(["*", "date_from", "date_to", "stays_date_id as date_id", "discount"])
				->where("date_from > NOW()")
				->leftJoin("stays_dates", "stays_date_id = stays_dates.id")
				->orderBy('date_from ASC')
				->limit(5)
				->all();
			}
			
			if ($this->option == "rooms")
			{
				$data["subtitle"] = "Nasze pokoje";
				$data["url_object"] = "nasze-pokoje";
			}
			if ($this->option == "activities")
			{
				$data["subtitle"] = "Turnusy lecznicze";
				$data["url_object"] = "turnusy-lecznicze";
			}
			if ($this->option == "packages")
			{
				$data["subtitle"] = "Pakiety weekendowe";
			}
			
			return $this->render('specialOffer', $data);
		}
	}