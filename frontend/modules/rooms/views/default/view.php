<div class="container">
	<div class="box-white box-shadow-dark box">
		<div class="object-title">
			<h2><?=$Object->name;?></h2>
			<div class="object-note">
				Ogólna ocena:
				<div class="note"><span>0</span><span>0</span></div>
			</div>
		</div>
		<!-- WIDGET GALERIA -->
		<div class="reservation-right-box">
			<?php if (!$Search) { ?>
			<form method="post" class="form-reservation" action="/pl/rezerwacja">
				<input type="hidden" name="object_type" value="object">
				<input type="hidden" name="object_id" value="<?=$item->id;?>">
				<div class="guest">
					<input type="hidden" name="people" value="2">
					<span class="line count">Gości: 2</span>
					<span class="line">Przyjazd / Wyjazd</span>
					<a href="javascript:;"><i class="fa fa-cog"></i></a>
				</div>
				<div class="period">
					<span class="line from">Przyjazd: <b id="text_from"><?=date("d.m.Y");?></b> <input type="hidden" name="date_from" value="<?=date("Y-m-d");?>"></span>
					<span class="line to">Wyjazd: <b id="text_to"><?=date("d.m.Y", strtotime("+6 day", strtotime(date("Y-m-d"))));?></b> <input type="hidden" name="date_to" value="<?=date("Y-m-d", strtotime("+6 day", strtotime(date("Y-m-d"))));?>"></span>
				</div>
				
				<div class="total_price text-center">
					<b>Cena całkowita</b>
					<span>za 6 nocy</span>
				</div>
				<div class="stay-reservation-button stay-reservation-button-slim">
					<a href="javascript:;" class="active">
						<b>Cena</b>
						<?=number_format($object_price["total"], 0, ',', '');?>
						<em>PLN</em>
					</a>
				</div>
				
				<div class="discount-slim discount-form">
					<div class="discount-slim-title"><b>Kod rabatowy</b></div>
					<div class="discount-alert">KOMUNIKAT BŁĘDU</div>
					<div class="discount-slim-field">
						<input type="text" name="discount">
						<img class="wait-load" src="/files/img/gfx/discount-load.gif">
						<button class="pink">Sprawdź</button>
					</div>
					<div>Jak zdobyć nasze kody rabatowe?</div>
				</div>
				
				<?php if ($block_reservation == false) : ?>
					<button class="btn-reservation"><span>Rezerwuj</span></button>
				<?php else : ?>
					<div class="alert alert-danger">Rezerwacja w dniach: <br><?=$date_from;?> - <?=$date_to;?><br> jest niedostępna</div>
				<?php endif; ?>
				
				<div class="booking-type-text text-center">
					<div class="booking-type">Tryb rezerwacji <span class="reservation-help show-help">?</span> </div>
					<?php if ($Object->booking_type == 1) { ?>
						wymaga potwierdzenia
					<?php } else { ?>
						natychmiastowe potwierdzenie
					<?php } ?>
				</div>
			</form>
			<?php } else { ?>
			<form method="post" class="form-reservation" action="/pl/rezerwacja">
				<input type="hidden" name="object_type" value="object">
				<input type="hidden" name="object_id" value="<?=$Object->id;?>">
				<input type="hidden" name="booking_percent" value="100">
				
				<input type="hidden" name="people_men" value="<?=$Search["men"];?>">
				<input type="hidden" name="people_women" value="<?=$Search["women"];?>">
				
				<div class="guest">
					<input type="hidden" name="people" value="<?=$Search["adults"];?>">
					<span class="line count">Gości: <?=$Search["adults"];?></span>
					<span class="line">Przyjazd / Wyjazd</span>
					<a href="javascript:;"><i class="fa fa-cog"></i></a>
				</div>
				<div class="period">
					<span class="line from">Przyjazd: <b id="text_from"><?=$Date["from"];?></b> <input type="hidden" name="date_from" value="<?=$Date["from"];?>"></span>
					<span class="line to">Wyjazd: <b id="text_to"><?=$Date["to"];?></b> <input type="hidden" name="date_to" value="<?=$Date["to"];?>"></span>
				</div>
				
				<?php
					
					$CountPerson = $BeedsCount["all"]["single"] + ($BeedsCount["all"]["double"] * 2);
							
					$object_price = Objects::model()->getPrice(
						$Object->id,
						$Date["from"],
						$Date["to"],
						$Search["adults"],
						$CountPerson
					);
					
					if ($SuperOfferDiscount)
					{
						if ($SuperOfferActive == "lastminute")
						{
							$last_total = 0;
							$price_day = number_format($object_price["total"], 0, '', '') / $CountNights;
							$price_discount = $price_day * ((100-$SuperOfferDiscount)/100);
							
							for($d = 0; $d < $CountNights; $d++)
							{
								if ($d < 7)
								{
									$last_total = $last_total + $price_discount;
								}
								else
								{
									$last_total = $last_total + $price_day;
								}
							}
							
							$object_price["total"] = $last_total;
						}
						else
						{
							$object_price["total"] = $object_price["total"] - ($object_price["total"] * ($SuperOfferDiscount / 100));
						}
					}
				?>
				<input type="hidden" name="price" value="<?=number_format($object_price["total"], 0, ',', '');?>">
				<input type="hidden" name="price_clear" value="<?=number_format($object_price["total"], 0, ',', '');?>">
				<div class="total_price text-center">
					<b>Cena całkowita</b>
					<?php if ($object_price["breakfast"] == true) { ?><span>za pokój ze śniadaniem</span><?php } else { ?>
					<span>za wynajem pokoju</span><?php } ?>
				</div>
				<div class="stay-reservation-button stay-reservation-button-slim">
					<a href="javascript:;" class="active">
						<b>Cena</b>
						<?=number_format($object_price["total"], 0, ',', '');?>
						<em>PLN</em>
					</a>
				</div>
				
				<?php if ($SuperOfferActive) { ?>
					<?php if ($SuperOfferActive == "lastminute") { ?>
						<div class="active-superoffer active-superoffer-slim superoffer-lastminute">
							<span>Twoja zniżka</span>
							<b>Last Minute</b>
							<span class="inline-discount"><?=$SuperOfferDiscount;?>%</span>
						</div>
					<?php } ?>
					
					<?php if ($SuperOfferActive == "bestdeal") { ?>
						<div class="active-superoffer active-superoffer-slim superoffer-bestdeal">
							<span>Twoja zniżka</span>
							<b>Super oferta</b>
							<span class="inline-discount"><?=$SuperOfferDiscount;?>%</span>
						</div>
					<?php } ?>
				<?php } ?>
				
				<div class="discount-slim discount-form">
					<div class="discount-slim-title"><b>Kod rabatowy</b></div>
					<div class="discount-alert">KOMUNIKAT BŁĘDU</div>
					<div class="discount-slim-field">
						<input type="text" name="discount">
						<img class="wait-load" src="/files/img/gfx/discount-load.gif">
						<button class="pink">Sprawdź</button>
					</div>
					<div>Jak zdobyć nasze kody rabatowe?</div>
				</div>
			
				<?php if ($block_reservation == false) : ?>
					<button class="btn-reservation"><span>Rezerwuj</span></button>
				<?php else : ?>
					<div class="alert alert-danger text-center">Rezerwacja w dniach: <br><?=$date_from;?> - <?=$date_to;?><br> jest niedostępna</div>
				<?php endif; ?>
				
				<div class="booking-type-text text-center">
					<div class="booking-type">Tryb rezerwacji <span class="reservation-help show-help">?</span> </div>
					<?php if ($Object->booking_type == 1) { ?>
						wymaga potwierdzenia
					<?php } else { ?>
						natychmiastowe potwierdzenie
					<?php } ?>
				</div>
			</form>
			<?php } ?>
		</div>
		<div class="clearfix">&nbsp;</div>
	</div>
	
	
	<div class="row object">
		<div class="col-md-9 left">
			<div class="box-special p30">
				<h2><?=$Object->name;?></h2>
				<h3><?=$Object->subname;?></h3>
				
				<div class="amenities-box">
					<div class="amenities amenities-1<?php if (isSet($amenities[1])) { echo "-active"; } ?> first"></div>
					<div class="amenities amenities-2<?php if (isSet($amenities[2])) { echo "-active"; } ?>"></div>
					<div class="amenities amenities-3<?php if (isSet($amenities[3])) { echo "-active"; } ?>"></div>
					<div class="amenities amenities-4<?php if (isSet($amenities[4])) { echo "-active"; } ?>"></div>
					<div class="amenities amenities-5<?php if (isSet($amenities[5])) { echo "-active"; } ?>"></div>
					<div class="amenities amenities-6<?php if (isSet($amenities[6])) { echo "-active"; } ?>"></div>
					<div class="amenities amenities-7<?php if (isSet($amenities[7])) { echo "-active"; } ?>"></div>
					<div class="amenities amenities-8<?php if (isSet($amenities[8])) { echo "-active"; } ?>"></div>
					<div class="amenities amenities-9<?php if (isSet($amenities[9])) { echo "-active"; } ?>"></div>
					<div class="amenities amenities-10<?php if (isSet($amenities[10])) { echo "-active"; } ?>"></div>
					<div class="amenities amenities-11<?php if (isSet($amenities[11])) { echo "-active"; } ?>"></div>
					<div class="amenities amenities-12<?php if (isSet($amenities[12])) { echo "-active"; } ?>"></div>
					<div class="clearfix">&nbsp;</div>
				</div>
				
				<div class="info">
					<span>Numer oferty: <b><?=$Object->no;?></b></span>
					<span>Piętro: <b><? if($Object->floor_number == 0) { echo "parter"; } else { echo $Object->floor_number; } ?></b></span>
					<span>Metraż obiektu: <b><?=$Object->property_size;?> m&sup2;</b></span>
				</div>
				
				<div class="summary">
					<p><?=$Object->summary;?></p>
					<div class="read-more"><a href="javascript:;">Czytaj więcej</a></div>
				</div>
				<?php /*
				<div class="comment">
					<p><span>Opinie Klientów</span></p>
					<div class="text">
						<span>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</span>
						<em>~  Jakub z Poznania  ~</em>
					</div>
					<div class="read-more"><a href="javascript:;">Czytaj wszystkie</a></div>
				</div>
				*/ ?>
				
				<div class="object-info-box">
					<div class="distances">
						<h3>Dystanse</h3>
						<ul>
							<li>Aleja zdrojowa: <span>4 minuty spacerem</span></li>
							<li>Dworzec autobusowy: <span>2 minuty samochodem</span></li>
							<li>Centrum Zdroju: <span>10 minut spacerem</span></li>
							<li>Park Zdrojowy: <span>6 minut spacerem</span></li>
						</ul>
					</div>
					<div class="rooms">
						<h3>Pokoje</h3>
						<?php if ($RoomsArray) { ?>
						<ul>
							<?php foreach($RoomsArray as $key => $room) { ?>
								<?php if ($room["count"] > 0) { ?>
								<li><?=$room["name"];?>: <span><?=$room["count"];?></span></li>
								<?php } ?>
							<?php } ?>
						</ul>
						<?php } ?>
					</div>
					<div class="beds">
						<h3>Łóżka</h3>
						<p>Podwójne łóżka: <b class="pause"><?=$BeedsCount["all"]["double"];?></b> Pojedyncze łóżka: <b><?=$BeedsCount["all"]["single"];?></b></p>
						<?php
						
	
						
						?>
						<div><a href="">Więcej informacji</a></div>
					</div>
					
					<div class="clear"></div>
				</div>
								
				<div class="descript">
					<div class="tabs">
						<p>Opisy</p>
						<ul>
							<li id="tab1" class="active"><a href="javascript:;"><span></span> Pełny opis</a></li> 
							<li id="tab2"><a href="javascript:;"><span></span> Informacje szczegółowe</a></li> 
							<li id="tab3"><a href="javascript:;"><span></span> Rodzaje łóżek</a></li>
							<li id="tab4"><a href="javascript:;"><span></span> Rodzaje pokojów</a></li>
						</ul>
						<p>Płatności</p>
						<ul>
							<li id="tab5"><a href="javascript:;"><span></span> Koszty wynajmu</a></li> 
							<li id="tab6"><a href="javascript:;"><span></span> Opłaty dodatkowe</a></li> 
							<li id="tab7"><a href="javascript:;"><span></span> Kaucja</a></li>
							<li id="tab8"><a href="javascript:;"><span></span> Koszty anulacji</a></li>
							<li id="tab9"><a href="javascript:;"><span></span> Zasady zwrotu kosztów</a></li>
						</ul>
						<p>Regulamin obiektu</p>
						<ul>
							<li id="tab10"><a href="javascript:;"><span></span> Zasady podstawowe</a></li> 
							<li id="tab11"><a href="javascript:;"><span></span> Zasady szczegółowe</a></li> 
						</ul>
					</div>
					<div class="tabs-content">
						<h3 class="subtitle">Pełny opis</h3>
						<h4 class="title"><?=$Object->name;?></h4>
						<div class="description-load-text" id="desc1">
							<?=$Object->full_description;?>
						</div>
						<div class="description-load-text description-load-text-hide" id="desc2">
							<?=$Object->detailed_info;?>
						</div>
						
						<div class="description-load-text description-beds description-load-text-hide" id="desc3">
						
							<p class="max-number">Maksymalna ilość osób w tym pokoju <b><?=($BeedsCount["all"]["single"] + ($BeedsCount["all"]["double"] * 2));?></b></p>
							
						
						</div>
						
						<div class="description-load-text description-load-text-hide" id="desc5">
							<?=$Object->rental_cost;?>
						</div>
						<div class="description-load-text description-load-text-hide" id="desc6">
							<?=$Object->extra_charges;?>
						</div>
						<div class="description-load-text description-load-text-hide" id="desc7">
							<?=$Object->breakeage_deposit;?>
						</div>
						<div class="description-load-text description-load-text-hide" id="desc8">
							<?=$Object->cancellation_charges;?>
						</div>
						<div class="description-load-text description-load-text-hide" id="desc9">
							<?=$Object->refunds;?>
						</div>
						<div class="description-load-text description-load-text-hide" id="desc10">
							<h2>Tego nie wolno:</h2>
							<?php
								if ($Rules)
								{
									for($r = 0; $r < count($Rules); $r++)
									{
										echo '<li>'.$Rules[$r]["text"].'</li>';
									}
								}
								
							?>
						</div>
						<div class="description-load-text description-load-text-hide" id="desc11">
							<?php
								if ($RulesDetailed)
								{
									for($r = 0; $r < count($RulesDetailed); $r++)
									{
										echo '<li>'.$RulesDetailed[$r]["text"].'</li>';
									}
								}
								
							?>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				
				<?php if ($BestDeal) { ?>
					<div class="best-deal">
					<h4><span>SUPER OFERTA dla tego pokoju</span></h4>
					<?php
					
						$max_best_deal = 0;
						for($b = 0; $b < count($BestDeal); $b++)
						{
							if ($BestDeal[$b]->discount_full > 0)
							{
								$bestDeals .= '<div class="item"><strong>'.$BestDeal[$b]->discount_full.'%</strong> za wynajem pokoju w całym okresie <b>'.$BestDeal[$b]->date_from.' - '.$BestDeal[$b]->date_to.'</b> <a href="javascript:;">Zobacz <i class="fa fa-angle-right"></i></a></div>';
								if ($BestDeal[$b]->discount_full > $max_best_deal) { $max_best_deal = $BestDeal[$b]->discount_full; }
							}
							if ($BestDeal[$b]->discount_part > 0)
							{
								$bestDeals .= '<div class="item"><strong>'.$BestDeal[$b]->discount_part.'%</strong> za wynajem pokoju pomiędzy datami <b>'.$BestDeal[$b]->date_from.' - '.$BestDeal[$b]->date_to.'</b> <a href="javascript:;">Zobacz <i class="fa fa-angle-right"></i></a></div>';
								if ($BestDeal[$b]->discount_part > $max_best_deal) { $max_best_deal = $BestDeal[$b]->discount_part; }
							}
						}
						
					?>
						<h5>aż do <b><?=$max_best_deal;?>%</b> <span>zniżki</span></h5>
						<div><?=$bestDeals;?></div>
					</div>
				<?php } ?>
				
				<?php if ($LastMinute) { ?>
				<div class="last-minute">
					<h4><span>LAST MINUTE</span></h4>
					<h5><span>rabat do <b><?=$max_last_minute;?>%</b></span> - rabat zależy od daty przyjazdu i daty rezerwacji</h5>
					<div><?=$lastMinute;?></div>
				</div>
				<?php } ?>
				
				<?php if ($ShortGaps) { ?>
				<div class="last-minute shortgaps">
					<h4><span>Nie czekaj na weekend</span></h4>
					<h5><span>rabat aż do <b><?=$max_short_gaps;?>%</b></span> - rabat tylko w określonych datach</h5>
					<?=$shortGaps;?>
				</div>
				<?php } ?>
				
				<?php if ($FirstMinute) { ?>
				<div class="last-minute first-minute">
					<h4><span>Taniej, bo wcześniej</span></h4>
					<h5><span>rabat aż do <b><?=$max_first_minute;?>%</b></span> - rabat zależy od daty przyjazdu i daty rezerwacji</h5>
					<?=$firstMinute;?>
				</div>
				<?php } ?>
				
				<div id="availability-widget">
					<!-- WIDGET DOSTEPNOSCI -->
				</div>
				
				<!-- WIDGET OPINIE -->
			
			</div>
		</div>
			<div class="col-md-3">
			<div class="box-special p30">
				
				
				<div class="box-recommended2"><img src="/themes/lawendowyzdroj/files/img/recommended_<?=$Object->recommended;?>.png"></div>
				
				<?php /* <a href="javascript:;" class="negotiate">Negocjuj <b>cenę</b> <i class="fa fa-angle-double-right"></i></a> */ ?>
				
				<? /*<div class="deals-box deal-blue">
					<div class="deal-top">
						<div class="deal-bottom">
							<div>
								<span>Dostępny</span>
							</div>
						</div>
					</div>
				</div>
				
				<div>
					<span>Dostępny</span>
				</div>
				
				<div>
					<span>Dostępny</span>
				</div>
				*/ ?>
				
				<div class="owner-thumb">
					<div><img src="/themes/lawendowyzdroj/files/img/img_logo.png"></div>
					<p><?=$Owner["Info"]["name"].' '.$Owner["Info"]["lastname"];?></p>
				</div>
				
				<script type="text/javascript">
					
					var object_id = <?=$Object->id;?>;
					var object_type = 'object';
					var object_date_id = '';
					
				</script>
				
				<div class="contact-buttons open-communication">
					<a class="btn-contact contact-portal" href="javascript:;">Kontakt<i class="fa fa-envelope-o"></i></a>
				</div>
				<div class="we-speak">Obsługa w języku: PL, EN</div>
				<a href="/pl/o-nas" class="about-owner">Więcej o właścicielu <i class="fa fa-angle-double-right"></i></a>
				
				<div class="owner-clocks">
					<span>
						<i class="fa fa-clock-o"></i>
						w ciągu jednego dnia
					</span>
					<span>
						<i class="fa fa-calendar"></i>
						zalogowany <?=$Owner["LastLog"];?>
					</span>
					<div class="clearfix"></div>
				</div>
				
				<div class="line-time">Czas w Polsce: <b><?=date("H:i");?></b></div>
			</div>
	
		
		<div class="clearfix"></div>
		
		<div class="boxes">
						
						<div class="clear">&nbsp;</div>
					</div>
				<div class="clear">&nbsp;</div>
			</div>
		<div class="clear">&nbsp;</div>
	</div>

</div>