<?php
	
	use yii\widgets\Pjax;
	use frontend\widgets\RoomsSearch;
	
?><div class="container">
	<div class="lead-header lead-header-slim">
		<h2>Nasze pokoje <small>Oferta noclegowa - Busko-Zdrój</small></h2>
		<img src="/img/page/header_rooms.png">
	</div>
	<div class="content-page">
		<div class="box-special">
			<?php Pjax::begin([]); ?>
			<?php /* echo RoomsSearch::widget(['params' => $params]); */ ?>
			<?php echo Yii::$app->controller->renderPartial('/layouts/_list', ['items' => $items]); ?>
			<?php Pjax::end(); ?>
		</div>
	</div>
</div>