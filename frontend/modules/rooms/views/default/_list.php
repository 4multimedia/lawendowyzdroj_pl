<?php
	
	use common\models\Rooms;
	
?><?php foreach ($items->models as $Item) : ?>
<div class="room-list-item">
	<img src="http://lawendowyzdroj.pl/themes/lawendowyzdroj/files/img/recommended_<?=$Item->recommended;?>.png" class="recommended">
		<div class="title">
			<div class="title-box">
				<h3><a href="/pl/pokoj/<?=$Item->alias;?>"><?=$Item->name;?></a></h3>
				<a class="btn-show-hide" href="javascript:;">Ukryj</a>
			</div>
			<div class="title-gray"></div>
		</div>
		<div class="details">
			<div class="left">
				<a href="javascript:;" class="pink">Dodaj do planu podróży</a>
				<div class="thumb">
					<a href="/pl/pokoj/<?=$Item->alias;?>">
						<?php $image = Yii::getAlias('@app')."/web".urldecode($Item->image); ?>
						<?php if (file_exists($image)) : ?>
							<img src="<?= Rooms::image($Item->image, 250, 170, 'lawendowy_zdroj_pokoj_'.$Item->name);?>">
						<?php endif; ?>
					</a>
				</div>
			</div>
			<div class="center">
				<div class="center-top">
					<p>Numer oferty: <b><?=$Item->no;?></b></p>
					<div class="note-box">Ogólna ocena: <div class="note"><span>0</span><span>0</span></div></div>
				</div>
				<h4><?=$Item->subname;?></h4>
				<div class="info info-long">
					<p>Szczegóły</p>
					<span>Maksymalna ilość osób: <b><?=$Item->max_person;?></b></span>
					<span>Ilość łóżek: <b><?=$Item->max_beds;?></b></span>
					<span>Metraż pokoju: <b><?=$Item->property_size;?> m&sup2;</b></span>
					<span>Piętro: <b><?=$Item->floor_number;?> (winda)</b></span>
					<span>Typ rezerwacji: <b>natychmiastowa</b></span>
				</div>
				<div class="info">
					<?php
						$amenities = array();
						if ($Item->amenities) { $amenities = explode(",", $Item->amenities); }
					?>			
					<p>Udogodnienia</p>
					<div class="amenities amenities-1<?php if (in_array(1, $amenities)) : echo "-active"; endif; ?>"></div>
					<div class="amenities amenities-2<?php if (in_array(2, $amenities)) : echo "-active"; endif; ?>"></div>
					<div class="amenities amenities-3<?php if (in_array(3, $amenities)) : echo "-active"; endif; ?>"></div>
					<div class="amenities amenities-4<?php if (in_array(4, $amenities)) : echo "-active"; endif; ?>"></div>
					<div class="amenities amenities-5<?php if (in_array(5, $amenities)) : echo "-active"; endif; ?>"></div>
					<div class="amenities amenities-6<?php if (in_array(6, $amenities)) : echo "-active"; endif; ?>"></div>
					<div class="amenities amenities-7<?php if (in_array(7, $amenities)) : echo "-active"; endif; ?>"></div>
					<div class="amenities amenities-8<?php if (in_array(8, $amenities)) : echo "-active"; endif; ?>"></div>
					<div class="amenities amenities-9<?php if (in_array(9, $amenities)) : echo "-active"; endif; ?>"></div>
					<div class="amenities amenities-10<?php if (in_array(10, $amenities)) : echo "-active"; endif; ?>"></div>
					<div class="amenities amenities-11<?php if (in_array(11, $amenities)) : echo "-active"; endif; ?>"></div>
					<div class="amenities amenities-12<?php if (in_array(12, $amenities)) : echo "-active"; endif; ?>"></div>
				</div>					
				<div class="summary">
					<div class="total">
						Całkowita cena dla 1 osoby:
						<b><?= \Yii::$app->price->getPrice($Item->id, 1, 1); ?> <span>PLN</span></b>
					</div>
					<p>Cena ze śniadaniem</p>
					Cena za osobę: <?= \Yii::$app->price->getPrice($Item->id, 1, 1); ?> PLN / za noc
				</div>
				<a href="/pl/pokoj/<?=$Item->alias;?>" class="link-details"><span class="orange">Zobacz szczegóły</span></a>
			</div>
		<div class="clearfix"></div>
	</div>
</div>
<?php endforeach; ?>