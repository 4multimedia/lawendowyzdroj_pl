<?php
	
	namespace app\modules\rooms\controllers;
	
	use Yii;
	use yii\web\Controller;
	use common\models\Rooms;
	use common\models\RoomsSearch;
	
	class DefaultController extends Controller
	{
	    public function actionSearch()
	    {
		    return $this->render('search');
	    }
	    
	    public function actionList()
	    {
		    $data["model"] = new RoomsSearch();
		    $data["params"] = Yii::$app->request->post();
			$data["items"] = $data["model"]->search($data["params"]);
		    
		    return $this->render('list', $data);
	    }
	    
	    public function actionView($alias)
	    {
		    $data["model"] = new RoomsSearch();
		    $data["item"] = $data["model"]->search(['alias' => $alias]);
		    
		    // STARE ZMIENNE DO ZMIANY Z YII 1.1
		    $data["BeedsCount"] = [];
		    
		    return $this->render('view', $data);
	    }
	}

?>