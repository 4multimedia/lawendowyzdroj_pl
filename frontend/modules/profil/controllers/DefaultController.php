<?php
	
	namespace app\modules\profil\controllers;
	
	use yii\web\Controller;
	
	class DefaultController extends Controller
	{
		public function actionIndex()
		{
			return $this->render('index');
		}
		
	    public function actionLogout()
	    {
		    \Yii::$app->user->logout();
			return $this->goHome();
	    }
	}

?>