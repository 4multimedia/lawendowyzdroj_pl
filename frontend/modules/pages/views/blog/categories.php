<div class="container">
	<div class="lead-header lead-header-slim">
		<h2><?=$category->name; ?> <small>Podkategoria</small></h2>
		<img src="/img/page/header_about_us.png">
	</div>
	
	<div class="content-page content-blog">
		<div class="box-special">
			<div class="row">
				<?php foreach ($items as $blog) : ?>
				<div class="col-md-4 item">
					<img src="<?=$blog->thumb;?>">
					<p><?=$blog->name;?></p>
					<span class="date"><?=$blog->date;?></span>
					<span class="summary"><?=$blog->summary;?></span>
					<a href="">czytaj więcej...</a>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>