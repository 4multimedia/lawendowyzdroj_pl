<?php
	
	namespace app\modules\pages\controllers;
	
	use yii\web\Controller;
	
	class KnowledgebaseController extends Controller
	{
	    public function actionIndex()
	    {
		    return $this->render('index');
	    }
	}

?>