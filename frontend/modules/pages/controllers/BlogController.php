<?php
	
	namespace app\modules\pages\controllers;
	
	use yii\web\Controller;
	use yii\helpers\ArrayHelper;
	use common\models\BlogPosts;
	use common\models\BlogCategories;
	
	class BlogController extends Controller
	{
	    public function actionIndex()
	    {
		    
	    }
	    
	    public function actionCategories()
	    {
		    $url = \Yii::$app->getRequest()->url;
		    $url = str_replace("/pl/", "", $url);
		    
		    $data["category"] = BlogCategories::find()->where(['alias' => $url])->one();  
		    $data["items"] = BlogPosts::find()->where(['id_category' => ArrayHelper::map(BlogCategories::find()->where(['id_parent' => $data["category"]->id])->all(), 'id', 'id')])->orderBy('date DESC')->all();
		    
		    return $this->render('categories', $data);
	    }
	}

?>