<?php
	
	namespace app\modules\promotions\controllers;
	
	use yii\web\Controller;
	
	class BestdealController extends Controller
	{
		public function actionRooms()
		{
			return $this->render('rooms');
		}
		
	    public function actionActivities()
	    {
		    return $this->render('activities');
	    }
	}

?>