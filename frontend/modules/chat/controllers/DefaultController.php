<?php
	
	namespace app\modules\chat\controllers;
	
	use yii\web\Controller;
	use common\models\Chat;
	
	class DefaultController extends Controller
	{
	    public function actionIndex()
	    {
		    $key = \Yii::$app->request->get('key');
		    $Chat = Chat::find()->where(['key' => $key])->orderBy('timestamp ASC')->all();
		    
		    if ($Chat)
		    {
			    foreach ($Chat as $Item)
			    {
				    $array["chat"][$Item->id][$Item->owner]["time"] = $Item->timestamp;
				    $array["chat"][$Item->id][$Item->owner]["text"] = $Item->text;
			    }
		    }
		    
		    echo json_encode($array);
	    }
	    
	    public function actionSave()
	    {
			$Chat = new Chat;
			$Chat -> key = \Yii::$app->request->post('key');
			$Chat -> timestamp = date("Y-m-d H:i:s");
			$Chat -> owner = \Yii::$app->request->post('owner');
			$Chat -> text = \Yii::$app->request->post('text');
			$Chat -> save();
	    }
	}

?>