<?php
	
	namespace app\modules\panel\controllers;
	
	use Yii;
	use yii\web\Controller;
	
	class CommunicationController extends Controller
	{
		public function behaviors()
	    {
	        return [
		        'verbs' => [
	                'class' => VerbFilter::className(),
	                'actions' => [
	                    'delete' => ['post'],
	                ],
	            ],
	            'access' => [
	                'class' => AccessControl::className(),
	                'only' => ['index', 'create', 'reply', 'remove'],
	                'rules' => [
	                    [
	                        'allow' => true,
	                        'actions' => ['index', 'create', 'reply', 'remove'],
	                        'roles' => ['owner','admin','receptionist'],
	                    ],
	                    [
	                        'allow' => true,
	                        'actions' => ['logout'],
	                        'roles' => ['@'],
	                    ],
	                ],
	            ],
	        ];
	    }
		
	    public function actionIndex()
	    {
		    return $this->render('index');
	    }
	}

?>