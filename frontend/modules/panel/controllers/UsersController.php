<?php
	
	namespace app\modules\panel\controllers;
	
	use Yii;
	use yii\web\Controller;
	use yii\filters\AccessControl;
	use yii\filters\VerbFilter;
	use common\models\UsersSearch;
	
	class ActivitiesController extends Controller
	{
		public function behaviors()
	    {
	        return [
		        'verbs' => [
	                'class' => VerbFilter::className(),
	                'actions' => [
	                    'delete' => ['post'],
	                ],
	            ],
	            'access' => [
	                'class' => AccessControl::className(),
	                'only' => ['index', 'create', 'update', 'remove'],
	                'rules' => [
	                    [
	                        'allow' => true,
	                        'actions' => ['index', 'create', 'update', 'remove'],
	                        'roles' => ['admin'],
	                    ],
	                    [
	                        'allow' => true,
	                        'actions' => ['logout'],
	                        'roles' => ['@'],
	                    ],
	                ],
	            ],
	        ];
	    }
		
	    public function actionIndex()
	    {
		    $data["model"] = new UsersSearch();
		    $data["params"] = Yii::$app->request->post();
			$data["items"] = $data["model"]->search($data["params"]);
		    
		    return $this->render('index', $data);
	    }
	    
	    public function actionUpdate($id)
	    {

	    }
	    
	    public function actionRemove($id)
	    {
		    
	    }
	}

?>