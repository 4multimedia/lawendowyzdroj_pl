<?php
	
	namespace app\modules\panel\controllers;
	
	use Yii;
	use yii\web\Controller;
	use common\models\Rooms;
	
	class ReservationsController extends Controller
	{
		public function behaviors()
	    {
	        return [
		        'verbs' => [
	                'class' => VerbFilter::className(),
	                'actions' => [
	                    'delete' => ['post'],
	                ],
	            ],
	            'access' => [
	                'class' => AccessControl::className(),
	                'only' => ['index', 'calendar'],
	                'rules' => [
	                    [
	                        'allow' => true,
	                        'actions' => ['index', 'calendar'],
	                        'roles' => ['owner','admin','receptionist'],
	                    ],
	                    [
	                        'allow' => true,
	                        'actions' => ['logout'],
	                        'roles' => ['@'],
	                    ],
	                ],
	            ],
	        ];
	    }
		
	    public function actionCalendar()
	    {
		    $data["rooms"] = Rooms::find()->where([])->all();
		    
		    return $this->render('calendar', $data);
	    }
	}

?>