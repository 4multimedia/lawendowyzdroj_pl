<?php
	
	namespace app\modules\panel\controllers;
	
	use Yii;
	use yii\web\Controller;
	use yii\filters\AccessControl;
	use yii\filters\VerbFilter;
	use common\models\RoomsSearch;
	
	class ActivitiesController extends Controller
	{
		public function behaviors()
	    {
	        return [
		        'verbs' => [
	                'class' => VerbFilter::className(),
	                'actions' => [
	                    'delete' => ['post'],
	                ],
	            ],
	            'access' => [
	                'class' => AccessControl::className(),
	                'only' => ['index', 'create', 'update', 'remove', 'availability'],
	                'rules' => [
	                    [
	                        'allow' => true,
	                        'actions' => ['index', 'create', 'update', 'remove', 'availability'],
	                        'roles' => ['receptionist','admin'],
	                    ],
	                    [
	                        'allow' => true,
	                        'actions' => ['logout'],
	                        'roles' => ['@'],
	                    ],
	                ],
	            ],
	        ];
	    }
		
	    public function actionIndex()
	    {
		    $data["model"] = new ActivitiesSearch();
		    $data["params"] = Yii::$app->request->post();
			$data["items"] = $data["model"]->search($data["params"]);
		    
		    return $this->render('index', $data);
	    }
	    
	    public function actionUpdate($id)
	    {
		    $data["model"] = new RoomsSearch();
			$data["items"] = $data["model"]->search(['id' => $id]);
		    
		    return $this->render('index', $data);
	    }
	    
	    public function actionRemove($id)
	    {
		    
	    }
	    
	    public function actionAvailability()
	    {
		    
	    }
	}

?>