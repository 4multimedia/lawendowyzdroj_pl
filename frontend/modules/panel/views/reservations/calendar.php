<?php
	
	use frontend\modules\panel\widgets\BookingCalendar;
	
?><div class="booking-area">
	<div class="booking-area-rooms">
		<div class="empty-header">
			&nbsp;
		</div>
		<?php foreach($rooms as $room) : ?>
			<div class="room"><?=$room->name;?></div>
		<?php endforeach; ?>
	</div>
	<div class="booking-area-months">
		<?php echo BookingCalendar::widget(['month' => date("m"), 'year' => date("Y")]) ?>
	</div>
</div>