<?php $this->renderPartial('menu-top'); ?>

<div class="panel-fluid panel-properties">
	<div class="area">
		<div>
			<div class="panel-top">
				<ul class="panel-breadcrumbs">
					<li><a href="/pl/panel">Strona główna</a></li>
					<li class="pause">/</li>
					<li><a href="/pl/panel/properties">Moje obiekty</a></li>
				</ul>
				<a href="javascript:;" class="btn-add-object">Dodaj nowy obiekt</a>
			</div>
			
			<div class="panel-top-tab">
				<?php $this->renderPartial('switch-account', array('type' => 'slim')); ?>
				<ul class="panel-tab panel-tab-right panel-tab-long">
					<li class="active"><a href="javascript:;">Wszystkie <i class="fa fa-caret-up"></i></a></li>
					<li><a href="javascript:;">SHOW MULTIUNITS <i class="fa fa-caret-up"></i></a></li>
					<li><a href="javascript:;">SHOW units ONLY <i class="fa fa-caret-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			
			<div class="panel-navigation">
					<div class="sort">
						<span>Sortuj według:</span>
						<a href="?order=date&by=<? if ($Sort["order"] == "date_created") { echo $Sort["next"]; } else { echo $Sort["by"]; } ?>">data utworzenia <i class="fa <? if ($Sort["order"] == "date_created" AND $Sort["by"] == "desc") { echo "active "; } ?>fa-caret-down"></i> <i class="fa <? if ($Sort["order"] == "date_created" AND $Sort["by"] == "asc") { echo "active "; } ?>fa-caret-up"></i></a>
						 | 
						<a href="?order=name&by=<? if ($Sort["order"] == "name") { echo $Sort["next"]; } else { echo $Sort["by"]; } ?>">nazwa obiektu <i class="fa <? if ($Sort["order"] == "name" AND $Sort["by"] == "desc") { echo "active "; } ?>fa-caret-down"></i> <i class="fa <? if ($Sort["order"] == "name" AND $Sort["by"] == "asc") { echo "active "; } ?>fa-caret-up"></i></a>
					</div>
					
					<div class="portion">
						Pokaż:
						<select style="width:70px">
							<option value="10"<?php if ($Limit == 10) { echo " selected=\"selected\""; } ?>>10</option>
							<option value="20"<?php if ($Limit == 20) { echo " selected=\"selected\""; } ?>>20</option>
							<option value="30"<?php if ($Limit == 30) { echo " selected=\"selected\""; } ?>>30</option>
							<option value="50"<?php if ($Limit == 50) { echo " selected=\"selected\""; } ?>>50</option>
							<option value="100"<?php if ($Limit == 100) { echo " selected=\"selected\""; } ?>>100</option>
						</select>
					</div>
					<div class="pagination">
						Wyświetlono <b><?=count($Objects);?></b> z <b><?=$Count;?></b> obiektów
						<?php $this->widget('CLinkPager',
							array(
								'pages' => $Pages,
								'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
								'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
								'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
								'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
								'header'=>'',
						)) ?>
					</div>
					
					
					<div class="clearfix"></div>
				</div>
			
		</div>
		
		<div class="copy-item"><div class="property-list"></div></div>
		
		<div class="reservation-periods">
			<p>Wybierz obiekt:</p>
			<select style="width:520px;">
				<?php for($o = 0; $o < count($ObjectsAll); $o++) { ?>
					<option value="<?=$ObjectsAll[$o]->id;?>"><?=$ObjectsAll[$o]->no;?>, <?=$ObjectsAll[$o]->name;?></option>
				<?php } ?>
			</select>
			<button class="violet show-property">Pokaż obiekt</button>
		</div>
		
		<div class="property-list">
			<?php for($o = 0; $o < count($Objects); $o++) { ?>
			<div class="item">
				<div class="title">
					<input type="checkbox">
						<? if ($Objects[$o]->active == 0) { ?>
							<span class="status">nieaktywny</span>
						<?php } ?>
					<?=$Objects[$o]->name;?>
					<span class="no"><?=$Objects[$o]->no;?></span>
				</div>
				<div class="info">
					<div class="thumb">
						<div class="image">
							<?=Objects::model()->image($Objects[$o]->id, 1, null, 200, 130);?>
						</div>
					</div>
					<div class="options">
						<div class="buttons">
							<a href="/pl/panel/availability/<?=$Objects[$o]->id;?>">Dostępność</a>
							<a href="/pl/panel/edit/<?=$Objects[$o]->id;?>">Edytuj</a>
							<a href="/pl/panel/reservations/<?=$Objects[$o]->id;?>">Rezerwacje</a>
							<a href="/pl/panel/pricelist/<?=$Objects[$o]->id;?>">Cennik</a>
							<a href="/pl/panel/community/<?=$Objects[$o]->id;?>">Komunikacja</a>
							<a href="/pl/panel/reviews/<?=$Objects[$o]->id;?>">Opinie</a>
						</div>
						<div class="location">
							<div class="text"><b>Busko Zdrój</b>, Polska</div>
							<? if ($Objects[$o]->active == 0) { ?>
							<a href="#">Opublikuj obiekt</a>
							<?php } ?>
						</div>
						<div class="alert">
							<img src="<?=Yii::app()->theme->baseUrl;?>/files/img/alert_info_orange.png">
							Ważne informacje, które musisz uzupełnić aby opublikować obiekt:
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<?php } ?>
		</div>
		
			<div class="panel-navigation">
					
					<div class="portion">
						Pokaż:
						<select style="width:70px">
							<option value="10"<?php if ($Limit == 10) { echo " selected=\"selected\""; } ?>>10</option>
							<option value="20"<?php if ($Limit == 20) { echo " selected=\"selected\""; } ?>>20</option>
							<option value="30"<?php if ($Limit == 30) { echo " selected=\"selected\""; } ?>>30</option>
							<option value="50"<?php if ($Limit == 50) { echo " selected=\"selected\""; } ?>>50</option>
							<option value="100"<?php if ($Limit == 100) { echo " selected=\"selected\""; } ?>>100</option>
						</select>
					</div>
					<div class="pagination">
						Wyświetlono <b><?=count($Objects);?></b> z <b><?=$Count;?></b> obiektów
						<?php $this->widget('CLinkPager',
							array(
								'pages' => $Pages,
								'firstPageLabel'=>'<i class="fa fa-angle-double-left"></i>',
								'lastPageLabel'=>'<i class="fa fa-angle-double-right"></i>',
								'prevPageLabel'=>'<i class="fa fa-angle-left"></i>',
								'nextPageLabel'=>'<i class="fa fa-angle-right"></i>',
								'header'=>'',
						)) ?>
					</div>
					
					
					<div class="clearfix"></div>
				</div>
		
	</div>
</div>