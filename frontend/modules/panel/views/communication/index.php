<?php $this->renderPartial('menu-top'); ?>

<?php

	if (Access::getType() == "owner")
	{
		$label_title = "Numer obiektu";
		$alert_empty = "Brak wiadomości w danym obiekcie";
		$label_reply = "Recepcja";
	}
	elseif (Access::getType() == "provider")
	{
		$label_title = "Numer turnusu";
		$alert_empty = "Brak wiadomości w danym turnusie";
		$label_reply = "Organizator";
	}	
	
?>

<div>
	<?php $this->renderPartial('community/menu', array('Items' => $Items, 'id' => $Item->id, 'UserID' => $UserID, 'DateID' => $DateID)); ?>
	<div class="panel-content">
		<div class="area">
			<div class="panel-top">
				<ul class="panel-breadcrumbs">
					<li><a href="/pl/panel">Strona główna</a></li>
					<li class="pause">/</li>
					<li><a href="/pl/panel/community">Komunikacja</a></li>
					<?php if ($Item) { ?>
						<li class="pause">/</li>
						<?php if ($Date) { ?>
							<li><a href="/pl/panel/community/<?=$Item->id;?>"><?=$Item->name;?> [<?=$label_title;?>: <?=$Item->no;?>]</a></li>
							<li class="pause">/</li>
							<li>Termin: <?=$Date->date_from;?> - <?= date("Y-m-d", strtotime('+'.$Item->days.' day', strtotime($Date->date_from))) ;?></li>
						<? } else { ?>
							<li><?=$Item->name;?> [<?=$label_title;?>: <?=$Item->no;?>]</li>
						<?php } ?>
					<?php } ?>
				</ul>
				<div>
				
				</div>
			</div>
			
			<? if ($Item AND Access::getType() == "owner") { echo $this->renderPartial('/panel/properties/record', array('Object' => $Item, 'type' => 'slim')); } ?>
			<? if ($Item AND Access::getType() == "provider") { echo $this->renderPartial('/panel/activity/record', array('Activity' => $Item, 'type' => 'slim')); } ?>
			

			<div>
				<?php if ($ConversationID) { ?>
					<a href="/pl/panel/community" class="pink return-community">Przejdź do listy wszystkich wątków</a>
					<div class="clearfix"></div>
				<?php } else { ?>
					<?php if (Access::getType() != "traveller") { ?>
					<ul class="panel-tab panel-tab-two-line">
						<li<?php if ($type == "all") { ?> class="active"<?php } ?>><a href="/pl/panel/community<?php if (isSet($Item->id)) { echo "/".$Item->id; } ?>">Wszystkie <small>zapytania i rozmowy</small><i class="fa fa-caret-up"></i></a></li>
						<li<?php if ($type == "question") { ?> class="active"<?php } ?>><a href="/pl/panel/community/question<?php if (isSet($Item->id)) { echo "/".$Item->id; } ?>">Zapytania <small>brak rezerwacji</small><i class="fa fa-caret-up"></i></a></li>
						<li<?php if ($type == "interview") { ?> class="active"<?php } ?>><a href="/pl/panel/community/interview<?php if (isSet($Item->id)) { echo "/".$Item->id; } ?>">Rozmowy <small>klient z rezerwacją</small> <i class="fa fa-caret-up"></i></a></li>
					</ul>
					
					<div class="panel-search">
						<form method="get" action=""><input type="text" value="<?=$_GET["string"];?>" name="string" placeholder="szukaj w wiadomościach"></form>
						<div class="buttons">
							<button class="pink"><i class="fa fa-print"></i> Drukuj</button>
							<button class="pink btn-move" data-toggle="modal" data-target="#myModal"><i class="fa fa-archive"></i> Przenieś</button>
							<button class="violet"><i class="fa fa-trash-o"></i> Usuń</button>
						</div>
					</div>
					<div class="clearfix"></div>
					<?php } else { ?>
					<div class="panel-search panel-search-right">
						<div class="buttons">
							<button class="violet"><i class="fa fa-trash-o"></i> Usuń</button>
						</div>
					</div>
					<div class="clearfix"></div>
					<?php } ?>
					
					<div class="panel-navigation">
						<div class="sort">
							<span>Sortuj według daty:</span>
							<a href="javascript;">otrzymania <i class="fa active fa-caret-down"></i> <i class="fa fa-caret-up"></i></a> | 
							<a href="javascript:;">ostatniej odpowiedzi <i class="fa fa-caret-down"></i> <i class="fa fa-caret-up"></i></a>
						</div>
						<div class="pagination">
							Wyświetlono <?=count($Messages);?> z <?=count($Messages);?> wiadomości
							<ul>
								<li><a href="javascript:;">1</a></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
				<?php } ?>
				
				<?php if ($Dates) { ?>
					<script>
						
						$(document).ready(function()
						{
							$(".reservation-periods button").click(function()
							{
								var url = "/pl/panel/community<? if ($type AND $type != "all") { echo "/".$type; } ?><? if (isSet($Item->id)) { echo "/".$Item->id; } ?>/"+$(".reservation-periods select").val();
								document.location.href=url;
							});
						})
						
					</script>
				
					<div class="reservation-periods">
						<p>Wybierz okres:</p>
						<select style="width:200px;">
							<option value="">wszystkie daty</option>
							<?php foreach($Dates as $date_id => $period) { ?>
								<option value="<?=$date_id;?>"<?php if ($date_id == $DateID) { echo "selected=\"selected\""; } ?>><?=$period;?></option>
							<?php } ?>
						</select>
						<button class="violet">Zobacz wiadomości</button>
					</div>
				<?php } ?>
				
				<?php if ($_GET["reservation_id"]) { ?>
				<form method="post" action="" class="form-converastion">
					<input type="hidden" name="reservation_id" value="<?=$_GET["reservation_id"];?>">
					<p>Wyślij wiadomość</p>
					<input type="text" name="subject" placeholder="Wpisz temat wiadomości">
					<textarea name="message"></textarea>
					<div class="buttons">
						<button>Wyślij wiadomość</button>
						<div class="clearfix"></div>
					</div>
				</form>
				<?php } else { ?>
				
				<!-- emails item begin -->
				<?php if (!$Messages) { ?>
				<div class="alert alert-danger"><?=$alert_empty;?></div>
				<?php } else { ?>
				<div class="messages">
					<?php for($m = 0; $m < count($Messages); $m++) { ?>

					
					<div class="message-area<?php if ($Messages[$m]["open"] == 0) {?> message-area-new<?php } ?>">
						<div class="message <?php if ($Messages[$m]["open"] == 0) { echo "closed"; } ?>" id="message_<?=$Messages[$m]["conversation_id"];?>">
							<?php $Params = json_decode($Messages[$m]["params"], true); ?>
							<div class="line">
								<div class="thumb">
									<?php if ($Messages[$m]["object_type"] == "object") { ?>
										<?=Objects::model()->image($Messages[$m]["object_id"], 1, 'crop', 78, 58);?>
									<?php } else if ($Messages[$m]["object_type"] == "stay") { ?>
										<?=Stays::model()->image($Messages[$m]["object_id"], 1, 'crop', 78, 58);?>
									<?php } ?>
								</div>
								<div class="check-item">
									<span>
										<input type="checkbox" class="check-box check-move" name="item[]" value="<?=$Messages[$m]["conversation_id"];?>">
									</span>
								</div>
								<div class="arrow">
									<?php if ($Messages[$m]["status_id"] == 2) { ?>
										<img src="<?=Yii::app()->theme->baseUrl;?>/files/img/message_reply.png">
										<?php } elseif ($Messages[$m]["status_id"] == 1) { ?>
									<img src="<?=Yii::app()->theme->baseUrl;?>/files/img/message_noreply.png">
									<?php } ?>
								</div>
								<div class="info">
									<span class="date"><?=$Messages[$m]["date"];?></span>
									<i class="fa fa-caret-down fa-<?php if ($Messages[$m]["status_id"] == 1) { ?>waiting<?php } ?>"></i>
									
									<span class="status status-<?php if ($Messages[$m]["status_id"] == 1) { ?>waiting<?php } ?>">
										<?php if ($Messages[$m]["status_id"] == 2) { ?>Wysłana<?php } else { ?>Otrzymana<?php } ?>
									</span>
									<span class="title title-<?php if ($Messages[$m]["status_id"] == 1) { ?>waiting<?php } ?>">
									<?=$Messages[$m]["subject"];?>
									</span>
									<div class="clearfix"></div>
									<div class="text" <?php if ($ConversationID) { echo "style=\"height:auto;\""; } ?>><?=nl2br($Messages[$m]["message"]);?></div>
								</div>
								<div class="summary">
									<?php if ($Messages[$m]["m_status"] == 1) { ?>
										<span>Status wiadomości</span>
										<span><b><?php if ($Messages[$m]["m_status"] == 0) { echo 'OTWARTE'; } else { echo 'ZAMKNIĘTE'; } ?></b></span>
									<?php } else { ?>
										<?php if (Access::getType() == "traveller") { ?>
											<?php if ($Messages[$m]["status_id"] == 2) { ?>
												<span><b>Oczekujesz na wiadomość</b></span>
											<?php } else { ?>
												<span><b>Recepcja oczekuje</b></span>
											<?php } ?>
										<?php } else { ?>
											<?php if ($Messages[$m]["status_id"] == 2) { ?>
												<span><b>Wiadomość wysłana</b></span>
											<?php } else { ?>
												<span><b>Klient czeka</b></span>
											<?php } ?>
										<?php } ?>
										<span><?=Communication::model()->awaiting($Messages[$m]["conversation_id"], 'object');?></span>
									<?php } ?>
									<button class="message-reply"><?php if ($Messages[$m]["status_id"] == 2) { ?>Napisz<? } else { ?>Odpowiedz<?php } ?></button>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="reply"<?php if ($ConversationID) { echo " style=\"display:block;\""; } ?>>
							<form method="post" action="" class="form-converastion">
								<input type="hidden" name="id" value="<?=$Messages[$m]["conversation_id"];?>">
								<p>Wyślij wiadomość</p>
								<input type="text" name="subject" value="Odp.: <?=$Messages[$m]["subject"];?>">
								<textarea name="message"></textarea>
								<div class="buttons">
									<div class="close-conversation"><input class="check-box" type="checkbox" name="closed" value="1"> <b>ZAMYKANIE TEMATU</b> (zakończ temat, jeśli nie oczekujesz na odpowiedź)</div>
									<!--<select style="width:240px">
										<option>wybierz z szablonu wiadomości</option>
									</select>
									<a href="javascript:;">zapisz jako szablon</a>-->
									<button>Wyślij wiadomość</button>
									<div class="clearfix"></div>
								</div>
							</form>
						</div>
						
						<div class="conversations"<?php if ($ConversationID) { echo " style=\"display:block;\""; } ?>>
							<?php
							
								$Criteria = new CDbCriteria;
								$Criteria -> condition = "conversation_id=:conversation_id";
								$Criteria -> params = array("conversation_id" => $Messages[$m]["conversation_id"]);
								$Criteria -> order = "date DESC";
							
								$Conversations = Communication::model()->findAll($Criteria);
								
								if ($Conversations AND count($Conversations) > 1)
								{
									echo '<ul>';
									
									for($c = 0; $c < count($Conversations); $c++)
									{
										
										if ($Conversations[$c]->user_from == $UserID)
										{
											$class = "send";
										}
										else if ($Conversations[$c]->id == $Conversations[$c]->conversation_id)
										{
											$class = "new";
										}
										else
										{
											$class = "re";
										}
								?>
								
								<li class="<?=$class;?>">
									<i class="fa fa-caret-down"></i>
									<span class="date"><?=$Conversations[$c]->date;?></span>
									<span class="from">
										<?php if ($Conversations[$c]->user_author_type == 2) { ?>
											Klient:
										<?php } else if ($Conversations[$c]->user_author_type == 3) { ?>
											Recepcja:
										<?php } else if ($Conversations[$c]->user_author_type == 4) { ?>
											Organizator:
										<?php } ?>
									</span>
									<span class="text" <?php if ($ConversationID) { echo "style=\"height:auto;\""; } ?>><b><?=$Conversations[$c]->subject;?></b><br><?=$Conversations[$c]->message;?></span>
									<div class="clearfix"></div>
								</li>
								
								<?php
									
									}
									
									echo '</ul>';
								}
								
							?>
						</div>
					</div>
					<?php } ?>
				</div>
				<?php } ?>
				
				<?php } ?>
				
				<!-- emails item end -->
				
				<?php if (!$ConversationID) { ?>
				<div class="panel-navigation">
					<div class="pagination">
						Wyświetlono <?=count($Messages);?> z <?=count($Messages);?> wiadomości
						<ul>
							<li><a href="javascript:;">1</a></li>
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
				<?php } ?>
				
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="" class="form-move-to" method="post">
			<input type="hidden" name="items_move" value="">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Przenieś wiadomość</h4>
			</div>
			<div class="modal-body">
				<ul class="stay-tree">
				<?php if ($Items) { ?>
					<?php for($o = 0; $o < count($Items); $o++) { ?>
						<? $Dates = StaysDates::model()->getPeriods($Items[$o]->id); ?>
						<li>
							<?php if ($Dates) { ?>
								<i class="fa fa-caret-right"></i>
							<?php } else { ?>
								<input type="radio" name="date_id" value="<?=$Items[$o]->id;?>|0">
							<?php } ?>
							<b><?=$Items[$o]->name;?></b>
						<?php if ($Dates) { ?>
						<ul>
							<?php foreach ($Dates as $date_id => $period_name) { ?>
								<li><input type="radio" name="date_id" value="<?=$Items[$o]->id;?>|<?=$date_id;?>"> <?=$period_name;?></li>
							<?php } ?>
						</ul>
						<?php } ?>
						</li>
					<?php } ?>
				<?php } ?>
				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
				<button type="submit" class="btn btn-success">Przenieś wiadomość</button>
			</div>
			</form>
		</div>
	</div>
</div>