<div class="container">
	<?php foreach ($items->models as $Item) : ?>
	
		<div class="item item-slim">
			<div class="title">
				<?=$Item->name;?>
				<span class="no"><?=$Item->no;?></span>
			</div>
			<div class="info">
				<div class="thumb">
								
				</div>
				<div class="options">
					<div class="buttons">
						<a href="/pl/panel/activity/availability/<?=$Item->id;?>">Dostępność</a>
						<a href="/pl/panel/activity/edit/<?=$Item->id;?>">Edytuj</a>
						<a href="/pl/panel/reservations/<?=$Item->id;?>">Rezerwacje</a>
						<a href="/pl/panel/activity/pricelist/<?=$Item->id;?>">Cennik</a>
						<a href="/pl/panel/community/<?=$Item->id;?>">Komunikacja</a>
						<a href="/pl/panel/reviews/<?=$Item->id;?>">Opinie</a>
					</div>
					<div class="location">
						<div class="text"><b>Busko Zdrój</b>, Polska</div>				
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	
	<?php endforeach; ?>
</div>