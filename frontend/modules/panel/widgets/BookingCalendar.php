<?php
	
	namespace frontend\modules\panel\widgets;
	
	use yii\base\Widget;
	use common\models\Rooms;
	use common\models\ReservationsBooking;
	
	class BookingCalendar extends Widget
	{
		public $month;
		public $year;
		
		public function run()
		{
			$data["month"]["name"] = "Luty";
		    $data["month"]["days"] = 28;
		    $data["month"]["year"] = $this->year;
		    
		    $data["rooms"] = Rooms::find()->where([])->all();
		    
		    $data["reserved"] = ReservationsBooking::reserved();
			
			return $this->render('bookingCalendar', $data);
		}
	}
	
?>