<?php
	
	use common\models\ReservationsBooking;
	
	$this->registerCssFile('/css/panel/bookingCalendar.css');
	$this->registerJsFile('/js/panel/bookingCalendar.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
	
?>
<div class="booking-header"><?=$month["name"];?></div>
<div class="booking-header">
	<?php for ($m = 1; $m <= $month["days"]; $m++) : ?>
		<div class="day">
			<?php $date = $month["year"]."-02-".($m < 10 ? "0" : "").$m; ?>
			<?php $day = date("N", strtotime($date)); ?>
			<div><?=$m;?></div>
			<div><?=$day;?></div>
		</div>
	<?php endfor; ?>
	<div class="clearfix"></div>
</div>
<div>
	<?php foreach ($rooms as $room) : ?>
	<div class="room-line">
		<?php for ($m = 1; $m <= $month["days"]; $m++) : ?>
		
			<?php $date = $month["year"]."-02-".($m < 10 ? "0" : "").$m; ?>
			<?php
				
				echo '<div id="'.str_replace("-", "", $date).'" class="avaibility" ondrop="drop(event)" ondragover="allowDrop(event)" ondragover="dragenter(event)">';
				if (isset($reserved[$room->id][$date])) : 
					
					$key = $reserved[$room->id][$date];
					$reservation = $reserved[$room->id][$reserved[$room->id][$date]];
					$days = ReservationsBooking::days($date, $reservation["date_end"]);
					
					$width = ($days-1) * 40;
					
					echo '<div id="'.$key.'" class="reserved-block reserved-start reserved-end" style="width:'.$width.'px;" draggable="true" ondragend="dragend(event)" ondragstart="drag(event)"> </div>';
					
					
				else :
					echo ' &nbsp; ';
				endif;
				
				echo '</div>';
			?>	
			
		<?php endfor; ?>
		<div class="clearfix"></div>
	</div>
	<?php endforeach; ?>
</div>