<?php
	
	namespace app\modules\activities\controllers;
	
	use Yii;
	use yii\web\Controller;
	use common\models\Activities;
	use common\models\ActivitiesSearch;
	use common\models\ActivitiesDates;
	
	class DefaultController extends Controller
	{
	    public function actionList()
	    {
		    $data["model"] = new ActivitiesSearch();
		    $data["params"] = Yii::$app->request->post();
			$data["items"] = $data["model"]->search($data["params"]);
		    
		    return $this->render('list', $data);
	    }
	    
	    public function actionSearch()
	    {
		    $session = Yii::$app->session;
		    if ($session->has('search_request'))
		    {
			    $search_request = $session->get('search_request');
				$data["params"]["begin"] = $search_request["begin"];
				$data["params"]["end"] = $search_request["end"];
				$data["params"]["category"] = $search_request["activities"];
				$data["params"]["elastic"] = $search_request["elastic"];
				
				$data["model"] = new ActivitiesSearch();
				$data["items"] = $data["model"]->search($data["params"]);
				
				return $this->render('list', $data);
		    }
		    else
		    {

		    }
	    }
	    
	    public function actionView($alias)
		{
			$date = $_GET["date"];
			$data["date"] = ActivitiesDates::find()->where(['id' => $date])->one();
			
			$data["item"] = Activities::find()->where(['alias' => $alias])->one();
			$data["item"]->date_from = $data["date"]->date_from;
			$data["item"]->date_to = $data["date"]->date_to;
			
			return $this->render('view', $data);
		}
		
		public function actionPackage($stay_id)
		{
			$date_id = $_GET["date_id"];
		}
		
	    public function actionModal()
	    {
		    $id = $_GET["id"];
		    
		    $data["model"] = new ActivitiesSearch();
			$data["items"] = $data["model"]->search(["id" => $id]);
			
			return Yii::$app->controller->renderPartial('modal', $data);
	    }
	}

?>