<?php
	
	namespace app\modules\activities\controllers;
	
	use Yii;
	use yii\web\Controller;
	use common\models\Activities;
	use common\models\ActivitiesSearch;
	use common\models\ActivitiesDates;
	
	class AjaxController extends Controller
	{
		public function actionCalculate()
		{
			
		}
		
		public function actionPopup()
		{
			$date_id = $_POST["date_id"];
			
			$ActivitesData = ActivitiesDates::find()->where(['id' => $date_id])->one();
			$Activites = Activities::find()->where(['id' => $ActivitesData->stays_id])->one();
			
			$array["period"] = Activities::period($ActivitesData->date_from, $ActivitesData->date_to);
			$array["days"] = $Activites->days;
			$array["name"] = $Activites->name;
			$array["image"] = $Activites->getImage(500, 370, 'lawendowy_zdroj_'.$Activites->name);
			
			echo json_encode($array);
		}
	}