<?php
	
	use yii\widgets\ActiveForm;
	use common\models\Activities;
	
?>

<?php foreach ($items->models as $Item) : ?>
<div class="stays" rel="stay-<?=$Stays[$s]->id;?>">
	<img src="http://lawendowyzdroj.pl/themes/lawendowyzdroj/files/img/recommended_gold.png" class="icon">
	<div class="stay-name">
		<h2><?=$Item->name;?></h2>
		<b><?=$Item->days;?> dni</b>
	</div>
	<div class="subtitle">
		<h3>Turnus</h3>
		<b class="period period-icon"><span>termin:</span> <?=Activities::period($Item->date_from, $Item->date_to);?></b>
	</div>
	<div class="stays-left">
			<div class="capacity">
				dostępnych miejsc: <b><?=$Item->capacity;?></b>!
			</div>
			<div class="thumb">
				<?php $image = Yii::getAlias('@app')."/web".urldecode($Item->image); ?>
				<?php if (file_exists($image) AND $Item->image != "") : ?>
					<img src="<?= Activities::image($Item->image, 500, 370, 'lawendowy_zdroj_'.$Item->name);?>">
				<?php else : ?>
					<img src="/img/gfx/logo_replacement.png" alt="">
				<?php endif; ?>
			</div>
		</div>
		<div class="stays-right">
			<ul class="tabs">
				<li class="active"><a href="javascript:;" rel="features">Główne cechy</a></li>
				<li><a href="javascript:;" rel="description">Krótki opis</a></li>
				<li class="extra"><a href="javascript:;" rel="terms"><i>!</i> Inne daty</a></li>
			</ul>
			<div class="features tab-content">
				<?=$Item->main_features;?>
			</div>
			<div class="description tab-content">
				<?=$Item->summary;?>
			</div>
			<div class="terms tab-content">

			</div>
			<div class="links">
				<a href="javascript:activityDetail(<?=$Item->stay_id;?>);" class="details orange">Pokaż szczegóły</a>
			</div>
		</div>
		<div class="clearfix">&nbsp;</div>
		
<div class="stay-package">
			
			
			<div class="package-area">
				
				<a class="create-package" href="/pl/stworz-wlasny-pakiet/36/567" id="stay_567">
					<img src="/img/gfx/arrow_hand.png" class="hand_left hand_arrow">
										<b>Wybierz swój pokój</b>, z aktualnie dostępnej oferty <br> Cena pakietu już od: <b>774 PLN</b>
										<span class="reservation-help show-help" rel="7" style="top:15px; right:85px;">?</span>
					<img src="/img/gfx/arrow_hand_right.png" class="hand_right hand_arrow">
				</a>
				
				<p class="title-package"><span>lub wybierz gotowy pakiet dla <b>1</b> osoby w lepszej cenie</span> <span class="reservation-help show-help" rel="1">?</span></p>
				
				<div>
				<?php $form = ActiveForm::begin(['action' => '/pl/turnus-leczniczy/'.$Item->alias.'?date='.$Item->stays_date_id, 'options' => ['class' => 'item-package']]); ?>
										<input name="package" type="hidden" value="1" kl_vkbd_parsed="true">
										<input name="price" type="hidden" value="627" kl_vkbd_parsed="true">
										<input name="price_clear" type="hidden" value="737" kl_vkbd_parsed="true">
											<button type="submit">
												<b>własny <br>pokój/pokoje</b>
												<p>KOMFORT</p>
												<span>602 <em>PLN</em> <div><strong>1</strong> Za<br>osobę</div></span>
												<em class="discount">602 PLN zamiast <strong>737</strong> PLN</em>
											</button>
				<?php ActiveForm::end(); ?>
				<?php $form = ActiveForm::begin(['action' => '/pl/turnus-leczniczy/'.$Item->alias.'?date='.$Item->stays_date_id, 'options' => ['class' => 'item-package']]); ?>
										<input name="package" type="hidden" value="2" kl_vkbd_parsed="true">
										<input name="price" type="hidden" value="645" kl_vkbd_parsed="true">
										<input name="price_clear" type="hidden" value="759" kl_vkbd_parsed="true">
											<button type="submit">
												<b>własny <br>pokój/pokoje</b>
												<p>PREMIUM</p>
												<span>619 <em>PLN</em> <div><strong>1</strong> Za<br>osobę</div></span>
												<em class="discount">619 PLN zamiast <strong>759</strong> PLN</em>
											</button>
				<?php ActiveForm::end(); ?>
				<?php $form = ActiveForm::begin(['action' => '/pl/turnus-leczniczy/'.$Item->alias.'?date='.$Item->stays_date_id, 'options' => ['class' => 'item-package']]); ?>
										<input name="package" type="hidden" value="3" kl_vkbd_parsed="true">
										<input name="price" type="hidden" value="682" kl_vkbd_parsed="true">
										<input name="price_clear" type="hidden" value="803" kl_vkbd_parsed="true">
											<button type="submit">
												<b>własny <br>pokój/pokoje</b>
												<p>PRESTIGE</p>
												<span>655 <em>PLN</em> <div><strong>1</strong> Za<br>osobę</div></span>
												<em class="discount">655 PLN zamiast <strong>803</strong> PLN</em>
											</button>
				<?php ActiveForm::end(); ?>
				</div>			
				<div class="clearfix" style="font-size:1px;">&nbsp;</div>
																	
				
			</div>
			
			
			
		</div>
</div>

<?php endforeach; ?>