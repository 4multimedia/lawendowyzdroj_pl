<?php
	
	use yii\widgets\Pjax;
	use frontend\widgets\WidgetsController;
	
	use frontend\widgets\modal\ActivitiesDetail;
	
?><div class="container">
	<div class="lead-header lead-header-slim">
		<h2>Turnusy lecznicze <small>Turnusy lecznicze, pakiety weekendowe - Busko-Zdrój</small></h2>
		<img src="/img/page/header_rooms.png">
	</div>
	<div class="content-page">
		<div class="box-special">
			<div class="row">
				<div class="col-md-9">
					<?php Pjax::begin([]); ?>
					<?php echo Yii::$app->controller->renderPartial('/layouts/_list', ['items' => $items]); ?>
					<?php Pjax::end(); ?>
				</div>
				<div class="col-md-3">
					<?php echo WidgetsController::widget(['action' => 'activities']) ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo ActivitiesDetail::widget([]) ?>