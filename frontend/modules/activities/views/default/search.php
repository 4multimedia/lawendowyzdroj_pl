<?php
	
	use yii\widgets\Pjax;
	
?><div class="container">
	<div class="lead-header lead-header-slim">
		<h2>Turnusy lecznicze <small>Oferta turnusów leczniczych i pobytów weekendowych</small></h2>
		<img src="/img/page/header_rooms.png">
	</div>
	<div class="content-page">
		<div class="box-special">
			<div class="row">
				<div class="col-md-9">
					<?php Pjax::begin([]); ?>
					<?php echo Yii::$app->controller->renderPartial('/layouts/_list', ['items' => $items]); ?>
					<?php Pjax::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>