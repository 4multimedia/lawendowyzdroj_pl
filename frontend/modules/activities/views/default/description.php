<div class="stay-title-descript"><?=$item->name;?></div>
<div class="stay-no">
	<span>Rodzaj turnusu:</span>
	<b>turnusy leczniczo-profilaktyczny</b>
</div>
<div class="stay-static-tab">
	<b class="tab">Turnus ID: <?=$item->id;?></b>
	<b class="name"><?=$item->slogan;?></b>
</div>

<div class="stay-prace">
	Cena już od
</div>

<div class="stay-terms">
	<b class="period-name"><?=$item->week;?></b>
	<div class="period">
		<em class="first">Start: <span> <?=$item->date_from;?></span></em>
		<em class="last">Koniec: <span> <?=$item->date_to;?></span></em>
	</div>
</div>

<div class="stay-option-boxes">
	<div class="stay-option-box stay-option-box-left">
		<p class="stay-title"><span>Co zawiera cena?</span> <a href="javascript:;" class="details">Szczegóły</a> <img src="/img/gfx/stay_title_option.png" class="header-stay-title"></p>
		<div class="text">
			<?=strip_tags($item->price_text, '<ul><li><b>');?>
		</div>
		<img src="/img/gfx/stay_bottom_shadow.gif">
	</div>
	<div class="stay-option-box stay-option-box-right">
		<p class="stay-title"><span>Metody leczenia</span> <a href="javascript:;" class="details">Szczegóły</a> <img src="/img/gfx/stay_title_option.png" class="header-stay-title"></p>
		<div class="text box-text-treatment">
			<?=nl2br(strip_tags($item->treatment_summary));?>
		</div>
		<img src="/img/gfx/stay_bottom_shadow.gif">
	</div>
	<div class="clearfix">&nbsp;</div>
</div>

<div class="recommended-text">
	<p><span>Turnus polecany dla:</span></p>
	<div></div>
</div>

<div class="contact-buttons">
	<a href="javascript:;" class="btn-contact open-communication contact-owner">Kontakt z organizatorem</a>
	<div class="clearfix"></div>
</div>

<div class="stay-program">
	Program turnusu
	<ul>
		<li><a href="javascript:;" rel="12" class="small">A</a></li>
		<li class="pause">&bull;</li>
		<li><a href="javascript:;"  rel="14">A</a></li>
		<li class="pause">&bull;</li>
		<li><a href="javascript:;"  rel="16" class="big">A</a></li>
	</ul>
</div>
		
<div class="stay-title">
	<h4>Szczegółowy opis <?=$item->name;?></h4>
</div>

<div class="descript">
	<div class="tabs">
		<p>Opisy</p>
		<ul>
			<li id="tab1" class="active"><a href="javascript:;"><span></span> Informacje podstawowe</a></li> 
			<li id="tab2"><a href="javascript:;"><span></span> Program turnusu</a></li> 
			<li id="tab3"><a href="javascript:;"><span></span> Metoda leczenia</a></li>
			<li id="tab4"><a href="javascript:;"><span></span> Zakwaterowanie</a></li>
			<li id="tab5"><a href="javascript:;"><span></span> Wyżywienie</a></li>
		</ul>
		<p>Płatności</p>
		<ul>
			<li id="tab9"><a href="javascript:;"><span></span> Cena</a></li> 
			<li id="tab10"><a href="javascript:;"><span></span> Metody płatności</a></li> 
			<li id="tab11"><a href="javascript:;"><span></span> Zwrotna kaucja</a></li>
			<li id="tab12"><a href="javascript:;"><span></span> Zasady anulacji</a></li>
		</ul>
		<p>Regulamin obiektu</p>
		<ul>
			<li id="tab13"><a href="javascript:;"><span></span> Zasady uproszczone</a></li> 
			<li id="tab14"><a href="javascript:;"><span></span> Zasady szczegółowe</a></li> 
		</ul>
	</div>
	<div class="tabs-content">
		<h4 class="title"><?=$item->slogan;?></h4>
		<div class="description-load-text" id="desc1">
			<?=$item->description;?>
		</div>
	</div>
	<div class="clearfix"></div>
</div>