<?php
	
	use frontend\widgets\WidgetsController;
	
?>
<div class="activity-header detail-header">
	
	<img src="/img/icon/recommended_gold.png" class="recommended">
	
	<img src="<?= $item->getImage(1200, 560, 'lawendowy_zdroj_'.$item->name);?>" class="image">
	
	<h4><?=$item->name;?></h4>
	<h5>Turnus leczniczy -  <?=$item->days;?> DNI</h5>
	
	<img src="/img/gfx/view_detail_header_gradient.png" class="gradient">
</div>

<div class="container">
	<form method="post" class="form-reservation-stay form-reservation-gallery">
		<div class="form-reservation-gallery-area ">
			<div class="box-option box-border-right box-option-terms">
				<p class="label-alert">Termin: <span class="reservation-help show-help">?</span></p>
				<div class="terms-dropdown"><?=$item->date_from;?> - <?=$item->date_to;?></div>
			</div>
			<div class="box-option box-border-left box-border-right box-option-person2">
				<div class="box-option-person">
					<p>Kobiety</p>
					<div class="box-option-spinner" data-max="4"><span class="minus">-</span> <u id="spinner_women">1</u> <span class="plus">+</span></div>
				</div>
				<div class="box-option-person">
					<p>Mężczyźni</p>
					<div class="box-option-spinner" data-max="4"><span class="minus">-</span> <u id="spinner_men">0</u> <span class="plus">+</span></div>
				</div>
			</div>
			<div class="box-option box-border-left box-border-right box-option-person1">
				<p>Osoby towarzyszące <span class="show-help" rel="11">?</span></p>
				<div class="box-option-spinner" data-max="3"><span class="minus">-</span> <u id="spinner_compan">0</u> <span class="plus">+</span></div>
			</div>
			<div class="box-option box-border-left box-border-right box-option-person2">
				<div class="box-option-person">
					<p>Dzieci 0-2 lat</p>
					<div class="box-option-spinner" data-max="3"><span class="minus">-</span> <u id="spinner_children1">0</u> <span class="plus">+</span></div>
				</div>
				<div class="box-option-person">
					<p>Dzieci 2-12 lat</p>
					<div class="box-option-spinner" data-max="3"><span class="minus">-</span> <u id="spinner_children3">0</u> <span class="plus">+</span></div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="box-option-package">
				<h4>Wybierz gotowy pakiet <span class="show-help" rel="2">?</span></h4>
					<div class="btn-pacakge" id="package-1">
										<div class="btn-package-border active">
											<button type="button">Pakiet KOMFORT
												<span>własny pokój/pokoje</span>
												<b><u>1099</u> PLN</b>
												<em>za 1 osobę</em>
											</button>
										</div>
									</div>
									
									<div class="btn-pacakge" id="package-2">
										<div class="btn-package-border ">
											<button type="button">Pakiet PREMIUM
												<span>własny pokój/pokoje</span>
												<b><u>1144</u> PLN</b>
												<em>za 1 osobę</em>
											</button>
										</div>
									</div>
									
									<div class="btn-pacakge" id="package-3">
										<div class="btn-package-border ">
											<button type="button">Pakiet PRESTIGE
												<span>własny pokój/pokoje</span>
												<b><u>1235</u> PLN</b>
												<em>za 1 osobę</em>
											</button>
										</div>
									</div>
													<div class="clearfix"></div>
				</div>
				
							
				<a href="/pl/stworz-wlasny-pakiet/53/743" class="create-own-package " id="stay_743">
					<span>
						<img src="/img/gfx/arrow_hand.png" class="hand_left hand_arrow">
												<u>lub stwórz <b>swój własny pakiet</b> z dostępnych aktualnie pokoi</u>
											<span class="show-help" rel="3">?</span>
						<img src="/img/gfx/arrow_hand_right.png" class="hand_right hand_arrow">
					</span>
				</a>
				
							
				<div class="btn-reservations">
					<div class="btn-left">
						<div class="btn-reservations-bordered" data-canceled="0">
							<p>Rezerwuj</p>
							<span>bez opcji anulacji</span>
							<b>1099 <small>PLN</small></b>
							<u>Wpłacasz całość teraz</u>
							
							<em>za 1 osobę</em>
							
							<span class="show-help" rel="10">?</span>
						</div>
					</div>
					<div class="btn-right">
						<div class="btn-reservations-bordered" data-canceled="1">
							<p>Rezerwuj</p>
							<span>z opcją anulacji</span>
							<b>1145 <small>PLN</small></b>
							<u>Wpłacasz teraz 30%</u>
							
							<em>za 1 osobę</em>
							
							<span class="show-help" rel="9">?</span>
						</div>
					</div>
					
					
				</div>
			</div>
		</form>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-9">
			<div class="box-special p10">
				<?=Yii::$app->controller->renderPartial('../../../activities/views/default/description', ['item' => $item]); ?>
			</div>
		</div>
		<div class="col-md-3">
			<?php echo WidgetsController::widget(['action' => 'activities']) ?>
		</div>
	</div>
</div>