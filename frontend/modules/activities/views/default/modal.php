<?php
	
		use common\models\Activities;
	
	foreach ($items->models as $item) : ?>
<div class="detail-header">
	
	<img src="/img/icon/recommended_gold.png" class="recommended">
	
	<?php $image = Yii::getAlias('@app')."/web".urldecode($item->image); ?>
	<?php if (file_exists($image) AND $item->image != "") : ?>
		<img src="<?= Activities::image($item->image, 1200, 460, 'lawendowy_zdroj_'.$item->name);?>" class="image">
	<?php endif; ?>
	
	<h4><?=$item->name;?></h4>
	<h5>Turnus leczniczy -  <?=$item->days;?> DNI</h5>
	
	<img src="/img/gfx/detail_header_gradient.png" class="gradient">
</div>
<div class="detail-subheader">
	NAJBLIŻSZY TERMIN - <span><?=Activities::period($item->date_from, $item->date_to);?></span> już od <span><b>1001</b></span> PLN za <span><b>1</b> osobę</span>
</div>

<div class="row">
	<div class="col-md-8">
		<?=Yii::$app->controller->renderPartial('../../../activities/views/default/description', ['item' => $item]); ?>
	</div>
	<div class="col-md-4">
		<div class="detail-right">
			<h5>Krótko turnusie</h5>
			<?=$item->summary;?>
			
			<h5>Główne cechy</h5>
			<?=$item->main_features;?>
			
			<h5>Dostępne terminy</h5>
			<ul class="dates">
				<?php foreach ($item->dates as $date) : ?>
					<li><a href="/turnusy-lecznicze/<?=$item->alias;?>"><?=mb_strtolower(Activities::period($date->date_from, $date->date_to), 'utf-8');?></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>

<?php
	endforeach;
?>