<?php
	
	namespace app\modules\treatments\controllers;
	
	use Yii;
	use yii\web\Controller;
	use common\models\Treatments;
	use common\models\TreatmentsCategories;
	
	class DefaultController extends Controller
	{
	    public function actionIndex()
	    {
		    $data["categories"] = TreatmentsCategories::find()->all();
		    
		    return $this->render('index', $data);
	    }
	}

?>