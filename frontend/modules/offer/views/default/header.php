	<div class="lead-header">
		<h2><?=$title;?> <small><?=$subtitle;?></small></h2>
		<img src="/img/offer/activity/header_activity.jpg">
		<img src="/img/gfx/lead_header_arrow.png" class="arrow_bottom">
		
		<div class="lead-tabs">
			<ul>
				<li<?php if ($tab == "calendar") : echo ' class="active"'; endif; ?>><a href="/pl/nasza-oferta"><i class="fa fa-calendar" aria-hidden="true"></i> Kalendarium</a></li>
				<li<?php if ($tab == "activities") : echo ' class="active"'; endif; ?>><a href="/pl/nasza-oferta/turnusy-lecznicze"><i class="fa fa-medkit" aria-hidden="true"></i> Turnusy lecznicze</a></li>
				<li<?php if ($tab == "packages") : echo ' class="active"'; endif; ?>><a href="/pl/nasza-oferta/pakiety-weekendowe"><i class="fa fa-sun-o" aria-hidden="true"></i> Pakiety weekendowe</a></li>
				<li<?php if ($tab == "treatments") : echo ' class="active"'; endif; ?>><a href="/pl/nasza-oferta/zabiegi"><i class="fa fa-life-ring" aria-hidden="true"></i> Zabiegi</a></li>
				
				<li<?php if ($tab == "rooms") : echo ' class="active"'; endif; ?>><a href="/pl/nasza-oferta/pokoje"><i class="fa fa-bed" aria-hidden="true"></i> Nasze pokoje</a></li>
				<li<?php if ($tab == "pricelist") : echo ' class="active"'; endif; ?>><a href="/pl/nasza-oferta/cennik"><i class="fa fa-money" aria-hidden="true"></i> Ceny</a></li>
				<li<?php if ($tab == "promotions") : echo ' class="active"'; endif; ?>><a href="/pl/nasza-oferta/oferty-promocyjne"><i class="fa fa-tags" aria-hidden="true"></i> Promocje</a></li>
			</ul>
		</div>
	</div>