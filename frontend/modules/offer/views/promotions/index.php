<?php
	
	use yii\widgets\Pjax;
	use yii\helpers\Html;
	
?>

<?php Pjax::begin([]); ?>
<div class="container">
	<?php echo Yii::$app->controller->renderPartial('/default/header', ['tab' => 'promotions', 'title' => $title, 'subtitle' => $subtitle]); ?>
	
	<div class="tab-offer-activity tab-offer-activity-bigger">
		<div class="row">
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == ""?"active_":"").'logo.png"><span>Atrakcyjne rabaty i ceny</span><h3>Wszystkie promocje</h3>', ['/pl/nasza-oferta/oferty-promocyjne'], ['class' => $type == ""?"active":""]);?>
			</div>
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "turnusy"?"active_":"").'logo.png"><span>Atrakcyjne rabaty i ceny</span><h3>Turnusy lecznicze</h3>', ['/pl/nasza-oferta/oferty-promocyjne/turnusy-lecznicze'], ['class' => $type == "tunrusy"?"active":""]);?>
			</div>
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "turnusy"?"active_":"").'logo.png"><span>Atrakcyjne rabaty i ceny</span><h3>Pakiety weekendowe</h3>', ['/pl/nasza-oferta/oferty-promocyjne/pakiety-weekendowe'], ['class' => $type == "tunrusy"?"active":""]);?>
			</div>
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "turnusy"?"active_":"").'logo.png"><span>Atrakcyjne rabaty i ceny</span><h3>Nasze pokoje</h3>', ['/pl/nasza-oferta/oferty-promocyjne/nasze-pokoje'], ['class' => $type == "tunrusy"?"active":""]);?>
			</div>
		</div>
	</div>
</div>
<?php Pjax::end(); ?>