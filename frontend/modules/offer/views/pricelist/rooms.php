<?php
	
	use common\models\Rooms;
	
	foreach ($items as $package_key => $package) : ?>
	<?php foreach($package as $persons => $item) : ?>
		<?php if (!empty($item)) : ?>
		<h4 class="pricelist-title">Pokój <?=$persons;?>-osobowy, pakiet <?=Rooms::classesName($package_key);?></h4>
		
		<div class="table-box">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="person-<?=$persons;?>">
				<thead>
					<tr>
						<td class="row-name"></td>
						<td>1 osoba</td>
						<td>2 osoby</td>
						<?php if ($persons >= 3) : echo '<td>3 osoby</td>'; else : endif ;?>
						<?php if ($persons >= 4) : echo '<td>4 osoby</td>'; else : endif ;?>
					</tr>
				</thead>
				<tbody>
					<?php for($d = 1; $d <= 8; $d++) : ?>
					<tr>
						<td class="row-name day"><?php if ($d == 8) : ?>Kolejna noc<? else : ?><?=$d;?> noc<?=($d>1?"e":"");?><?php endif; ?></td>
						<td>
							<div><?=\Yii::$app->general->price($item[1][$d], 2, 'PLN');?></td>
						<td>
							<?php
								if ($d == 8) : 
									$price_one = (($item[2][$d] / 2));
								else :
									$price_one = (($item[2][$d] / 2) / $d);
								endif;
								?>
							<div><span data-toggle="tooltip" data-placement="right" title="<?=\Yii::$app->general->price($price_one, 2, 'PLN');?> osoba / noc"><?=\Yii::$app->general->price($item[2][$d], 2, 'PLN');?></span></div>
							
						</td>
						<?php
							
						if ($persons >= 3) : 
						
							if ($d == 8) : 
								$price_one = (($item[3][$d] / 3));
							else :
								$price_one = (($item[3][$d] / 3) / $d);
							endif;
						
							echo '<td><div><span data-toggle="tooltip" data-placement="right" title="'.\Yii::$app->general->price($price_one, 2, 'PLN').' osoba / noc">'.\Yii::$app->general->price($item[3][$d], 2, 'PLN').'</span></div></td>';
						else : 
							//echo '<td></td>';
						endif ;
						
						?>
						
						<?php
							
						if ($persons >= 4) : 
						
							if ($d == 8) : 
								$price_one = (($item[4][$d] / 4));
							else :
								$price_one = (($item[4][$d] / 4) / $d);
							endif;
						
							echo '<td><div><span data-toggle="tooltip" data-placement="right" title="'.\Yii::$app->general->price($price_one, 2, 'PLN').' osoba / noc">'.\Yii::$app->general->price($item[4][$d], 2, 'PLN').'</span></div></td>';
						else : 
							//echo '<td></td>';
						endif ;
						
						?>
					</tr>
					<?php endfor; ?>
				</tbody>
			</table>
		</div>
		<?php endif; ?>
	<?php endforeach; ?>
<?php endforeach; ?>