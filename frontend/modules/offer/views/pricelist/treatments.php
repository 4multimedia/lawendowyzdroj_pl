<div class="box-special page-treatment p25">
	<h1>Lista zabiegów z cennikiem</h1>
	<h3 class="text-center">Cennik dotyczy zabiegów zakupionych poza pakietem</h3>
				
	<table border="0" cellpadding="0" cellspacing="0" width="100%" class="treatments">
		<?php foreach ($items as $category) : ?>
		<thead>
			<tr>
				<th colspan="4"><?=$category->name;?></th>
			</tr>
		</thead>
		<tbody>
			<tr class="dark">
				<td>Nazwa zabiegu</td>
				<td class="text-center">Wymagana konsultacja lekarza</td>
				<td class="text-center">Czas trwania</td>
				<td class="text-right">Cena</td>
			</tr>
			<?php foreach ($category->items as $item) : ?>
			<tr>
				<td><?=$item->name;?></td>
				<td class="text-center"><? if($item->medical_consultation == 1) { echo '<i class="fa fa-check" style="color:green"></i>'; } ?></td>
				<td class="text-center">
				<?php if ($item->time_begin OR $item->time_end) { ?>
					<?php if ($item->time_begin) { echo $item->time_begin.'-'; } ?>
					<?php if ($item->time_end) { echo $item->time_end; } ?> min
				<?php } else { ?>
					&nbsp;
				<?php } ?>
				</td>
				<td class="text-right"><? if($item->price_package == 1) { echo 'w cenie pobytu'; } else { echo $item->price.' PLN'; } ?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
		<?php endforeach; ?>
	</table>
</div>