<?php
	
	use yii\widgets\Pjax;
	use yii\helpers\Html;
	
?>

<?php Pjax::begin([]); ?>
<div class="container">
	<?php echo Yii::$app->controller->renderPartial('/default/header', ['tab' => 'pricelist', 'title' => $title, 'subtitle' => $subtitle]); ?>
	
	<div class="tab-offer-activity tab-offer-activity-bigger">
		<div class="row">
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "nasze-pokoje"?"active_":"").'logo.png"><span>Klimatyczne pokoje od jedno- do czteroosobowych</span><h3>Nasze pokoje</h3>', ['/pl/nasza-oferta/cennik/nasze-pokoje'], ['class' => $type == "nasze-pokoje"?"active":""]);?>
			</div>
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "turnusy-lecznicze"?"active_":"").'logo.png"><span>Klimatyczne pokoje od jedno- do czteroosobowych</span><h3>Turnusy lecznicze</h3>', ['/pl/nasza-oferta/cennik/turnusy-lecznicze'], ['class' => $type == "turnusy-lecznicze"?"active":""]);?>
			</div>
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "pakiety-weekendowe"?"active_":"").'logo.png"><span>Klimatyczne pokoje od jedno- do czteroosobowych</span><h3>Pakiety weekendowe</h3>', ['/pl/nasza-oferta/cennik/pakiety-weekendowe'], ['class' => $type == "pakiety-weekendowe"?"active":""]);?>
			</div>
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "zabiegi"?"active_":"").'logo.png"><span>Klimatyczne pokoje od jedno- do czteroosobowych</span><h3>Zabiegi</h3>', ['/pl/nasza-oferta/cennik/zabiegi'], ['class' => $type == "zabiegi"?"active":""]);?>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="items-offer-rooms items-offer-rooms-mt-m15">
		<div class="row">
			<?php if ($cols == true) : ?>
			<div class="col-md-3">
				<?php if (in_array('persons', $mods)) : ?>
				<ul class="left-tabs left-tabs-mt-20">
					<li<?php if($persons == "wszystkie") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/cennik/<?=$type;?>/wszystkie/<?=$class;?><? if ($seassons) : echo '/'.$seassons; endif; ?><? if ($category) : echo '/'.$category; endif; ?>">
							<p>Wszystkie</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($persons == "2-osobowe") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/cennik/<?=$type;?>/2-osobowe/<?=$class;?><? if ($seassons) : echo '/'.$seassons; endif; ?><? if ($category) : echo '/'.$category; endif; ?>">
							<p>2-osobowe</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($persons == "3-osobowe") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/cennik/<?=$type;?>/3-osobowe/<?=$class;?><? if ($seassons) : echo '/'.$seassons; endif; ?><? if ($category) : echo '/'.$category; endif; ?>">
							<p>3-osobowe</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($persons == "4-osobowe") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/cennik/<?=$type;?>/4-osobowe/<?=$class;?><? if ($seassons) : echo '/'.$seassons; endif; ?><? if ($category) : echo '/'.$category; endif; ?>">
							<p>4-osobowe</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
				</ul>
				<?php endif; ?>
				
				<?php if (in_array('class', $mods)) : ?>
				<ul class="left-tabs left-tabs-mt-20">
					<li<?php if($class == "wszystkie") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/cennik/<?=$type;?>/<?=$persons;?>/wszystkie<? if ($seassons) : echo '/'.$seassons; endif; ?><? if ($category) : echo '/'.$category; endif; ?>">
							<p>Wszystkie</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($class == "komfort") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/cennik/<?=$type;?>/<?=$persons;?>/komfort<? if ($seassons) : echo '/'.$seassons; endif; ?><? if ($category) : echo '/'.$category; endif; ?>">
							<p>Komfort</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($class == "premium") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/cennik/<?=$type;?>/<?=$persons;?>/premium<? if ($seassons) : echo '/'.$seassons; endif; ?><? if ($category) : echo '/'.$category; endif; ?>">
							<p>Premium</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($class == "prestige") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/cennik/<?=$type;?>/<?=$persons;?>/prestige<? if ($seassons) : echo '/'.$seassons; endif; ?><? if ($category) : echo '/'.$category; endif; ?>">
							<p>Prestige</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
				</ul>
				<?php endif; ?>
				
				<?php if (in_array('seassons', $mods)) : ?>
				<ul class="left-tabs left-tabs-mt-20">
					<li<?php if($seassons == "nowy-rok") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/cennik/<?=$type;?>/<?=$persons;?>/<?=$class;?>/nowy-rok">
							<p>Nowy rok</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($seassons == "swieta") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/cennik/<?=$type;?>/<?=$persons;?>/<?=$class;?>/swieta">
							<p>Święta</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($seassons == "sezon-wysoki") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/cennik/<?=$type;?>/<?=$persons;?>/<?=$class;?>/sezon-wysoki">
							<p>Sezon wysoki</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($seassons == "sezon-sredni") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/cennik/<?=$type;?>/<?=$persons;?>/<?=$class;?>/sezon-sredni">
							<p>Sezon średni</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($seassons == "sezon-niski") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/cennik/<?=$type;?>/<?=$persons;?>/<?=$class;?>/sezon-niski">
							<p>Sezon niski</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
				</ul>
				<?php endif; ?>
				
				<?php if (in_array('type', $mods)) : ?>
				<ul class="left-tabs left-tabs-mt-20">
					<li<?php if($category == "wszystkie") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/cennik/<?=$type;?>/<?=$persons;?>/<?=$class;?>/wszystkie">
							<p>Wszystkie</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<?php foreach ($categories as $alias_category => $name_category) : ?>
						<li<?php if($category == $alias_category) : echo ' class="active"' ; endif; ?>>
							<a href="/pl/nasza-oferta/cennik/<?=$type;?>/<?=$persons;?>/<?=$class;?>/<?=$alias_category;?>">
								<p><?=$name_category;?></p>
								<span>Lorem ipsum dolor sit amet</span>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
				<?php endif; ?>
			</div>
			<?php endif; ?>
			<div class="col-md-<?=$cols == true ? 9 : 12;?>">
				<?php echo Yii::$app->controller->renderPartial($view, ['items' => $items]); ?>
			</div>
		</div>
	</div>
</div>

<?php Pjax::end(); ?>