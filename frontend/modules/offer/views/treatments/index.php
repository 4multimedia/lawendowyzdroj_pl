<?php
	
	use yii\widgets\Pjax;
	
?><div class="container">
	<?php echo Yii::$app->controller->renderPartial('/default/header', ['tab' => 'treatments', 'title' => $title, 'subtitle' => $subtitle]); ?>
	
	<?php Pjax::begin(['id' => 'myPjax']); ?>
	
	<div class="treatments-content">
		<div class="row">
			<div class="col-md-3">
				<a href="/pl/nasza-oferta/cennik/zabiegi" class="change-view">
					<i class="fa fa-refresh" aria-hidden="true"></i>
					Zmień widok na cennik prosty
				</a>
				
				<ul class="left-tabs">
					<li<?php if($alias == "") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/zabiegi">
							<p>Wszystkie</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<?php foreach ($categories as $category) : ?>
					<li<?php if($category->alias == $alias) : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/zabiegi/<?=$category->alias;?>">
							<p><?=$category->name;?></p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="col-md-9">
				<div class="row">
				<?php foreach ($items->models as $Item) : ?>
					<div class="col-md-6">
						<div class="flip-container">
						<div class="flipper">
							<div class="front">
								<div class="card">
								<div class="item-offer-activity item-offer-room treatments-item">
									<h4><?=$Item->name;?></h4>
									<div class="thumb"></div>
										<div class="bottoms">
											<div class="row">
												<div class="col-md-5">
													<?php if ($Item->price) : ?>
														Cena: <?=$Item->price;?> zł
													<?php endif; ?>
												</div>
												<div class="col-md-7 text-right">
													<?php if ($Item->time != false) : ?>
														Czas: <?=$Item->time;?>
													<?php endif; ?>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<a href="javascript:;" class="btn btn-block btn-info">Szczegóły</a>
											</div>
											<div class="col-md-6">
												<a href="/turnusy-lecznicze/<?=$item->alias;?>" class="btn btn-block btn-success">Zamów</a>
											</div>
										</div>
									</div>

								</div>
							</div>
							<div class="back">
								<div class="item-offer-activity item-offer-room treatments-item">
									<h4><?=$Item->name;?></h4>
									<div class="row">
										<div class="col-md-12">
											<p class="info"></p>
											<div class="row">
												<div class="col-md-12 tab-card-contents">
													<ul class="card-tabs card-tabs-half">
														<li data-tab="1" class="active">Opis zabigu</li>
														<li data-tab="2">Zalecenia</li>
													</ul>
													<div class="tab-card-content">
														<div class="tab_1 tab_0">
															<?=$Item->summary;?>
														</div>
														<div class="tab_2 tab_0">
															
														</div>
													</div>
												</div>
											</div>
											<div class="bottoms">
												<div class="row">
													<div class="col-md-5">
														<?php if ($Item->price) : ?>
															Cena: <?=$Item->price;?> zł
														<?php endif; ?>
													</div>
													<div class="col-md-7 text-right">
														<?php if ($Item->time != false) : ?>
															Czas: <?=$Item->time;?>
														<?php endif; ?>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<a href="javascript:;" class="btn btn-block btn-info">Szczegóły</a>
												</div>
												<div class="col-md-6">
													<a href="/turnusy-lecznicze/<?=$item->alias;?>" class="btn btn-block btn-success">Zamów</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
	
	<?php Pjax::end(); ?>
</div>