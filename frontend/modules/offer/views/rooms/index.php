<?php
	
	use yii\widgets\Pjax;
	use yii\helpers\Html;
	use common\models\Rooms;
	use common\models\RoomsAmenities;
	
?><div class="container">
	<?php echo Yii::$app->controller->renderPartial('/default/header', ['tab' => 'rooms', 'title' => $title, 'subtitle' => $subtitle]); ?>

	<?php Pjax::begin([]); ?>
	<div class="tab-offer-activity">
		<div class="row">
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "wszystkie"?"active_":"").'logo.png"><span>Klimatyczne pokoje od jedno- do czteroosobowych</span><h3>Wszystkie</h3>', ['/pl/nasza-oferta/pokoje/wszystkie/'.$class.'/'.$seasson], ['class' => $type == "wszystkie"?"active":""]);?>
			</div>
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "2-osobowe"?"active_":"").'logo.png"><span>Pokoje idealne dla jednej lub dwóch osób</span><h3>2-osobowe</h3>', ['/pl/nasza-oferta/pokoje/2-osobowe/'.$class.'/'.$seasson], ['class' => $type == "2-osobowe"?"active":""]);?>
			</div>
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "3-osobowe"?"active_":"").'logo.png"><span>Atrakcyjne pokoje dla Twojej rodziny</span><h3>3-osobowe</h3>', ['/pl/nasza-oferta/pokoje/3-osobowe/'.$class.'/'.$seasson], ['class' => $type == "3-osobowe"?"active":""]);?>
			</div>
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "4-osobowe"?"active_":"").'logo.png"><span>Wyjątkowe pokoje dla Ciebie i Twoich przyjaciół</span><h3>4-osobowe</h3>', ['/pl/nasza-oferta/pokoje/4-osobowe/'.$class.'/'.$seasson], ['class' => $type == "4-osobowe"?"active":""]);?>
			</div>
		</div>
	</div>
	
	<div class="items-offer-rooms items-offer-rooms-mt-m15">
		<div class="row">
			<div class="col-md-3">
				<ul class="left-tabs left-tabs-mt-20">
					<li<?php if($class == "wszystkie") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/pokoje/<?=$type;?>/wszystkie/<?=$seasson;?>">
							<p>Wszystkie</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($class == "komfort") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/pokoje/<?=$type;?>/komfort/<?=$seasson;?>">
							<p>Komfort</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($class == "premium") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/pokoje/<?=$type;?>/premium/<?=$seasson;?>">
							<p>Premium</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($class == "prestige") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/pokoje/<?=$type;?>/prestige/<?=$seasson;?>">
							<p>Prestige</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
				</ul>
				
				<? /*
				<ul class="left-tabs left-tabs-mt-20">
					<li<?php if($seasson == "nowy-rok") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/pokoje/<?=$type;?>/<?=$class;?>/nowy-rok">
							<p>Nowy rok</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($seasson == "swieta") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/pokoje/<?=$type;?>/<?=$class;?>/swieta">
							<p>Święta</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($seasson == "sezon-wysoki") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/pokoje/<?=$type;?>/<?=$class;?>/sezon-wysoki">
							<p>Sezon wysoki</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($seasson == "sezon-sredni") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/pokoje/<?=$type;?>/<?=$class;?>/sezon-sredni">
							<p>Sezon średni</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
					<li<?php if($seasson == "sezon-niski") : echo ' class="active"' ; endif; ?>>
						<a href="/pl/nasza-oferta/pokoje/<?=$type;?>/<?=$class;?>/sezon-niski">
							<p>Sezon niski</p>
							<span>Lorem ipsum dolor sit amet</span>
						</a>
					</li>
				</ul>
				*/ ?>
			</div>
			<div class="col-md-9">
				<div class="row">
				<?php foreach ($items->models as $item) : ?>
				<div class="col-md-6">
					<div class="flip-container">
						<div class="flipper">
							<div class="front">
								<div class="item-offer-activity item-offer-room">
									<img src="/img/icon/recommended_gold.png" class="recommended">
									<div class="row">
										<div class="col-md-12">
											<p class="info"><?=$item->max_person;?>-osobowy</p>
											<div class="thumb">
												
												<?php $image = Yii::getAlias('@app')."/web".urldecode($item->image); ?>
												<?php if (file_exists($image) AND $item->image != "") : ?>
													<img src="<?= Rooms::image($item->image, 400, 230, 'lawendowy_zdroj_pokoj_'.$item->name);?>">
												<?php endif; ?>
											</div>
										</div>
									</div>
									<h3><small><?=$item->subname;?></small> <?=$item->name;?></h3>
									<div class="row">
										<div class="col-md-6">
											<a href="" class="btn btn-block btn-info">Szczegóły</a>
										</div>
										<div class="col-md-6">
											<a href="" class="btn btn-block btn-success">Zarezerwuj</a>
										</div>
									</div>
								</div>
							</div>
							<div class="back">
								<div class="item-offer-activity item-offer-room">
									<img src="/img/icon/recommended_gold.png" class="recommended">
									<div class="row">
										<div class="col-md-12">
											<p class="info"><?=Rooms::classesName($item->class);?></p>
											<div class="row">
												<div class="col-md-12 tab-card-contents">
													<ul class="card-tabs card-tabs-half">
														<li data-tab="1" class="active">Opis pokoju</li>
														<li data-tab="2">Udogodnienia</li>
													</ul>
													<div class="tab-card-content">
														<div class="tab_1 tab_0">
															<?=$item->summary;?>
														</div>
														<div class="tab_2 tab_0">
															<?php
																$amenities = array();
																if ($item->amenities) { $amenities = explode(",", $item->amenities); }
																
																
															?>
															<div class="tab-amenities">
																<div class="row">
																	<?php for($a = 1; $a <= 12; $a++) : ?>
																		<div class="col-md-3"><img
																			<?php if (in_array($a, $amenities)) : ?>
																			 data-toggle="tooltip" data-placement="bottom" title="<?=RoomsAmenities::label($a);?>"
																			 <?php endif; ?>
																			 style="opacity:0.<?php if (in_array($a, $amenities)) : echo 7; else: echo 2; endif; ?>" src="/img/icon/amenities<?=$a;?>.png"></div>
																	<?php endfor; ?>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<h3><small><?=$item->subname;?></small> <?=$item->name;?></h3>
									<div class="row">
										<div class="col-md-6">
											<a href="" class="btn btn-block btn-info">Szczegóły</a>
										</div>
										<div class="col-md-6">
											<a href="" class="btn btn-block btn-success">Zarezerwuj</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
	<?php Pjax::end(); ?>
</div>