<?php
	
	use yii\widgets\Pjax;
	use yii\helpers\Html;
	
	$this->title = 'Oferta turnusów leczniczych w Lawendowym Zdroju - Busko Zdrój';
	
?><div class="container">
	<?php echo Yii::$app->controller->renderPartial('/default/header', ['tab' => 'activities', 'title' => $title, 'subtitle' => $subtitle]); ?>
	
	<?php Pjax::begin([
		'id' => 'myPjax'
	]); ?>
	<div class="tab-offer-activity">
		<div class="row">
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == ""?"active_":"").'logo.png"><span>Sanatoryjne, profilaktyczno-lecznicze i specjalistyczne</span><h3>Wszystkie</h3>', ['/pl/nasza-oferta/turnusy-lecznicze/'], ['class' => $type == ""?"active":""]);?>
			</div>
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "sanatoryjne"?"active_":"").'logo.png"><span>Zadbaj o swoje zdrowie w profesjonalny sposób</span><h3>Sanatoryjne</h3>', ['/pl/nasza-oferta/turnusy-lecznicze/sanatoryjne'], ['class' => $type == "sanatoryjne"?"active":""]);?>
			</div>
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "profilaktyczno-lecznicze"?"active_":"").'logo.png"><span>Wykłady edukacyjne, zdrowa kuchnia/dietoterapia</span><h3>Profilaktyczne</h3>', ['/pl/nasza-oferta/turnusy-lecznicze/profilaktyczno-lecznicze'], ['class' => $type == "profilaktyczno-lecznicze"?"active":""]);?>
			</div>
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "specjalistyczne"?"active_":"").'logo.png"><span>Holistyczna terapia manualna</span><h3>Specjalistyczne</h3>', ['/pl/nasza-oferta/turnusy-lecznicze/specjalistyczne'], ['class' => $type == "specjalistyczne"?"active":""]);?>
			</div>
		</div>
	</div>
	
	<div class="items-offer-activity">
		<?=$Items;?>
	</div>
	
	<h3 class="title-lead text-violet"><span data-title="Oferty aktualnie niedostępne">Oferty aktualnie niedostępne</span></h3>
	
	<p class="text-center f18 text-red">Jeśli jesteś zainteresowany turnusem aktualnie nie zaplanowanym, skontaktuj się z nami -<br>być może specjalnie dla Ciebie go zorganizujemy</p><br><br>
	
	<div class="items-offer-activity">
		<?=$Disabled;?>
	</div>
	
	<?php Pjax::end(); ?>
</div>
