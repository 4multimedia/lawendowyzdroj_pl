<?php
	
	use common\models\Activities;
	
	use frontend\widgets\modal\ActivitiesDetail;
	
?><div class="row">
	<?php foreach ($items->models as $item) : ?>
	<?php $active = true; if (strtotime($item->date_to) < time()) : $active = false; endif; ?>
	<div class="col-md-6">
		<div class="flip-container">
			<div class="flipper">
				<div class="front">
					<div class="item-offer-activity">
						<img src="/img/icon/recommended_gold.png" class="recommended">
						<div class="row">
							<div class="col-md-6">
								<?php if ($active == true) : ?>
									<p class="info"><?=Activities::period($item->date_from, $item->date_to);?></p>
								<?php else : ?>
									<p class="info">Oferta niedostępna</p>
								<?php endif; ?>
							</div>
							<div class="col-md-6">
								<p class="info"><?=$item->days;?> dni</p>
							</div>
							<div class="col-md-12">
								<div class="thumb">
									<?php $image = Yii::getAlias('@app')."/web".urldecode($item->image); ?>
									<?php if (file_exists($image) AND $item->image != "") : ?>
										<img src="<?= Activities::image($item->image, 500, 200, 'lawendowy_zdroj_'.$item->name);?>">
									<?php else : ?>
										<?php echo $item->image; ?>
										<img src="/img/gfx/logo_replacement.png" alt="">
									<?php endif; ?>
								</div>
							</div>
						</div>
						<h3><?=$item->name;?></h3>
						<div class="row">
							<div class="col-md-6">
								<a href="javascript:activityDetail();" class="btn btn-block btn-info">Szczegóły</a>
							</div>
							<div class="col-md-6">
								<?php if ($active == true) : ?>
									<a href="/pl/turnus-leczniczy/<?=$item->alias;?>?date=<?=$item->stays_date_id;?>" class="btn btn-block btn-success">Zarezerwuj pobyt</a>
								<?php else : ?>
									<a href="javascript:;>" class="btn btn-block btn-success">Zapytaj o dostępność</a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="back">
					<div class="item-offer-activity">
						<img src="/img/icon/recommended_gold.png" class="recommended">
						<div class="row">
							
							<div class="col-md-6">
								<?php if ($active == true) : ?>
									<p class="info"><?=Activities::period($item->date_from, $item->date_to);?></p>
								<?php else : ?>
									<p class="info">Oferta niedostępna</p>
								<?php endif; ?>
							</div>
							<div class="col-md-6">
								<p class="info"><?=$item->days;?> dni</p>
							</div>
							<div class="col-md-12 tab-card-contents">
								<ul class="card-tabs">
									<li data-tab="1" class="active">Główne cechy</li>
									<li data-tab="2">Krótki opis</li>
									<li data-tab="3">Dostępne terminy</li>
								</ul>
								<div class="tab-card-content">
									<div class="tab_1 tab_0">
										<?=$item->main_features;?>
									</div>
									<div class="tab_2 tab_0">
										<?=$item->summary;?>
									</div>
									<div class="tab_3 tab_0">
										<ul class="dates">
											<?php foreach ($item->dates as $date) : ?>
												<li><a href="/pl/turnus-leczniczy/<?=$item->alias;?>?date=<?=$item->stays_date_id;?>"><?=mb_strtolower(Activities::period($date->date_from, $date->date_to), 'utf-8');?></a></li>
											<?php endforeach; ?>
										</ul>
									</div>
								</div>
							</div>
							
						</div>
						<h3><?=$item->name;?></h3>
						<div class="row">
							<div class="col-md-6">
								<a href="javascript:activityDetail(<?=$item->stay_id;?>);" class="btn btn-block btn-info">Szczegóły</a>
							</div>
							<div class="col-md-6">
								<?php if ($active == true) : ?>
									<a href="/pl/turnus-leczniczy/<?=$item->alias;?>?date=<?=$item->stays_date_id;?>" class="btn btn-block btn-success">Zarezerwuj pobyt</a>
								<?php else : ?>
									<a href="javascript:;>" class="btn btn-block btn-success">Zapytaj o dostępność</a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endforeach; ?>
	
</div>

<?php echo ActivitiesDetail::widget([]) ?>