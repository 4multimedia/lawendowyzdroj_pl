<?php
	
	use yii\widgets\Pjax;
	use yii\helpers\Html;
	
	$this->title = 'Oferta pobytów weekendowych w Lawendowym Zdroju - Busko Zdrój';
	
?><div class="container">
	<?php echo Yii::$app->controller->renderPartial('/default/header', ['tab' => 'packages', 'title' => $title, 'subtitle' => $subtitle]); ?>

	<?php Pjax::begin([]); ?>
	<div class="tab-offer-activity">
		<div class="row">
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == ""?"active_":"").'logo.png"><span>SPA & Wellness, okazjonalne i profilaktyczno-lecznicze</span><h3>Wszystkie</h3>', ['/pl/nasza-oferta/pakiety-weekendowe'], ['class' => $type == ""?"active":""]);?>
			</div>
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "spa-i-wellness"?"active_":"").'logo.png"><span>Zabiegi relaksacyjne, masaże, strefa SPA</span><h3>SPA & Wellness</h3>', ['/pl/nasza-oferta/pakiety-weekendowe/spa-i-wellness'], ['class' => $type == "spa-i-wellness"?"active":""]);?>
			</div>
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "pakiety-okazjonalne"?"active_":"").'logo.png"><span>Atrakcyjne zabiegi, wyjątkowy czas</span><h3>Okazjonalne</h3>', ['/pl/nasza-oferta/pakiety-weekendowe/pakiety-okazjonalne'], ['class' => $type == "pakiety-okazjonalne"?"active":""]);?>
			</div>
			<div class="col-md-3">
				<?= Html::a('<img src="/img/icon/tab_offer_'.($type == "profilaktyczno-lecznicze"?"active_":"").'logo.png"><span>Wykłady edukacyjne, zdrowa kuchnia/dietoterapia</span><h3>Profilaktyczne</h3>', ['/pl/nasza-oferta/pakiety-weekendowe/profilaktyczno-lecznicze'], ['class' => $type == "profilaktyczno-lecznicze"?"active":""]);?>
			</div>
		</div>
	</div>
	
	<div class="items-offer-activity">
		<?=$Items;?>
	</div>
	
	<h3 class="title-lead text-violet"><span data-title="Oferty aktualnie niedostępne">Oferty aktualnie niedostępne</span></h3>
	
	<p class="text-center f18 red">Jeśli jesteś zainteresowany turnusem aktualnie nie zaplanowanym, skontaktuj się z nami -<br>być może specjalnie dla Ciebie go zorganizujemy</p><br><br>
	
	<div class="items-offer-activity">
		<?=$Disabled;?>
	</div>
	
	<?php Pjax::end(); ?>
</div>