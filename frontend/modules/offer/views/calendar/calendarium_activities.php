<?php foreach ($items as $item) : ?>
<?php if ($item->special_event == 1) : ?>
		<div class="event-top">
			<div class="calendarium-left calendarium-left-label-event calendarium-left-event">Wydarzenie specjalne</div>
			<div class="calendarium-content calendarium-content-event"><?=$item->special_event_text;?></div>
		</div>
		<div class="clearfix"></div>
	<?php endif; ?>
<div class="calendarium-activities<?php if ($item->special_event == 1) : echo " event-bottom"; endif; ?>">
	
	<div class="calendarium-left text-right calendarium-activities-name <?php if ($item->special_event == 1) : echo "calendarium-left-event"; endif; ?>">
		<?=$item->name;?>
	</div>
	<div class="calendarium-content">
		<div class="calendarium-activities-days">
			<?php for($d = 1; $d <= $days; $d++) : ?>
				<span class="calendarium-activities-grill"> </span>
			<?php endfor; ?>
			
			<!-- lista turnusow -->

			
			<?php if (isset($dates[$item->stay_id])) : ?>
				<?php foreach ($dates[$item->stay_id] as $date_id => $date_values) : ?>
					<div data-stay-date-id="<?=$date_id;?>" class="calendarium-item-dates calendarium-item-days<?=$date_values["days"];?>" style="width:<?=$date_values["width"];?>px; left:<?=$date_values["_x"];?>px;"><?=$date_values["promotions"];?></div>
				<?php endforeach ;?>
			<?php endif; ?>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<?php endforeach; ?>