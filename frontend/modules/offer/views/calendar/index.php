<?php
	
	use yii\widgets\Pjax;
	use app\modules\offer\controllers\CalendarController;
	
	$this->title = 'Kalendarz turnusów leczniczych i pobytów weekendowych - '.$month_name.' '.$year;
	$this->registerJsFile('@web/js/calendarium.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
	
?><div class="container">
	<?php echo Yii::$app->controller->renderPartial('/default/header', ['tab' => 'calendar', 'title' => $title, 'subtitle' => $subtitle]); ?>
	
	<?php Pjax::begin([
		'id' => 'myPjax'
	]); ?>
	<div class="calendarium-area">
		<div class="calendarium-header">
			<img src="/img/icon/tab_offer_active_logo.png" class="logo-mini">
			<div class="calendarium-left">
				<? /*
				<p class="">Wybierz widok</p>
				<ul class="calendarium-views">
					<li>Miesięczny</li>
					<li>Półmiesięczny</li>			
				</ul>
				*/ ?>
				<div class="clearfix"></div>
			</div>
			<div class="calendarium-content">
				<?php if (isset($prev_month)) : ?>
					<a href="/pl/nasza-oferta/kalendarz/<?=$prev_month["url"];?>" class="calendarium-nav-month calendarium-prev"><?=$prev_month["label"];?></a>
				<?php endif; ?>
				<p class="calendarium-month">
					<?=$month_name;?> <?=$year;?>
				</p>
				<div class="calendarium-days">
					<?php for($d = 1; $d <= $days; $d++) : ?>
						<?php $date = "{$year}-".($month < 10 ? 0 : '').$month."-".($d < 10 ? 0 : '').$d; ?>
						
						<span><?=$d;?><br><?=Yii::$app->general->day_from_date($date, true);?></span>
					<?php endfor; ?>
				</div>
				<?php if (isset($next_month)) : ?>
					<a href="/pl/nasza-oferta/kalendarz/<?=$next_month["url"];?>" class="calendarium-nav-month calendarium-next"><?=$next_month["label"];?></a>
				<?php endif; ?>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="calendarium-body">
			<?php foreach ($groups as $group_id => $group_data) : ?>
			<?php if (isset($items[$group_id])) : ?>
			
				<?php 
					if (isset($subgroups[$group_id])) :
						$exists_items = false;
						if ($items[$group_id][1]->models OR $items[$group_id][2]->models OR $items[$group_id][3]->models) :
							$exists_items = true;
						endif;
					else :
						$exists_items = $items[$group_id]->getTotalCount()==0?false:true;
					endif;
				?>
				
				<?php if ($exists_items == true) : ?>
				<div class="calendarium-group">
					<div class="calendarium-left text-right calendarium-left-days">
						<?=$group_data["days"];?>
					</div>
					<div class="calendarium-content text-center">
						<?=$group_data["name"];?>
					</div>
					<div class="clearfix"></div>
				</div>
				
				<!-- podkategorie -->
					<?php if (isset($subgroups[$group_id])) : ?>
					
						<?php foreach ($subgroups[$group_id] as $subgroup_id => $subgroup_data) : ?>
							<?php if ($items[$group_id][$subgroup_id]->models) : ?>
								<div class="calendarium-subgroup">
									<div class="calendarium-left text-right">
										<?=$subgroup_data["name"];?>
									</div>
									<div class="clearfix"></div>
								</div>
								<!-- lista turnusow -->
								<?=CalendarController::calendariumActivities(['items' => $items[$group_id][$subgroup_id]->models, 'days' => $days, 'period' => $period]);?>
							<?php endif; ?>
						<?php endforeach; ?>
	
					<?php else : ?>
					
					<!-- lista turnusow -->
					<?=CalendarController::calendariumActivities(['items' => $items[$group_id]->models, 'days' => $days, 'period' => $period]);?>
					
					<?php endif; ?>
				
				<?php endif; ?>
				
				
			<?php endif; ?>
			<?php endforeach; ?>
		</div>
	</div>
	<?php Pjax::end(); ?>
</div>

<div class="calendarium-popup">
	<img src="/img/icon/recommended_gold.png" class="recommended">
	<div class="row">
		<div class="col-md-6">
			<p class="info load-period"></p>
			<div class="thumb"><img class="load-image" src="/img/gallery/lawendowy_zdroj_turnus_sanatoryjny_tygodniowy_500_370.jpg"></div>
		</div>
		<div class="col-md-6 text-center">
			<p class="info text-left load-days"></p>
			<div class="pricelist">
				Cena od <span class="price">1031 <small>PLN</small></span>
				<span class="old-price">zamiast 1107 PLN</span><br>
				za 2 osoby
			</div>
		</div>
	</div>
	<p class="calendarium-popup-title load-name">Chwila dla ciała i duszy</p>
	<div class="row">
		<div class="col-md-6">
			<a href="" class="btn btn-block btn-info">Szczegóły</a>
		</div>
		<div class="col-md-6">
			<a href="" class="btn btn-block btn-success">Zarezerwuj pobyt</a>
		</div>
	</div>
</div>