<?php
	
	namespace app\modules\offer\controllers;
	
	use yii\web\Controller;
	use common\models\TreatmentsCategories;
	use common\models\RoomsPrice;
	use common\models\SeassonGroups;
	use common\models\Activities;
	use common\models\ActivitiesSearch;
	
	class PricelistController extends Controller
	{
	    public function actionRooms($persons = "wszystkie", $class = "wszystkie", $seassons = "sezon-sredni")
	    {
		    $data["title"] = "Cennik pokoi";
		    $data["subtitle"] = "w Hotelu Lawendowy Zdrój";
		    $data["type"] = "nasze-pokoje";
		    $data["view"] = "rooms";
		    $data["cols"] = true;
		    
		    $data["mods"] = ["persons", "class", "seassons"];
		    
		    $data["persons"] = $persons;
		    $data["class"] = $class;
		    $data["seassons"] = $seassons;
		    $percent_discount = 0;
		    
		    if ($seassons != "sezon-sredni")
		    {
		    	$data["seasson"] = SeassonGroups::find()->where(['alias' => $seassons])->one();
				$percent_discount = $data["seasson"]->discount;
			}

		    if (($class == "wszystkie" OR $class == "komfort") AND ($persons == "wszystkie" OR $persons == "2-osobowe")) { $data["items"][1][2] = RoomsPrice::pricelist(3,2,0, $percent_discount); }
		    if (($class == "wszystkie" OR $class == "komfort") AND ($persons == "wszystkie" OR $persons == "3-osobowe")) { $data["items"][1][3] = RoomsPrice::pricelist(3,3,0, $percent_discount); }
		    if (($class == "wszystkie" OR $class == "komfort") AND ($persons == "wszystkie" OR $persons == "4-osobowe")) { $data["items"][1][4] = RoomsPrice::pricelist(3,4,0, $percent_discount); }
		    if (($class == "wszystkie" OR $class == "premium") AND ($persons == "wszystkie" OR $persons == "2-osobowe")) { $data["items"][2][2] = RoomsPrice::pricelist(2,2,0, $percent_discount); }
		    if (($class == "wszystkie" OR $class == "premium") AND ($persons == "wszystkie" OR $persons == "3-osobowe")) { $data["items"][2][3] = RoomsPrice::pricelist(2,3,0, $percent_discount); }
		    if (($class == "wszystkie" OR $class == "premium") AND ($persons == "wszystkie" OR $persons == "4-osobowe")) { $data["items"][2][4] = RoomsPrice::pricelist(2,4,0, $percent_discount); }
		    if (($class == "wszystkie" OR $class == "prestige") AND ($persons == "wszystkie" OR $persons == "2-osobowe")) { $data["items"][3][2] = RoomsPrice::pricelist(1,2,0, $percent_discount); }
		    if (($class == "wszystkie" OR $class == "prestige") AND ($persons == "wszystkie" OR $persons == "3-osobowe")) { $data["items"][3][3] = RoomsPrice::pricelist(1,3,0, $percent_discount); }
		    if (($class == "wszystkie" OR $class == "prestige") AND ($persons == "wszystkie" OR $persons == "4-osobowe")) { $data["items"][3][4] = RoomsPrice::pricelist(1,4,0, $percent_discount); }
		    
		    return $this->render('index', $data);
	    }
	    
	    public function actionActivities($persons = "wszystkie", $class = "wszystkie", $category = "wszystkie")
	    {
		    $data["title"] = "Cennik turnusów leczniczych";
		    $data["subtitle"] = "w Hotelu Lawendowy Zdrój";
		    $data["type"] = "turnusy-lecznicze";
		    $data["view"] = "activities";
		    $data["cols"] = true;
		    
		    $data["categories"] = ["sanatoryjne" => "Sanatoryjne", "profilaktyczno-lecznicze" => "Profilaktyczne", "specjalistyczne" => "Specjalistyczne"];
		    $data["mods"] = ["persons", "class", "type"];
		    
		    $data["persons"] = $persons;
		    $data["class"] = $class;
		    $data["category"] = $category;
		    
		    
		    
		    return $this->render('index', $data);
	    }
	    
	    public function actionPackage($persons = "wszystkie", $class = "wszystkie", $category = "wszystkie")
	    {
		    $data["title"] = "Cennik pakietów weekendowych";
		    $data["subtitle"] = "w Hotelu Lawendowy Zdrój";
		    $data["type"] = "pakiety-weekendowe";
		    $data["cols"] = true;
		    $data["view"] = "packages";
		    
		    $data["mods"] = ["persons", "class", "type"];
		    $data["categories"] = ["spa-i-wellness" => "SPA & Wellness", "pakiety-okazjonalne" => "Okazjonalne", "profilaktyczno-lecznicze" => "Profilaktyczne"];
		    
		    $data["model"] = new ActivitiesSearch();
			$data["items"] = $data["model"]->search(["type" => "pakiety-weekendowe".($category != "wszystkie" ? "-".$category : ""), "disabled" => $disabled]);
		    
		    $data["persons"] = $persons;
		    $data["class"] = $class;
		    $data["category"] = $category;
		    
		    return $this->render('index', $data);
	    }
	    
	    public function actionTreatments()
	    {
		    $data["title"] = "Cennik zabiegów medycznych";
		    $data["subtitle"] = "w Hotelu Lawendowy Zdrój";
		    $data["type"] = "zabiegi";
		    $data["cols"] = false;
		    $data["view"] = "treatments";
		    $data["items"] = TreatmentsCategories::find()->all();
		    
		    return $this->render('index', $data);
	    }
	}

?>