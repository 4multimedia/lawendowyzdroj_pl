<?php
	
	namespace app\modules\offer\controllers;
	
	use yii\web\Controller;
	use common\models\RoomsSearch;
	
	class RoomsController extends Controller
	{
	    public function actionIndex($type = "wszystkie", $class = "wszystkie", $seasson = "sezon-sredni")
	    {
		    $data["title"] = "Oferta pokoi hotelowych";
		    $data["subtitle"] = "W Busko-Zdroju, Hotel Lawendowy Zdrój";
		    $data["type"] = $type;
		    
		    $data["model"] = new RoomsSearch();
		    
		    if ($type AND $type != "wszystkie")
		    {
		    	$data["params"]["person_from"] = str_replace("-osobowe", "", $type);
		    	$data["params"]["person_to"] = $data["params"]["person_from"];
		    }
		    
		    $data["class"] = $class;
		    if ($class AND $class != "wszystkie")
		    {
			    if ($class == "komfort") { $class = 3; }
			    if ($class == "premium") { $class = 2; }
			    if ($class == "prestige") { $class = 1; }
			    $data["params"]["class"] = $class;
		    }
		    
		    $data["seasson"] = $seasson;
		    
		    $data["items"] = $data["model"]->search($data["params"]);
		    
		    return $this->render('index', $data);
	    }
	}

?>