<?php
	
	namespace app\modules\offer\controllers;
	
	use Yii;
	use yii\web\Controller;
	use common\models\Activities;
	use common\models\ActivitiesSearch;
	
	class ActivitiesController extends Controller
	{
	    public function actionIndex($type = "")
	    {
		    $data["title"] = "Oferta turnusów leczniczych";
		    $data["subtitle"] = "w Lawendowym Zdroju - Busko Zdrój";
		    $data["type"] = $type;
		    
		    $data["Items"] = self::actionList("turnusy-lecznicze".($type ? "-".$type : ""));
		    $data["Disabled"] = self::actionList("turnusy-lecznicze".($type ? "-".$type : ""), true);
		    return $this->render('index', $data);
	    }
	    
	    public function actionList($type = "", $disabled = false)
	    {
			$data["model"] = new ActivitiesSearch();
			$data["items"] = $data["model"]->search(["type" => $type, "disabled" => $disabled]);
			$data["type"] = $type;
			
		    return Yii::$app->controller->renderPartial('list', $data);
	    }
	}

?>