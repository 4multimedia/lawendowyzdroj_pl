<?php
	
	namespace app\modules\offer\controllers;
	
	use Yii;
	use yii\web\Controller;
	use common\models\Activities;
	use common\models\ActivitiesSearch;
	
	class PackagesController extends Controller
	{
	    public function actionIndex($type = "")
	    {
		    $data["title"] = "Oferta pobytów weekendowych & SPA";
		    $data["subtitle"] = "Hotel Lawendowy Zdrój, Busko-Zdrój";
		    $data["type"] = $type;
		    
		    $data["Items"] = self::actionList("pakiety-weekendowe".($type ? "-".$type : ""));
		    $data["Disabled"] = self::actionList("pakiety-weekendowe".($type ? "-".$type : ""), true);
		    return $this->render('index', $data);
	    }
	    
	    public function actionList($type = "", $disabled = false)
	    {
			$data["model"] = new ActivitiesSearch();
			$data["items"] = $data["model"]->search(["type" => $type, "disabled" => $disabled]);
			$data["type"] = $type;
			
		    return Yii::$app->controller->renderPartial('/activities/list', $data);
	    }
	}

?>