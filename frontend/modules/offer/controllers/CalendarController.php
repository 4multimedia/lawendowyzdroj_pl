<?php
	
	namespace app\modules\offer\controllers;
	
	use yii\web\Controller;
	use common\models\Activities;
	use common\models\ActivitiesSearch;
	use common\models\ActivitiesDates;
	
	class CalendarController extends Controller
	{
		CONST day_width = 26;
		
		public function calendariumActivities($data)
		{
			$activities = [];
			foreach ($data["items"] as $item)
			{
				$activities[] = $item->stay_id;
			}
			
			if ($activities)
			{
				list($period_year, $period_month) = explode("-", $data["period"]);
				$dates = ActivitiesDates::find()
				->select([
					'stays_dates.date_from',
					'stays_dates.date_to',
					'stays_dates.stays_id',
					'stays_dates.id',
					'stays.days'
				])
				->where("stays_id IN (".implode(",", $activities).") AND (MONTH(`stays_dates`.`date_to`) = $period_month AND YEAR(`stays_dates`.`date_to`) = $period_year) OR (MONTH(`stays_dates`.`date_from`) = $period_month AND YEAR(`stays_dates`.`date_from`) = $period_year)")
				->leftJoin("stays", "stays.id = stays_id")
				->orderBy("date_from ASC")
				->all();
				
				$data["dates"] = [];
				if ($dates)
				{
					foreach ($dates as $date)
					{
						$this_month = date("m", strtotime($date->date_from));
						$data["dates"][$date->stays_id][$date->id]["begin"] = $date->date_from;
						$data["dates"][$date->stays_id][$date->id]["end"] = $date->date_to;
						$data["dates"][$date->stays_id][$date->id]["days"] = $date->days;
						$data["dates"][$date->stays_id][$date->id]["promotions"] = "5%";
						
						// ustawienia dla polozenia
						if ($period_month == $this_month)
						{
							$data["dates"][$date->stays_id][$date->id]["_x"] = (number_format(date("d", strtotime($date->date_from))) - 1) * self::day_width;
						}
						else
						{
							$data["dates"][$date->stays_id][$date->id]["_x"] = (($date->days - number_format(date("d", strtotime($date->date_to)))) * self::day_width) * -1;
						}
						
						$data["dates"][$date->stays_id][$date->id]["width"] = number_format($date->days) * self::day_width;
					}
				}
				
				return $this->render('calendarium_activities', $data);
			}
		}
		
	    public function actionIndex($month = "", $year = "")
	    {
		    if ($month != "")
		    {
			    $month = \Yii::$app->general->month_id($month);
		    }
		    
			$data["month"] = number_format($month?$month:date("m"));
		    $data["month_name"] = \Yii::$app->general->month($data["month"]);
		    $data["year"] = $year?$year:date("Y");
		    $data["period"] = $data["year"].'-'.($data["month"] < 10 ? 0 : '').$data["month"];
		    $data["days"] = date("t", strtotime($data["period"]));
		    $last_period = ActivitiesDates::last_period();
		    
		    if (strtotime(date("Y-m")) > strtotime($data["period"]))
		    {
				\Yii::$app->getResponse()->redirect(array('/pl/nasza-oferta'));  
			}
			else if (strtotime($data["period"]) > strtotime($last_period))
			{
				list ($year, $month) = explode("-", $last_period);
				\Yii::$app->getResponse()->redirect(array("/pl/nasza-oferta/kalendarz/".\Yii::$app->general->create_alias(\Yii::$app->general->month($month))."/".$year));
			}
		    else
		    {
			    $data["title"] = "Kalendarz turnusów";
			    $data["subtitle"] = "Zaplanuj pobyt w Lawendowym Zdroju";
			    
			    $prev_year = date("Y", strtotime("-1 month", strtotime($data["period"])));
			    $prev_month = date("m", strtotime("-1 month", strtotime($data["period"])));
			    
			    if (strtotime(date("Y-m")) < strtotime($data["period"]))
			    {
			    	$data["prev_month"]["label"] = \Yii::$app->general->month($prev_month)." ".$prev_year;
			    	$data["prev_month"]["url"] = \Yii::$app->general->create_alias(\Yii::$app->general->month($prev_month))."/".$prev_year;
				}
				
				$next_year = date("Y", strtotime("+1 month", strtotime($data["period"])));
				$next_month = date("m", strtotime("+1 month", strtotime($data["period"])));
			    
			    if (strtotime($data["period"]) < strtotime($last_period))
			    {
			    	$data["next_month"]["label"] = \Yii::$app->general->month($next_month)." ".$next_year;
					$data["next_month"]["url"] = \Yii::$app->general->create_alias(\Yii::$app->general->month($next_month))."/".$next_year;	
				}
			    
			    $data["groups"] = [
			    	1 => ['name' => 'Pakiety weekendowe', 'days' => 'około 2-dniowe'],
			    	2 => ['name' => 'Turnusy lecznicze około 7-dniowe', 'days' => 'około 7-dniowe'],
			    	3 => ['name' => 'Turnusy lecznicze około 14-dniowe', 'days' => 'około 14-dniowe'],
			    	4 => ['name' => 'Turnusy lecznicze około 21-dniowe', 'days' => 'około 21-dniowe']
			    ];
			    
			    $data["subgroups"] = [
				    1 => [
						1 => ['name' => 'SPA & Wellness'],
						2 => ['name' => 'Okazjonalne'],
						3 => ['name' => 'Profilaktyczne']
					],
					2 => [
						1 => ['name' => 'Sanatoryjne'],
						2 => ['name' => 'Profilaktyczne'],
						3 => ['name' => 'Specjalistyczne']
					],
					3 => [
						1 => ['name' => 'Sanatoryjne'],
						2 => ['name' => 'Profilaktyczne'],
						3 => ['name' => 'Specjalistyczne']
					],
					4 => [
						1 => ['name' => 'Sanatoryjne'],
						2 => ['name' => 'Profilaktyczne'],
						3 => ['name' => 'Specjalistyczne']
					]  
				];
			    
			    // petla po turnusach
			    $data["model"] = new ActivitiesSearch();
				$data["items"][1][1] = $data["model"]->search(['type' => 'pakiety-weekendowe-spa-i-wellness', 'period' => $data["period"]]);
				$data["items"][1][2] = $data["model"]->search(['type' => 'pakiety-weekendowe-pakiety-okazjonalne', 'period' => $data["period"]]);
				$data["items"][1][3] = $data["model"]->search(['type' => 'pakiety-weekendowe-profilaktyczno-lecznicze', 'period' => $data["period"]]);
				$data["items"][2][1] = $data["model"]->search(['type' => 'turnusy-lecznicze-7dniowe-sanatoryjne', 'period' => $data["period"]]);
				$data["items"][2][2] = $data["model"]->search(['type' => 'turnusy-lecznicze-7dniowe-profilaktyczne', 'period' => $data["period"]]);
				$data["items"][2][3] = $data["model"]->search(['type' => 'turnusy-lecznicze-7dniowe-specjalistyczne', 'period' => $data["period"]]);
				$data["items"][3][1] = $data["model"]->search(['type' => 'turnusy-lecznicze-14dniowe-sanatoryjne', 'period' => $data["period"]]);
				$data["items"][3][2] = $data["model"]->search(['type' => 'turnusy-lecznicze-14dniowe-profilaktyczne', 'period' => $data["period"]]);
				$data["items"][3][3] = $data["model"]->search(['type' => 'turnusy-lecznicze-14dniowe-specjalistyczne', 'period' => $data["period"]]);
				$data["items"][4][1] = $data["model"]->search(['type' => 'turnusy-lecznicze-21dniowe-sanatoryjne', 'period' => $data["period"]]);
				$data["items"][4][2] = $data["model"]->search(['type' => 'turnusy-lecznicze-21dniowe-profilaktyczne', 'period' => $data["period"]]);
				$data["items"][4][3] = $data["model"]->search(['type' => 'turnusy-lecznicze-21dniowe-specjalistyczne', 'period' => $data["period"]]);
			    
			    return $this->render('index', $data);
			}
	    }
	}

?>