<?php
	
	namespace app\modules\offer\controllers;
	
	use yii\web\Controller;
	
	class PromotionsController extends Controller
	{
	    public function actionIndex()
	    {
		    $data["title"] = "Promocje i oferty specjalne";
		    $data["subtitle"] = "w Hotelu Lawendowy Zdrój";
		    
		    return $this->render('index', $data);
	    }
	}

?>