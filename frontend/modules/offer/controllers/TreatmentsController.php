<?php
	
	namespace app\modules\offer\controllers;
	
	use yii\web\Controller;
	use common\models\TreatmentsSearch;
	use common\models\TreatmentsCategories;
	
	class TreatmentsController extends Controller
	{
	    public function actionIndex($alias = "")
	    {
		    $data["title"] = "Zabiegi lecznicze";
		    $data["subtitle"] = "Cennik zabiegów leczniczych";
		    $data["alias"] = $alias;
		    
		    $data["categories"] = TreatmentsCategories::find()->all();
		    if ($alias)
		    {
		    	$data["category"] = TreatmentsCategories::find()->where(['alias' => $alias])->one();
		    }
		    
		    $data["model"] = new TreatmentsSearch();
			$data["items"] = $data["model"]->search(["category_id" => $data["category"]->id]);
		    
		    
		    return $this->render('index', $data);
	    }
	}

?>