<?php
	
	namespace app\modules\specialoffer\controllers;
	
	use yii\web\Controller;
	use common\models\ActivitiesBestDeal;
	
	class ActivitiesController extends Controller
	{
		public function actionBestdeal()
		{
			$data["Discounts"] = ActivitiesBestDeal::find()
			->select(["*", "date_from", "date_to", "stays_date_id as date_id", "discount"])
			->where("date_from > NOW()")
			->leftJoin("stays_dates", "stays_date_id = stays_dates.id")
			->orderBy('RAND()')
			->all();
			
			return $this->render('bestdeal', $data);
		}
	}

?>