<?php
	
	namespace app\modules\specialoffer\controllers;
	
	use yii\web\Controller;
	use common\models\RoomsLastMinute;
	
	class RoomsController extends Controller
	{
		public function actionLastminute()
		{
			$data["Discounts"] = RoomsLastMinute::find()
			->from("(select * from objects_last_minute ORDER BY discount DESC) as objects_last_minute")
			->groupBy("object_id")
			->all();
			
			return $this->render('lastminute', $data);
		}
	}

?>