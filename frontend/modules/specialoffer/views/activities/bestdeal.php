<div class="container">
	
	<div class="lead-header lead-header-slim">
		<h2>Super oferty <small>Turnusy lecznicze</small></h2>
		<img src="/img/offer/activity/header_activity.jpg">
	</div>
	
	<div class="content-page content-text-cms">
		<div class="box-special">
			<div class="discount">
				<div class="awardeds">
					<div class="title"><h2><span>POLECAMY</span></h2><hr></div>
		<?php if ($Discounts) { ?>
		<div class="box-awarded"><div class="row">
			<?php foreach($Discounts as $discount_key => $Discount) { ?>
			<div class="col-md-4">
			<div class="item">
				<div class="title">
					<span class="no"><?=$Discount->activities->no;?></span>
					<img src="/img/icon/recommended_gold.png">
					<span class="rooms"><?=$Discount->activities->capacity;?> miejsc</span>
				</div>
				<div class="body">
					<div class="body-top">
						<div class="image">
							<?php $image = Yii::getAlias('@app')."/web".urldecode($Discount->activities->getImage(500, 310, 'lawendowy_zdroj_'.$Discount->activities->name)); ?>
							<?php if (file_exists($image) AND $Discount->activities->getImage(500, 310, 'lawendowy_zdroj_'.$Discount->activities->name) != "") : ?>
								<img src="<?= $Discount->activities->getImage(500, 310, 'lawendowy_zdroj_'.$Discount->activities->name);?>">
							<?php endif; ?>
						</div>
						<div class="name name-twoline"><?=$Discount->activities->name;?></div>
						
						<div class="discount-name">Super oferta</div>
						
						<div class="buttons">
							<a href="#" class="orange use" data-stay-from="<?=$Discount["date_from"];?>" data-stay-to="<?=$Discount["date_to"];?>" data-object_date_id="<?=$Discount["date_id"];?>" data-object-id="<?=$Discount["id"];?>" data-cancelation="<?=$Discount->activities->price_cancellation;?>">Skorzystaj z oferty</a>
						</div>
						
						<div class="period"></div>
						
						<div class="discount-name"><?=($Discount["discount"] + $Discount->activities->price_discount_random_room);?>% taniej</div>
						
						<div class="clearfix"></div>
						
					</div>
					<div class="body-footer">
						<span class="period">Teraz cena od <?=$price;?> PLN / <?=$Participants;?> osoby</span>
					</div>
				</div>
			</div>
			</div>
			<?php if ($discount_key == 2) { break; } ?>
			<?php } ?>
			<div class="clearfix"></div>
		</div></div>
		<?php } ?>
	</div>
	<?php if ($Discounts) { ?>
	<div class="list">
		<?php foreach($Discounts as $discount_key => $Discount) { ?>
		<div class="item">
			<div class="title">
				<span class="no"><?=$Discount->activities->no;?></span>
				<img src="/img/icon/recommended_gold.png">
				<span class="rooms"><?=$Discount->activities->capacity;?> miejsc</span>
				<span class="location"></span>
			</div>
		</div>
		<div class="body">
			<div class="left">
				<div class="image">
					<?php $image = Yii::getAlias('@app')."/web".urldecode($Discount->activities->getImage(500, 310, 'lawendowy_zdroj_'.$Discount->activities->name)); ?>
					<?php if (file_exists($image) AND $Discount->activities->getImage(500, 310, 'lawendowy_zdroj_'.$Discount->activities->name) != "") : ?>
						<img src="<?= $Discount->activities->getImage(500, 310, 'lawendowy_zdroj_'.$Discount->activities->name);?>">
					<?php endif; ?>
				</div>
				<div class="buttons">
					<a href="" class="use orange" data-stay-from="<?=$Discount["date_from"];?>" data-stay-to="<?=$Discount["date_to"];?>" data-object_date_id="<?=$Discount["date_id"];?>" data-object-id="<?=$Discount["id"];?>" data-cancelation="<?=$Discount->activities->price_cancellation;?>">Skorzystaj z oferty</a>
				</div>
			</div>
			<div class="right">
				
				<div class="right-summary">
					<h3><?=$Discount->activities->name;?></h3>
				</div>
				
				<p><b>Super oferta</b> <strong><?=($Discount["discount"] + $Discount->activities->price_discount_random_room);?>% taniej</strong></p>
								
				<div class="bestdeal bestdeal-long">
					<span class="text">teraz cena od <strong><?=$price;?></strong> PLN / <?=$Participants;?> osoby</span>
					<a class="use 2big" href="" data-stay-from="<?=$Discount["date_from"];?>" data-stay-to="<?=$Discount["date_to"];?>" data-object_date_id="<?=$Discount["date_id"];?>" data-object-id="<?=$Discount["id"];?>" data-cancelation="<?=$Discount->activities->price_cancellation;?>">Skorzystaj <i class="2big fa fa-angle-right"></i></a>
				</div>

				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php } ?>
	</div>
	<?php } ?>
</div>

		</div>
	</div>
</div>