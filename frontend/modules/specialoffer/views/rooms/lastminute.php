<div class="container">
	
	<div class="lead-header lead-header-slim">
		<h2>Last Minute <small>Nasze pokoje</small></h2>
		<img src="/img/offer/activity/header_activity.jpg">
	</div>
	
	<div class="content-page content-text-cms">
		<div class="box-special">
			<div class="discount discount-lastminute">
				<div class="awardeds">
					<div class="title"><h2><span>POLECAMY</span></h2><hr></div>
					<?php if ($Discounts) : ?>
						<div class="box-awarded">
							<div class="row">
								<?php for($d = 0; $d < (count($Discounts) > 3 ? 3 : count($Discounts)); $d++) : ?>
								<div class="col-md-4">
									<div class="item">
										<div class="title">
											<span class="no"><?=$Discounts[$d]->room->no;?></span>
											<img src="/img/icon/recommended_gold.png">
											<span class="rooms"><?=$Discounts[$d]->room->max_beds;?> łóżka</span>
										</div>
										<div class="body">
											<div class="body-top">
												<div class="image">
													<?php $image = Yii::getAlias('@app')."/web".urldecode($Discounts[$d]->room->getImage(500, 310, 'lawendowy_zdroj_pokoj_'.$Discounts[$d]->room->name)); ?>
													<?php if (file_exists($image) AND $Discounts[$d]->room->getImage(500, 310, 'lawendowy_zdroj_pokoj_'.$Discounts[$d]->room->name) != "") : ?>
														<img src="<?= $Discounts[$d]->room->getImage(500, 310, 'lawendowy_zdroj_pokoj_'.$Discounts[$d]->room->name);?>">
													<?php endif; ?>
												</div>
												<div class="name"><?=$Discounts[$d]->room->name;?></div>
												<div class="buttons">
													<a href="javascript:;" class="orange">Skorzystaj z promocji</a>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="body-footer">
												<span class="period">Zniżki do <?=$Discounts[$d]->discount;?>%</span>
											</div>
										</div>
									</div>
								</div>
								<?php endfor; ?>
							</div>
						</div>
					<?php endif; ?>
				</div>
			
				<?php if ($Discounts AND count($Discounts) > 3) : ?>
				<div class="list">
					<?php for($d = 3; $d < count($Discounts); $d++) : ?>
					<div class="item">
						<div class="title">
							<span class="no"><?=$Discounts[$d]->room->no;?></span>
							<img src="/img/icon/recommended_gold.png">
							<span class="rooms"><?=$Discounts[$d]->room->max_beds;?> łóżka</span>
							<span class="location"><?=$Discounts[$d]->room->name;?></span>
						</div>
					</div>
					<div class="body">
						<div class="left">
							<div class="image">
								<?php $image = Yii::getAlias('@app')."/web".urldecode($Discounts[$d]->room->getImage(500, 310, 'lawendowy_zdroj_pokoj_'.$Discounts[$d]->room->name)); ?>
								<?php if (file_exists($image) AND $Discounts[$d]->room->getImage(500, 310, 'lawendowy_zdroj_pokoj_'.$Discounts[$d]->room->name) != "") : ?>
									<img src="<?= $Discounts[$d]->room->getImage(500, 310, 'lawendowy_zdroj_pokoj_'.$Discounts[$d]->room->name);?>">
								<?php endif; ?>
							</div>
							<div class="buttons">
								<a href="javascript:;" class="orange">Dodaj do planera podróży</a>
							</div>
						</div>
						<div class="right">
							<p class="evenly"><b>Zyskasz rabat</b> jeśli zarezerwujesz dziś</p>
	
							<?php foreach($Discounts[$d]->room->lastminute as $LastMinute) : ?>
							<div class="bestdeal bestdeal-shortgaps bestdeal-slimlong" data-discount="'.$LastMinute[$l]->discount.'" data-date-name="'.$day_name.'" data-date="'.$days_date.'">
								<b><?=$LastMinute->discount;?>%</b>
								<span class="period-inline">jeśli zarezerwujesz <strong>DZISIAJ</strong> a przyjedziesz <strong><?=$day_name;?></strong> <?=(($max_rent_days < 4 AND $max_rent_days != "") ? '(maks. '.$max_rent_days.' '.($max_rent_days == 1 ? 'noc' : 'noce').')' : '');?></span>
								<a href="javascript:;" data-object-url="obiekt/<?=$Discount[$d]->alias;?>" data-object-id="<?=$Discount[$d]->id;?>" data-max-person="<?=$Discount[$d]->max_person;?>" class="check-avaibility-modal">Skorzystaj <i class="fa fa-angle-right"></i></a>
							</div>
							<?php endforeach; ?>
	
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<?php endfor; ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>