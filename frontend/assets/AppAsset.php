<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/main.css',
        'css/main.responsive.css',
        'css/font-awesome.min.css',
        'css/jquery.ui.css',
    ];
    public $js = [
	    'js/main.js',
	    'js/search.js',
	    'js/moment.js',
	    'js/livechat.js',
    ];
    public $depends = [
	    'yii\jui\JuiAsset',
        'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapPluginAsset',
    ];
}
